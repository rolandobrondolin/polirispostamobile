package it.polirisposta.mobile.poliRISPOSTA.form.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewFlipper;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.listeners.ApplicableListener;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.listeners.CorrectnessListener;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.listeners.GPSListener;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.listeners.ImageListener;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.listeners.SpinnerListener;
import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.Note;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.QuestionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.Resource;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DataType;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DateAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.FloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.GpsAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IstatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.MeasureAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.ResourceType;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.Select1Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.SelectAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.StringAnswer;
import it.polirisposta.mobile.poliRISPOSTA.util.Util;


public class ContentFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener {
    public static final String TAG = "ContentFragmentView";
    private QuestionInstance questionInstance;
    private int sectionId, questionId;
    private HashMap<Integer, Integer> connection;
    private SaveContent saveActivity;
    private OnRetrieveQuestion questionActivity;
    private boolean invalidated;
    private static int REQUEST_CAMERA = 1000;
    private static int SELECT_FILE = 2000;
    String mCurrentPhotoPath;

    public interface OnRetrieveQuestion{
        public QuestionInstance retrieveQuestion(int sectionId, int questionId);
    }

    public ContentFragment(){
    }

    public interface SaveContent{
        public void saveContent(QuestionInstance qInst);
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        sectionId = args.getInt("sectionId");
        questionId = args.getInt("questionId");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.questionActivity = (OnRetrieveQuestion) activity;
            this.saveActivity = (SaveContent) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnRetrieveQuestion and SaveContent");
        }
        invalidated=false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_fragment, container, false);
        questionInstance = questionActivity.retrieveQuestion(sectionId, questionId);
        //build part!
        //connection = new HashMap<Integer, Integer>();
        TextView title = (TextView) rootView.findViewById(R.id.titleView);
        TextView description = (TextView) rootView.findViewById(R.id.descriptionView);

        String textTitle = questionInstance.getQuestion().getTextName();
        if(questionInstance.getIndex()>1){
            textTitle = textTitle.concat(" ("+ questionInstance.getIndex()+")");
        }
        title.setText(textTitle);

        description.setText(questionInstance.getQuestion().getDescription());
        return rootView;
    }

    public void onResume(){
        super.onResume();
        connection = new HashMap<Integer, Integer>();
        final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout content = (LinearLayout) getActivity().findViewById(R.id.contentLayout);
        content.removeAllViews();
        for(int i = 0; i< questionInstance.getAnswers().size(); i++){
            Answer a = questionInstance.getAnswer(i);
            if(a!=null){
                View fieldView = /*(LinearLayout)*/ generateField(a, inflater);
                connection.put(i, fieldView.getId());
                content.addView(fieldView);
            }
        }

        if(questionInstance.getQuestion().getPictures().size()==0){
            getActivity().findViewById(R.id.bundleImage).setVisibility(View.INVISIBLE);
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            p.addRule(RelativeLayout.BELOW, R.id.descriptionView);
            content.setLayoutParams(p);

        }
        else{
            //viewFlipper setup
            final ViewFlipper flip = (ViewFlipper) getActivity().findViewById(R.id.questionImages);
            Button btnNext = (Button) getActivity().findViewById(R.id.nextButton);
            Button btnPrev = (Button) getActivity().findViewById(R.id.prevButton);
            TextView descr = (TextView) getActivity().findViewById(R.id.imageDescription);
            if(questionInstance.getQuestion().getPictures().size()==1){
                btnNext.setVisibility(View.INVISIBLE);
                btnPrev.setVisibility(View.INVISIBLE);
                descr.setVisibility(View.INVISIBLE);
            }

            for(Resource res: questionInstance.getQuestion().getPictures()){
                ImageView v = new ImageView(getActivity());
                //TODO: add also descriprion
                try {
                    InputStream stream = getActivity().getContentResolver().openInputStream(res.getUri());
                    ImageView img = new ImageView(getActivity());
                    Bitmap thumbImage = BitmapFactory.decodeStream(stream);
                    img.setImageBitmap(thumbImage);
                    stream.close();
                    v.setImageBitmap(thumbImage);
                    v.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT ));
                    flip.addView(v);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flip.showNext();

                }
            });
            btnPrev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flip.showPrevious();
                }
            });
        }

        //images setup
        LinearLayout pictures = (LinearLayout) getActivity().findViewById(R.id.picturesLayout);
        pictures.removeAllViews();
        ImageView addImage = new ImageView(getActivity());
        addImage.setImageResource(R.drawable.add_picture);
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {getResources().getString(R.string.from_camera), getResources().getString(R.string.from_storage), getResources().getString(R.string.cancel)};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.add_photo);

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals(getResources().getString(R.string.from_camera))) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (intent.resolveActivity(ContentFragment.this.getActivity().getPackageManager()) != null) {
                                // Create the File where the photo should go
                                File photoFile = null;
                                try {
                                    photoFile = createImageFile();
                                } catch (IOException ex) {
                                    // Error occurred while creating the File
                                    ex.printStackTrace();
                                }
                                // Continue only if the File was successfully created
                                if (photoFile != null) {
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                                    startActivityForResult(intent, REQUEST_CAMERA);
                                }

                            }

                        } else if (items[item].equals(getResources().getString(R.string.from_storage))) {
                            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                        } else if (items[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });
        pictures.addView(addImage);
        for(int i = questionInstance.getPictures().size()-1; i>=0; i--){
            Uri uri = questionInstance.getPictures().get(i).getUri();
            try {
                InputStream stream = getActivity().getContentResolver().openInputStream(uri);
                ImageView img = new ImageView(getActivity());
                Bitmap thumbImage = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeStream(stream), 200, 200);
                img.setImageBitmap(thumbImage);
                stream.close();
                ImageListener imgListener = new ImageListener(getActivity(), uri, questionInstance, img);
                img.setOnClickListener(imgListener);
                pictures.addView(img);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }

        final LinearLayout noteLayout = (LinearLayout) ContentFragment.this.getActivity().findViewById(R.id.notesLayout);
        //notes setup
        Button noteAddButton = (Button) getActivity().findViewById(R.id.addNoteButton);
        noteAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LinearLayout l = (LinearLayout) inflater.inflate(R.layout.note_layout, null);
                noteLayout.addView(l, questionInstance.getNotes().size());
                l.setId(Util.generateViewId());
                Note note = new Note(-1, "");
                questionInstance.addNote(note);
                ImageButton removeNote = (ImageButton) l.findViewById(R.id.deleteNoteButton);
                removeNote.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        View parent = (View) view.getParent();
                        int layoudID = parent.getId();
                        int noteIndex = -1;
                        int n = noteLayout.getChildCount();
                        for(int i =0; i<n; i++){
                            if (noteLayout.getChildAt(i).getId() == layoudID){
                                noteIndex = i;
                            }
                        }
                        if (noteIndex != -1) {
                            Note note = questionInstance.getNotes().remove(noteIndex);
                            noteLayout.removeView(noteLayout.findViewById(layoudID));
                            ContentFragment.this.getActivity().getContentResolver().delete(DatabaseContentProvider.NOTE_URI, PoliRispostaDbContract.NoteColumns._ID + " = " + note.getId(), null);
                        }
                    }
                });
            }
        });

        //notes add!
        for(int i=0; i<questionInstance.getNotes().size(); i++){
            Note note = questionInstance.getNotes().get(i);
            LinearLayout l = (LinearLayout) inflater.inflate(R.layout.note_layout, null);
            noteLayout.addView(l, i);
            l.setId(Util.generateViewId());
            TextView t = (TextView) l.findViewById(R.id.noteEditText);
            t.setText(note.getNote());
            ImageButton removeNote = (ImageButton) l.findViewById(R.id.deleteNoteButton);
            removeNote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    View parent = (View) view.getParent();
                    int layoudID = parent.getId();
                    int noteIndex = -1;
                    int n = noteLayout.getChildCount();
                    for(int i =0; i<n; i++){
                        if (noteLayout.getChildAt(i).getId() == layoudID){
                            noteIndex = i;
                        }
                    }
                    if (noteIndex != -1) {
                        Note note = questionInstance.getNotes().remove(noteIndex);
                        noteLayout.removeView(noteLayout.findViewById(layoudID));
                        ContentFragment.this.getActivity().getContentResolver().delete(DatabaseContentProvider.NOTE_URI, PoliRispostaDbContract.NoteColumns._ID + " = " + note.getId(), null);
                    }
                }
            });

        }



    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File image = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), imageFileName + ".jpg");

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(mCurrentPhotoPath);
                Uri imgUri = Uri.fromFile(f);
                Resource img = new Resource(Uri.parse(mCurrentPhotoPath), "", ResourceType.IMAGE, -1);
                questionInstance.addPicture(img);

                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                mediaScanIntent.setData(imgUri);
                getActivity().sendBroadcast(mediaScanIntent);

            } else if (requestCode == SELECT_FILE) {

                for(Resource img : questionInstance.getPictures()){
                    if(img.getUri().compareTo(data.getData()) == 0){
                        Toast.makeText(getActivity(), "L'immagine è già stata aggiunta alla domanda", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                Resource img = new Resource(data.getData(), "", ResourceType.IMAGE, -1);
                questionInstance.addPicture(img);
            }
        }
    }

    private View generateField(Answer a, LayoutInflater inflater){
        View fieldView = new LinearLayout(this.getActivity());
        TextView fieldName = null;
        CheckBox applicable;
        switch (a.getType()){
            case STRING:
                fieldView = inflater.inflate(R.layout.my_text_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.textView);
                EditText textAnswer = (EditText) fieldView.findViewById(R.id.textEditText);
                //Set correctness listener
                textAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                if(a.isFilled()){
                    textAnswer.setText(((StringAnswer)a).getAnswer());
                }
                else{
                    textAnswer.setText("");
                }
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.STRING, this));
                fieldName.setText(a.getField().getTextName());
                applicable.setChecked(!a.isApplicable());

                break;
            case INTEGER:
                fieldView = inflater.inflate(R.layout.my_text_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.textView);
                EditText intAnswer = (EditText) fieldView.findViewById(R.id.textEditText);
                //Set correctness listener
                intAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                if(a.isFilled()){
                    intAnswer.setText(Integer.toString(((IntegerAnswer)a).getAnswer()));
                }
                else{
                    intAnswer.setText("");
                }
                fieldName.setText(a.getField().getTextName());
                intAnswer.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_CLASS_NUMBER);
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.INTEGER, this));
                applicable.setChecked(!a.isApplicable());
                break;
            case POSITIVE_INTEGER:
                fieldView = inflater.inflate(R.layout.my_text_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.textView);
                EditText intPosAnswer = (EditText) fieldView.findViewById(R.id.textEditText);
                //Set correctness listener
                intPosAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                if(a.isFilled()){
                    intPosAnswer.setText(Integer.toString(((PositiveIntegerAnswer)a).getAnswer()));
                }
                else{
                    intPosAnswer.setText("");
                }
                fieldName.setText(a.getField().getTextName());
                intPosAnswer.setInputType(InputType.TYPE_CLASS_NUMBER);
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.POSITIVE_INTEGER, this));
                applicable.setChecked(!a.isApplicable());

                break;
            case NEGATIVE_INTEGER:
                fieldView = inflater.inflate(R.layout.my_text_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.textView);
                EditText intNegAnswer = (EditText) fieldView.findViewById(R.id.textEditText);
                //Set correctness listener
                intNegAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                if(a.isFilled()){
                    intNegAnswer.setText(Integer.toString(((NegativeIntegerAnswer)a).getAnswer()));
                }
                else{
                    intNegAnswer.setText("");
                }
                fieldName.setText(a.getField().getTextName());
                intNegAnswer.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}

                    @Override
                    public void afterTextChanged(Editable s) {
                        String text = s.toString();
                        int length = text.length();
                        if(!text.matches("^-[0-9]*") && length>0) {
                            s.delete(length - 1, length);
                        }
                    }
                });
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.NEGATIVE_INTEGER, this));
                applicable.setChecked(!a.isApplicable());

                break;
            case FLOAT:
                fieldView = inflater.inflate(R.layout.my_text_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.textView);
                EditText floatAnswer = (EditText) fieldView.findViewById(R.id.textEditText);
                //Set correctness listener
                floatAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                if(a.isFilled()){
                    floatAnswer.setText(Float.toString(((FloatAnswer)a).getAnswer()));
                }
                else{
                    floatAnswer.setText("");
                }
                fieldName.setText(a.getField().getTextName());
                floatAnswer.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.FLOAT, this));
                applicable.setChecked(!a.isApplicable());

                break;
            case POSITIVE_FLOAT:
                fieldView = inflater.inflate(R.layout.my_text_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.textView);
                EditText posFloatAnswer = (EditText) fieldView.findViewById(R.id.textEditText);
                //Set correctness listener
                posFloatAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                if(a.isFilled()){
                    posFloatAnswer.setText(Float.toString(((PositiveFloatAnswer)a).getAnswer()));
                }
                else{
                    posFloatAnswer.setText("");
                }
                fieldName.setText(a.getField().getTextName());
                posFloatAnswer.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.POSITIVE_FLOAT, this));
                applicable.setChecked(!a.isApplicable());

                break;
            case NEGATIVE_FLOAT:
                fieldView = inflater.inflate(R.layout.my_text_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.textView);
                EditText negFloatAnswer = (EditText) fieldView.findViewById(R.id.textEditText);
                //Set correctness listener
                negFloatAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                if(a.isFilled()){
                    negFloatAnswer.setText(Float.toString(((NegativeFloatAnswer)a).getAnswer()));
                }
                else{
                    negFloatAnswer.setText("");
                }
                fieldName.setText(a.getField().getTextName());
                negFloatAnswer.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);

                negFloatAnswer.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(!hasFocus) {
                            EditText ed = (EditText) v;
                            String text = ed.getText().toString();
                            int length = text.length();
                            if (!text.matches("^-[0-9]*.?[0-9]+$") && length > 0) {
                                Toast.makeText(getActivity(), getActivity().getString(R.string.wrong_input) + text + "' !", Toast.LENGTH_SHORT).show();
                                ed.getText().delete(0, length);
                            }
                        }
                    }
                });
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.NEGATIVE_FLOAT, this));
                applicable.setChecked(!a.isApplicable());

                break;
            case MEASURE:
                fieldView = inflater.inflate(R.layout.my_measure_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.measureTextView);
                fieldName.setText(a.getField().getTextName());
                EditText measureAnswer = (EditText) fieldView.findViewById(R.id.measureEditText);
                measureAnswer.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                Spinner spinner = (Spinner) fieldView.findViewById(R.id.measureSpinner);
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.measures_um, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

                spinner.setSelection(adapter.getPosition(((MeasureAnswer)a).getUm()));
                //Set correctness listener
                measureAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                if(a.isFilled()){
                    measureAnswer.setText(Float.toString(((MeasureAnswer)a).getAnswer()));
                }
                else{
                    measureAnswer.setText("");
                }
                spinner.setOnItemSelectedListener(new SpinnerListener(spinner.getSelectedItemPosition()));
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.MEASURE, this));
                applicable.setChecked(!a.isApplicable());

                break;
            case ISTAT_CODE:
                fieldView = inflater.inflate(R.layout.my_text_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.textView);
                EditText istatAnswer = (EditText) fieldView.findViewById(R.id.textEditText);
                //Set correctness listener
                istatAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                if(a.isFilled()){
                    istatAnswer.setText(((IstatAnswer)a).getAnswer());
                }
                else{
                    istatAnswer.setText("");
                }
                fieldName.setText(a.getField().getTextName());
                istatAnswer.setInputType(InputType.TYPE_CLASS_NUMBER);
                int maxLength = 3;
                istatAnswer.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.ISTAT_CODE, this));
                applicable.setChecked(!a.isApplicable());

                break;

            case GPS:
                fieldView = inflater.inflate(R.layout.my_gps_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.gpsTextView);
                fieldName.setText(a.getField().getTextName());
                TextView gpsDisplay = (TextView) fieldView.findViewById(R.id.gpsDisplayPosition);
                ImageButton getPosition = (ImageButton) fieldView.findViewById(R.id.gpsGetPosition);
                final ProgressDialog progDiag = new ProgressDialog(getActivity());
                final GPSListener locationListener = new GPSListener(getActivity(), fieldView, progDiag);
                getPosition.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                        progDiag.setMessage("Localizzazione GPS in corso...");
                        progDiag.setTitle("Localizzazione GPS");
                        progDiag.show();
                    }
                });
                gpsDisplay.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                if(a.isFilled()){
                    GpsAnswer gpsAns = (GpsAnswer)a;
                    gpsDisplay.setText(gpsAns.getLat() + " " + gpsAns.getLon() + " " + gpsAns.getPrec());
                }
                else{
                    gpsDisplay.setText("");
                }

                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.GPS, this));
                applicable.setChecked(!a.isApplicable());

                break;
            case DATE:
                fieldView = inflater.inflate(R.layout.my_date_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.textDateView);
                MyDatePicker dateAnswer = (MyDatePicker) fieldView.findViewById(R.id.datePicker);
                dateAnswer.setSpinnersShown(false);

                //Set correctness listener (not necessary)
                //dateAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                Calendar cal = Calendar.getInstance();
                if(a.isFilled()){
                    Date date = ((DateAnswer) a).getAnswer();
                    cal.setTime(date);
                    dateAnswer.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), null);
                }
                else{
                    dateAnswer.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), null);
                }
                fieldName.setText(a.getField().getTextName());
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.DATE, this));
                applicable.setChecked(!a.isApplicable());
                break;
            case DATE_TIME:
                fieldView = inflater.inflate(R.layout.my_date_time_view, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.textDateView);
                MyDatePicker dateFieldAnswer = (MyDatePicker) fieldView.findViewById(R.id.datePicker);
                dateFieldAnswer.setSpinnersShown(false);
                TimePicker timeFieldAnswer = (TimePicker) fieldView.findViewById(R.id.timePicker);

                //Set correctness listener (not necessary)
                //dateAnswer.addTextChangedListener(new CorrectnessListener(fieldView, this, a.getType()));
                Calendar calTime = Calendar.getInstance();
                if(a.isFilled()){
                    Date date = ((DateAnswer) a).getAnswer();
                    calTime.setTime(date);
                    dateFieldAnswer.init(calTime.get(Calendar.YEAR), calTime.get(Calendar.MONTH), calTime.get(Calendar.DAY_OF_MONTH), null);
                    timeFieldAnswer.setCurrentHour(calTime.get(Calendar.HOUR));
                    timeFieldAnswer.setCurrentMinute(calTime.get(Calendar.MINUTE));
                }
                else{
                    dateFieldAnswer.init(calTime.get(Calendar.YEAR), calTime.get(Calendar.MONTH), calTime.get(Calendar.DAY_OF_MONTH), null);
                    timeFieldAnswer.setCurrentHour(calTime.get(Calendar.HOUR));
                    timeFieldAnswer.setCurrentMinute(calTime.get(Calendar.MINUTE));
                }
                fieldName.setText(a.getField().getTextName());
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.DATE_TIME, this));
                applicable.setChecked(!a.isApplicable());
                break;
            case SELECT:
                fieldView = inflater.inflate(R.layout.my_check_group, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.checkTextView);
                fieldName.setText(a.getField().getTextName());

                final LinearLayout checkGroup = (LinearLayout) fieldView.findViewById(R.id.checkGroup);
                LinearLayout otherCheck = (LinearLayout) checkGroup.findViewById(R.id.otherLinearLayout);
                checkGroup.removeAllViews();
                ArrayList<Integer> checkIds = new ArrayList<>();


                for(int i=0; i<a.getField().getChoices().size(); i++){
                    String str = a.getField().getChoices().get(i);
                    if(a.getField().getOther().get(i)){
                        checkGroup.addView(otherCheck);
                        CheckBox checkOther = (CheckBox) otherCheck.findViewById(R.id.otherCheckBox);
                        checkOther.setTextSize(20);
                        checkOther.setText(str);
                        if(((SelectAnswer)a).getAnswer(i)!=null){
                            EditText otherText = (EditText) otherCheck.findViewById(R.id.otherText);
                            ((CheckBox) otherCheck.findViewById(R.id.otherCheckBox)).setChecked(true);
                            otherText.setText(((SelectAnswer) a).getAnswer(i));
                        }
                        checkOther.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                ContentFragment.this.correctnessChecking((View) buttonView.getParent().getParent().getParent(), DataType.SELECT);
                            }
                        });
                        //TODO:modify!
                        //checkOther.setOnCheckedChangeListener(this);
                    }
                    else{
                        CheckBox checkBox = new CheckBox(this.getActivity());
                        checkBox.setText(str);
                        checkBox.setTextSize(20);
                        checkBox.setId(Util.generateViewId());
                        checkIds.add(checkBox.getId());
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                        );

                        checkGroup.addView(checkBox, i, params);
                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                ContentFragment.this.correctnessChecking((View) buttonView.getParent().getParent(), DataType.SELECT);
                            }
                        });
                        if(a.isFilled() && (((SelectAnswer)a).getAnswer(i))!=null){
                            //if((((SelectAnswer)a).getAnswer(i)).equals(str)){
                                checkBox.setChecked(true);
                            //}
                        }
                    }
                    ((SelectAnswer)a).setViewIds(checkIds);

                }
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.SELECT, this));
                applicable.setChecked(!a.isApplicable());
                //set imageView initial state
                ImageView imgCheck = (ImageView) fieldView.findViewById(R.id.fieldImageView);
                if(!a.isFilled()){
                    imgCheck.setImageResource(R.drawable.ic_action_remove);
                }
                break;
            case SELECT1:
                boolean noMatching = true;
                int idOther = 0;
                int idBtn = 0;
                fieldView = inflater.inflate(R.layout.my_radio_group, null);
                fieldView.setId(Util.generateViewId());
                fieldName = (TextView) fieldView.findViewById(R.id.radioTextView);
                fieldName.setText(a.getField().getTextName());
                RadioGroup group = (RadioGroup) fieldView.findViewById(R.id.radioGroup);

                LinearLayout otherLayout = (LinearLayout) fieldView.findViewById(R.id.otherLinearLayout);
                //otherLayout.setVisibility(View.INVISIBLE);
                group.removeAllViews();

                for(int i=0; i<a.getField().getChoices().size(); i++){
                    String str = a.getField().getChoices().get(i);
                    if(a.getField().getOther().get(i)){
                        group.addView(otherLayout);
                        RadioButton btnOther = (RadioButton) otherLayout.findViewById(R.id.otherRadio);
                        btnOther.setTextSize(20);
                        btnOther.setText(str);
                        btnOther.setOnCheckedChangeListener(this);
                    }
                    else{
                        RadioButton radioBtn = new RadioButton(this.getActivity());
                        radioBtn.setText(str);
                        radioBtn.setTextSize(20);
                        radioBtn.setId(Util.generateViewId());

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                        );

                        group.addView(radioBtn, i, params);
                        if(a.isFilled() && (((Select1Answer)a).getAnswer()).equals(str)){
                            group.check(radioBtn.getId());
                            noMatching = false;
                            otherLayout.findViewById(R.id.otherText).setEnabled(false);
                        }
                    }

                }
                //assuming only one button with free text!
                if(noMatching && a.isFilled()){
                    EditText otherText = (EditText) otherLayout.findViewById(R.id.otherText);
                    ((RadioButton) otherLayout.findViewById(R.id.otherRadio)).setChecked(true);
                    otherText.setText(((Select1Answer) a).getAnswer());
                }
                group.setOnCheckedChangeListener(this);
                //Set applicable listener
                applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
                applicable.setOnCheckedChangeListener(new ApplicableListener(fieldView, DataType.SELECT1, this));
                applicable.setChecked(!a.isApplicable());
                //set imageView initial state
                ImageView imgCorrect = (ImageView) fieldView.findViewById(R.id.fieldImageView);
                if(!a.isFilled()){
                    imgCorrect.setImageResource(R.drawable.ic_action_remove);
                }
                break;

            case SKETCH:
                break;

        }
        //highlight mandatoriness of a field
        if(fieldName!=null){
            if(a.getField().isRequired()){
                String text = fieldName.getText().toString();
                text = text.concat("*");
                fieldName.setText(text);
            }
        }
        return fieldView;
    }

    private void saveAnswer(View fieldView, Answer a){
        switch (a.getType()){
            case INTEGER:
                EditText intAns = (EditText) fieldView.findViewById(R.id.textEditText);
                IntegerAnswer intAnswer = ((IntegerAnswer)a);
                if(intAns.getText().toString().equals("")){
                    intAnswer.setAnswer(0);
                    intAnswer.setFilled(false);
                }
                else{
                    intAnswer.setAnswer(Integer.parseInt(intAns.getText().toString()));
                    intAnswer.setFilled(true);
                }
                break;
            case POSITIVE_INTEGER:
                EditText intPosAns = (EditText) fieldView.findViewById(R.id.textEditText);
                PositiveIntegerAnswer intPosAnswer = ((PositiveIntegerAnswer)a);
                if(intPosAns.getText().toString().equals("")){
                    intPosAnswer.setAnswer(0);
                    intPosAnswer.setFilled(false);
                }
                else{
                    intPosAnswer.setAnswer(Integer.parseInt(intPosAns.getText().toString()));
                    intPosAnswer.setFilled(true);
                }
                break;
            case NEGATIVE_INTEGER:
                EditText intNegAns = (EditText) fieldView.findViewById(R.id.textEditText);
                NegativeIntegerAnswer intNegAnswer = ((NegativeIntegerAnswer)a);
                if(intNegAns.getText().toString().equals("")){
                    intNegAnswer.setAnswer(0);
                    intNegAnswer.setFilled(false);
                }
                else{
                    intNegAnswer.setAnswer(Integer.parseInt(intNegAns.getText().toString()));
                    intNegAnswer.setFilled(true);
                }
                break;
            case FLOAT:
                EditText floatAns = (EditText) fieldView.findViewById(R.id.textEditText);
                FloatAnswer floatAnswer = (FloatAnswer) a;
                if(floatAns.getText().toString().equals("")){
                    floatAnswer.setAnswer(0);
                    floatAnswer.setFilled(false);
                }
                else{
                    floatAnswer.setAnswer(Float.parseFloat(floatAns.getText().toString()));
                    floatAnswer.setFilled(true);
                }
                break;
            case POSITIVE_FLOAT:
                EditText posFloatAns = (EditText) fieldView.findViewById(R.id.textEditText);
                PositiveFloatAnswer posFloatAnswer = (PositiveFloatAnswer) a;
                if(posFloatAns.getText().toString().equals("")){
                    posFloatAnswer.setAnswer(0);
                    posFloatAnswer.setFilled(false);
                }
                else{
                    posFloatAnswer.setAnswer(Float.parseFloat(posFloatAns.getText().toString()));
                    posFloatAnswer.setFilled(true);
                }
                break;
            case NEGATIVE_FLOAT:
                EditText negFloatAns = (EditText) fieldView.findViewById(R.id.textEditText);
                NegativeFloatAnswer negFloatAnswer = (NegativeFloatAnswer) a;
                if(negFloatAns.getText().toString().equals("") || !negFloatAns.getText().toString().matches("^-[0-9]*.?[0-9]+$")){
                    //TODO: verify correct application of regex
                    negFloatAnswer.setAnswer(0);
                    negFloatAnswer.setFilled(false);
                }
                else{
                    negFloatAnswer.setAnswer(Float.parseFloat(negFloatAns.getText().toString()));
                    negFloatAnswer.setFilled(true);
                }
                break;
            case MEASURE:
                EditText measureAns = (EditText) fieldView.findViewById(R.id.measureEditText);
                Spinner spin = (Spinner) fieldView.findViewById(R.id.measureSpinner);
                MeasureAnswer measureAnswer = (MeasureAnswer) a;
                measureAnswer.setUm(((String) spin.getAdapter().getItem(spin.getSelectedItemPosition())));
                if(measureAns.getText().toString().equals("")){
                    measureAnswer.setAnswer(0);
                    measureAnswer.setFilled(false);
                }
                else{
                    measureAnswer.setAnswer(Float.parseFloat(measureAns.getText().toString()));
                    measureAnswer.setFilled(true);
                }
                break;
            case ISTAT_CODE:
                EditText istatAns = (EditText) fieldView.findViewById(R.id.textEditText);
                IstatAnswer istatAnswer = ((IstatAnswer)a);
                if(istatAns.getText().toString().equals("")){
                    istatAnswer.setAnswer("");
                    istatAnswer.setFilled(false);
                    //TODO: verify correct application of the regex
                }
                else{
                    istatAnswer.setAnswer(istatAns.getText().toString());
                    istatAnswer.setFilled(true);
                }
                break;
            case GPS:
                TextView gpsAns = (TextView) fieldView.findViewById(R.id.gpsDisplayPosition);
                GpsAnswer gpsAnswer = ((GpsAnswer)a);
                if(gpsAns.getText().toString().equals("")){
                    gpsAnswer.setLat("0");
                    gpsAnswer.setLon("0");
                    gpsAnswer.setPrec("0");
                    gpsAnswer.setFilled(false);
                }
                else{
                    gpsAnswer.setGpsFromConcatSpace(gpsAns.getText().toString());
                    gpsAnswer.setFilled(true);
                }
                break;
            case DATE:
                MyDatePicker picker = (MyDatePicker) fieldView.findViewById(R.id.datePicker);
                Calendar cal = Calendar.getInstance();
                cal.set(picker.getYear(), picker.getMonth(), picker.getDayOfMonth());
                DateAnswer dateAns = (DateAnswer)a;
                dateAns.setAnswer(cal.getTime());
                dateAns.setFilled(true);
                break;
            case DATE_TIME:
                MyDatePicker datePicker = (MyDatePicker) fieldView.findViewById(R.id.datePicker);
                TimePicker timePicker = (TimePicker) fieldView.findViewById(R.id.timePicker);
                Calendar timeCal = Calendar.getInstance();
                timeCal.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), timePicker.getCurrentHour(), timePicker.getCurrentMinute());
                DateAnswer dateTimeAns = (DateAnswer)a;
                dateTimeAns.setAnswer(timeCal.getTime());
                dateTimeAns.setFilled(true);
                break;
            case SELECT:
                LinearLayout checkGroup = (LinearLayout) fieldView.findViewById(R.id.checkGroup);
                ArrayList<Integer> ids = ((SelectAnswer)a).getViewIds();
                SelectAnswer ansSel = (SelectAnswer)a;
                ansSel.resetAnswer();
                int index = 0;
                for(Integer i: ids){
                    CheckBox c = (CheckBox) checkGroup.findViewById(i);
                    if(c.isChecked()){
                        ansSel.putAnswer(index, c.getText().toString());
                        ansSel.setFilled(true);
                    }
                    index++;
                }
                LinearLayout otherCheck = (LinearLayout) fieldView.findViewById(R.id.otherLinearLayout);
                if(otherCheck!=null){
                    CheckBox otherBox = (CheckBox) otherCheck.findViewById(R.id.otherCheckBox);
                    if(otherBox.isChecked()){
                        EditText editOther = (EditText) otherCheck.findViewById(R.id.otherText);
                        ansSel.putAnswer(index, editOther.getText().toString());
                        ansSel.setFilled(true);
                    }
                }
                break;
            case SELECT1:
                RadioGroup group = (RadioGroup) fieldView.findViewById(R.id.radioGroup);
                int checkedRadioButtonId = group.getCheckedRadioButtonId();
                RadioButton btn = (RadioButton)group.findViewById(checkedRadioButtonId);
                Select1Answer ans = ((Select1Answer)a);
                if(btn!=null){
                    ans.setAnswer(btn.getText().toString());
                    ans.setFilled(true);
                }
                else{
                    LinearLayout otherLayout = (LinearLayout) group.findViewById(R.id.otherLinearLayout);
                    if(otherLayout!=null){
                        RadioButton otherBtn = (RadioButton) otherLayout.findViewById(R.id.otherRadio);
                        if(otherBtn.isChecked()){
                            EditText otherTxt = (EditText) otherLayout.findViewById(R.id.otherText);
                            ans.setAnswer(otherTxt.getText().toString());
                            ans.setFilled(true);
                        }
                        else{
                            ans.setFilled(false);
                        }
                    }
                    else{
                        ans.setFilled(false);
                    }

                }
                break;
            case STRING:
                EditText stringAns = (EditText) fieldView.findViewById(R.id.textEditText);
                StringAnswer answer = ((StringAnswer)a);
                if(stringAns.getText().toString().equals("")){
                    answer.setAnswer("");
                    answer.setFilled(false);
                }
                else{
                    answer.setAnswer(stringAns.getText().toString());
                    answer.setFilled(true);
                }
                break;
            case SKETCH:
                break;
        }

        //set applicability for saving purposes
        boolean applicableInput = !((CheckBox)fieldView.findViewById(R.id.applicableCheckBox)).isChecked();
        a.setApplicable(applicableInput);

    }

    @Override
    public void onPause(){
        if(invalidated){
            super.onPause();
            return;
        }
        //save all!
        try{
            for (int i=0; i<questionInstance.getAnswers().size(); i++){
                View fieldView = getView().findViewById(connection.get(i));
                saveAnswer(fieldView, questionInstance.getAnswer(i));
            }
            LinearLayout noteLayout = (LinearLayout) getActivity().findViewById(R.id.notesLayout);

            for(int i = 0; i<questionInstance.getNotes().size(); i++){
                LinearLayout l = (LinearLayout) noteLayout.getChildAt(i);
                EditText e = (EditText) l.findViewById(R.id.noteEditText);
                questionInstance.getNotes().get(i).setNote(e.getText().toString());
            }
            this.saveActivity.saveContent(questionInstance);
            //remove all the views from the content
            LinearLayout content = (LinearLayout) getView().findViewById(R.id.contentLayout);
            content.removeAllViews();
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }

        super.onPause();
    }

    /**
     * <p>Called when the checked radio button has changed. When the
     * selection is cleared, checkedId is -1.</p>
     *
     * @param group     the group in which the checked radio button has changed
     * @param checkedId the unique identifier of the newly checked radio button
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(checkedId!=-1){
            getActivity().findViewById(R.id.contentLayout).requestFocus();
            RadioButton pippo = (RadioButton) group.findViewById(R.id.otherRadio);
            if(pippo!=null){
                pippo.setChecked(false);
                EditText text = (EditText) group.findViewById(R.id.otherText);
                text.setText("");
                text.setEnabled(false);
            }
        }
        ImageView imgCorrect = (ImageView) ((LinearLayout) group.getParent()).findViewById(R.id.fieldImageView);
        imgCorrect.setImageResource(R.drawable.ic_action_accept);
        correctnessChecking(((LinearLayout) group.getParent()), DataType.SELECT1);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        getActivity().findViewById(R.id.contentLayout).requestFocus();
        if(isChecked){
            RadioGroup parent = (RadioGroup) ((ViewParent) buttonView.getParent()).getParent();
            LinearLayout layout = (LinearLayout) buttonView.getParent();
            EditText text = (EditText)layout.findViewById(R.id.otherText);
            parent.clearCheck();
            text.setEnabled(true);
            //text.requestFocus();
            buttonView.setChecked(true);
        }
        correctnessChecking(((LinearLayout) buttonView.getParent().getParent().getParent()), DataType.SELECT1);
    }


    public void correctnessChecking(View fieldView, DataType type){
        ImageView image = (ImageView) fieldView.findViewById(R.id.fieldImageView);
        CheckBox applicable = (CheckBox) fieldView.findViewById(R.id.applicableCheckBox);
        boolean applicableInput = applicable.isChecked();
        
        switch (type){
            case STRING:
            case INTEGER:
            case POSITIVE_INTEGER:
            case FLOAT:
            case POSITIVE_FLOAT:
                EditText txt = (EditText) fieldView.findViewById(R.id.textEditText);
                if(applicableInput){
                    image.setImageResource(R.drawable.ic_action_warning);
                }
                else{
                    if(txt.getText().toString().equals("")){
                        image.setImageResource(R.drawable.ic_action_remove);
                    }
                    else{
                        image.setImageResource(R.drawable.ic_action_accept);
                    }
                }
                break;
            case MEASURE:
                EditText measureTxt = (EditText) fieldView.findViewById(R.id.measureEditText);
                if(applicableInput){
                    image.setImageResource(R.drawable.ic_action_warning);
                }
                else{
                    if(measureTxt.getText().toString().equals("")){
                        image.setImageResource(R.drawable.ic_action_remove);
                    }
                    else{
                        image.setImageResource(R.drawable.ic_action_accept);
                    }
                }
                break;
            case NEGATIVE_INTEGER:
                EditText negInt = (EditText) fieldView.findViewById(R.id.textEditText);
                if(applicableInput){
                    image.setImageResource(R.drawable.ic_action_warning);
                }
                else{
                    if(!negInt.getText().toString().matches("^-[0-9]*")){
                        image.setImageResource(R.drawable.ic_action_remove);
                    }
                    else{
                        image.setImageResource(R.drawable.ic_action_accept);
                    }
                }
                break;
            case NEGATIVE_FLOAT:
                EditText negFloat = (EditText) fieldView.findViewById(R.id.textEditText);
                if(applicableInput){
                    image.setImageResource(R.drawable.ic_action_warning);
                }
                else{
                    if(!negFloat.getText().toString().matches("^-[0-9]*.?[0-9]+$")){
                        image.setImageResource(R.drawable.ic_action_remove);
                    }
                    else{
                        image.setImageResource(R.drawable.ic_action_accept);
                    }
                }
                break;
            case ISTAT_CODE:
                EditText istat = (EditText) fieldView.findViewById(R.id.textEditText);
                if(applicableInput){
                    image.setImageResource(R.drawable.ic_action_warning);
                }
                else{
                    if(!(istat.getText().toString().length()==3)){
                        image.setImageResource(R.drawable.ic_action_remove);
                    }
                    else{
                        image.setImageResource(R.drawable.ic_action_accept);
                    }
                }
                break;
            case GPS:
                TextView gps = (TextView) fieldView.findViewById(R.id.gpsDisplayPosition);
                if(applicableInput){
                    image.setImageResource(R.drawable.ic_action_warning);
                }
                else{
                    if(gps.getText().toString().equals("")){
                        image.setImageResource(R.drawable.ic_action_remove);
                    }
                    else{
                        image.setImageResource(R.drawable.ic_action_accept);
                    }
                }
                break;
            case DATE:
            case DATE_TIME:
                if(applicableInput){
                    image.setImageResource(R.drawable.ic_action_warning);
                }
                else{
                    image.setImageResource(R.drawable.ic_action_accept);
                }
                break;
            case SELECT:
                if(applicableInput){
                    image.setImageResource(R.drawable.ic_action_warning);
                }
                else{
                    LinearLayout l = (LinearLayout) fieldView.findViewById(R.id.checkGroup);
                    boolean end = false;
                    for(int i=0; i<l.getChildCount();i++){
                        if(l.getChildAt(i) instanceof CheckBox){
                            if(((CheckBox)l.getChildAt(i)).isChecked()){
                                image.setImageResource(R.drawable.ic_action_accept);
                                end=true;
                            }
                        }
                        else if(l.getChildAt(i) instanceof LinearLayout){
                            LinearLayout other = (LinearLayout) l.getChildAt(i);
                            if(((CheckBox)other.findViewById(R.id.otherCheckBox)).isChecked()){
                                image.setImageResource(R.drawable.ic_action_accept);
                                end=true;
                            }
                        }
                    }
                    if(!end){
                        image.setImageResource(R.drawable.ic_action_remove);
                    }
                }
                break;
            case SELECT1:
                RadioGroup group = (RadioGroup) fieldView.findViewById(R.id.radioGroup);
                int checkedRadioButtonId = group.getCheckedRadioButtonId();
                RadioButton btn = (RadioButton)group.findViewById(checkedRadioButtonId);
                if(applicableInput){
                    image.setImageResource(R.drawable.ic_action_warning);
                }
                else{
                    if(btn!=null){
                        image.setImageResource(R.drawable.ic_action_accept);
                    }
                    else{
                        LinearLayout otherLayout = (LinearLayout) group.findViewById(R.id.otherLinearLayout);
                        if(otherLayout!=null){
                            RadioButton otherBtn = (RadioButton) otherLayout.findViewById(R.id.otherRadio);
                            if(otherBtn.isChecked()){
                                //EditText otherTxt = (EditText) otherLayout.findViewById(R.id.otherText);
                                image.setImageResource(R.drawable.ic_action_accept);
                            }
                            else{
                                image.setImageResource(R.drawable.ic_action_remove);
                            }
                        }
                        else{
                            image.setImageResource(R.drawable.ic_action_remove);
                        }

                    }
                }
                break;
            case SKETCH:
                break;
        }
    }

    public void invalidateFragment(boolean invalidate){
        this.invalidated = invalidate;
    }


}

