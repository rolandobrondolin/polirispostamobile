package it.polirisposta.mobile.poliRISPOSTA.form.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.FormInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.QuestionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.SectionInstance;
import it.polirisposta.mobile.poliRISPOSTA.util.Util;

/**
 * Created by Rolando on 30/03/15.
 */
public class EmptyContentFragment extends Fragment {
    public static final String TAG = "ContentFragmentEmptyView";
    private int formInstanceId;
    private ListFragment.DataRetriever retriever;
    private FormInstance instance;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.retriever = (ListFragment.DataRetriever) activity;

    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        formInstanceId = args.getInt("instanceId");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_fragment_start, container, false);

        LinearLayout scrollLayout = (LinearLayout) rootView.findViewById(R.id.statLayout);

        instance = retriever.getInstanceData();

        for(SectionInstance sInst : instance.getSections()){
            String title = sInst.getSection().getTextName();
            if(sInst.getIndex()>1){
                title = title.concat(" (" + sInst.getIndex() + ")");
            }
            String description = sInst.getSection().getDescription();

            int total = 0;
            int answered = 0;
            int notAnswered = 0;
            int notApplicable = 0;
            int images = 0;
            int notes = 0;

            int mandatoryAnswered = 0;
            int mandatoryNotAnswered = 0;
            int mandatoryNotApplicable = 0;

            for(QuestionInstance qInst: sInst.getQuestionInstances()){
                images += qInst.getPictures().size();
                notes += qInst.getNotes().size();
                //TODO: missing note stats

                for(Answer a: qInst.getAnswers()){
                    total++;
                    if(a.isApplicable()){
                        if(a.isFilled()){
                            answered++;
                            if(a.getField().isRequired()){
                                mandatoryAnswered++;
                            }
                        }
                        else{
                            notAnswered++;
                            if(a.getField().isRequired()){
                                mandatoryNotAnswered++;
                            }
                        }
                    }
                    else{
                        notApplicable++;
                        if(a.getField().isRequired()){
                            mandatoryNotApplicable++;
                        }
                    }
                }
            }

            //init graphics
            RelativeLayout stat = (RelativeLayout) inflater.inflate(R.layout.stat_section, null);
            stat.setId(Util.generateViewId());

            //check and set correctness!

            ImageView icon = (ImageView) stat.findViewById(R.id.imageView);
            if(notAnswered == 0){
                if(notApplicable==0){
                    icon.setImageResource(R.drawable.ic_action_accept);
                }
                else{
                    icon.setImageResource(R.drawable.ic_action_warning);
                }
            }
            else{
                icon.setImageResource(R.drawable.ic_action_remove);
            }

            TextView v;
            //set title and description
            v = (TextView) stat.findViewById(R.id.sectionText);
            v.setText(title);

            v = (TextView) stat.findViewById(R.id.sectionDescription);
            if(description.equals("")){
                v.setVisibility(View.INVISIBLE);
                v.setHeight(0);
            }
            else{
                v.setText(description);
            }

            v = (TextView) stat.findViewById(R.id.answeredField);
            v.setText(Integer.toString(answered));
            v = (TextView) stat.findViewById(R.id.notAnsweredField);
            v.setText(Integer.toString(notAnswered));
            v = (TextView) stat.findViewById(R.id.notApplicableField);
            v.setText(Integer.toString(notApplicable));

            v = (TextView) stat.findViewById(R.id.answeredMandatoryField);
            v.setText(Integer.toString(mandatoryAnswered));
            v = (TextView) stat.findViewById(R.id.notAnsweredMandatoryField);
            v.setText(Integer.toString(mandatoryNotAnswered));
            v = (TextView) stat.findViewById(R.id.notApplicableMandatoryField);
            v.setText(Integer.toString(mandatoryNotApplicable));
            scrollLayout.addView(stat);

            v = (TextView) stat.findViewById(R.id.imagesField);
            v.setText(Integer.toString(images));

            v = (TextView) stat.findViewById(R.id.notesField);
            v.setText(Integer.toString(notes));
        }

        return rootView;
    }


}
