package it.polirisposta.mobile.poliRISPOSTA.form.activity;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.asynctask.RepeatQuestionInstanceTask;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.asynctask.RepeatSectionInstanceTask;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.asynctask.SaveAsyncTask;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.asynctask.SetupDB;
import it.polirisposta.mobile.poliRISPOSTA.model.Note;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.Resource;
import it.polirisposta.mobile.poliRISPOSTA.model.SectionInstance;
import it.polirisposta.mobile.poliRISPOSTA.search.activity.SearchActivity;
import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.FormInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.QuestionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;


public class FormActivity extends ActionBarActivity implements ListFragment.OnQuestionSelectedListener, ListFragment.DataRetriever, ContentFragment.SaveContent, ContentFragment.OnRetrieveQuestion, ListFragment.ContextMenuQuestionModifier, SetupDB.LoadCallback {
    private FormInstance instance;
    public static final String TAG = "poliRisposta";
    private int formID = 1;
    private int formInstanceID = 1;
    private int sectionInstanceId = 0;
    private int questionInstanceId = 0;
    private int searchSectionID;
    private int searchQuestionID;
    private boolean editFlag = false;
    private boolean empty;
    private SetupDB setupTask;
    private boolean fromSearch;

    public FormActivity(){
        super();
        this.formID=1;
        this.formInstanceID=1;
        editFlag=false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        Integer arrayParam[] = new Integer[3];
        if (savedInstanceState == null) {
            Intent i = getIntent();
            int id = i.getIntExtra("id", -1);
            int taskID = i.getIntExtra("taskID", -1);
            editFlag = i.getBooleanExtra("edit", false);

            arrayParam[0] = editFlag?1:0;
            arrayParam[1] = id;
            arrayParam[2] = taskID;

            setupTask = new SetupDB(this, this);
            setupTask.execute(arrayParam);

            fromSearch = i.getBooleanExtra("fromSearch", false);
            if(fromSearch){
                searchSectionID = i.getIntExtra("sectionInstanceID", -1);
                searchQuestionID = i.getIntExtra("questionInstanceID", -1);
            }

        }
        else {
            empty = savedInstanceState.getBoolean("emptyFragment");
            formID = savedInstanceState.getInt("formId");
            formInstanceID = savedInstanceState.getInt("formInstanceId");
            if (!empty) {
                sectionInstanceId = savedInstanceState.getInt("sectionId");
                questionInstanceId = savedInstanceState.getInt("questionId");
            }
            instance = (FormInstance) savedInstanceState.getSerializable("instance");
            if(!empty){
                onQuestionSelected(sectionInstanceId,questionInstanceId);
            }
        }
    }

    @Override
    public void loadCompletedCallback(){
        try {
            instance = setupTask.get();
            formInstanceID = instance.getId();
            formID = instance.getForm().getId();

            ListFragment list = new ListFragment();

            if(fromSearch){
                if(searchSectionID==-1){
                    questionInstanceId = instance.findQuestionFromPK(searchQuestionID);
                    sectionInstanceId = instance.findSectionFromQuestionPK(searchQuestionID);
                }
                else{
                    sectionInstanceId = instance.findSectionFromPK(searchSectionID);
                    questionInstanceId = 0;
                }

                FragmentManager manager = getFragmentManager();
                ContentFragment content = new ContentFragment();
                Bundle qBundle = new Bundle();
                qBundle.putInt("sectionId", sectionInstanceId);
                qBundle.putInt("questionId", questionInstanceId);
                content.setArguments(qBundle);
                Bundle listBundle = new Bundle();
                listBundle.putInt("sectionId", sectionInstanceId);
                listBundle.putInt("questionId", questionInstanceId);
                list.setArguments(qBundle);
                manager.beginTransaction().add(R.id.list, list, ListFragment.TAG)
                        .add(R.id.container, content, ContentFragment.TAG).commit();

                empty = false;
            }
            else{
                EmptyContentFragment emptyContent = new EmptyContentFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("instanceId", instance.getId());
                emptyContent.setArguments(bundle);

                getFragmentManager().beginTransaction()
                        .add(R.id.list, list, ListFragment.TAG)
                        .add(R.id.container, emptyContent, EmptyContentFragment.TAG)
                        .commit();
                empty = true;
            }

            this.setTitle(instance.getForm().getTextName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onResume(){
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.formSearch).getActionView();

        ComponentName cn = new ComponentName(this, SearchActivity.class);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_home) {
            if(!empty){
                sectionInstanceId = 0;
                questionInstanceId = 0;
                EmptyContentFragment emptyContent = new EmptyContentFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("instanceID", instance.getId());
                emptyContent.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .remove(getFragmentManager().findFragmentByTag(ContentFragment.TAG))
                        .add(R.id.container, emptyContent, EmptyContentFragment.TAG)
                        .commit();
                empty = true;
                return true;
            }
        }
        if(id== R.id.formSearch){
            onSearchRequested();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void startActivity(Intent intent) {
        // check if search intent
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            intent.putExtra("instanceID", formInstanceID);
            intent.putExtra("taskID", instance.getTaskId());

        }

        super.startActivity(intent);
    }

    @Override
    public void onQuestionSelected(int sectionInstanceId, int questionInstanceId){
        this.questionInstanceId = questionInstanceId;
        this.sectionInstanceId = sectionInstanceId;
        FragmentManager manager = getFragmentManager();
        ContentFragment content = new ContentFragment();
        Bundle qBundle = new Bundle();
        qBundle.putInt("sectionId", sectionInstanceId);
        qBundle.putInt("questionId", questionInstanceId);
        content.setArguments(qBundle);
        if(empty){
            empty=false;
            manager.beginTransaction().remove(manager.findFragmentByTag(EmptyContentFragment.TAG))
                    .add(R.id.container, content, ContentFragment.TAG).commit();
        }
        else{
            manager.beginTransaction().remove(manager.findFragmentByTag(ContentFragment.TAG))
                    .add(R.id.container, content, ContentFragment.TAG).commit();

            QuestionInstance qInst = instance.getQuestionInstance(sectionInstanceId, questionInstanceId);
            for(Answer a : qInst.getAnswers()){
                statSub(a);
            }
        }
    }

    @Override
    public FormInstance getInstanceData() {
        return instance;
    }

    @Override
    //callback used to store the fields coming from the content fragment in the database
    public void saveContent(QuestionInstance qInst) {
        for(Answer a : qInst.getAnswers()){
           statAdd(a);
        }
        saveStat();
        SectionInstance sInst = instance.getSections().get(this.sectionInstanceId);
        SaveAsyncTask save = new SaveAsyncTask(this, sInst, qInst, this.questionInstanceId, this.sectionInstanceId);
        save.execute();
    }

    @Override
    public QuestionInstance retrieveQuestion(int sectionId, int questionId) {
        QuestionInstance qInst = instance.getQuestionInstance(sectionId, questionId);
        return qInst;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //TODO: save the open tabs in the list view
        outState.putBoolean("emptyFragment", empty);
        outState.putInt("formInstanceId", this.formInstanceID);
        outState.putInt("formId", this.formID);
        outState.putSerializable("instance", this.instance);
        if(!empty){
            outState.putInt("questionId", this.questionInstanceId);
            outState.putInt("sectionId", this.sectionInstanceId);
        }
        super.onSaveInstanceState(outState);

    }


    @Override
    public void resetElement(int sectionInstanceId, int questionInstanceId){
        if(questionInstanceId!=-1){
            SectionInstance sectInst = instance.getSections().get(sectionInstanceId);
            QuestionInstance qInst = instance.getQuestionInstance(sectionInstanceId, questionInstanceId);
            for(Answer a : qInst.getAnswers()){
                statSub(a);
                a.resetAnswer();
            }
            saveStat();
            SaveAsyncTask saveEmpty = new SaveAsyncTask(this, sectInst, qInst, questionInstanceId, sectionInstanceId);
            saveEmpty.execute();
            //remove resources
            getContentResolver().delete(DatabaseContentProvider.RESOURCE_INSTANCE_URI, PoliRispostaDbContract.ResourceInstanceColumns.QUESTION_INSTANCE_ID + " = " + qInst.getId(), null);
            qInst.setPictures(new ArrayList<Resource>());
            qInst.setNotes(new ArrayList<Note>());
        }
        else{
            SectionInstance sectInst = instance.getSections().get(sectionInstanceId);
            for(QuestionInstance qInst : sectInst.getQuestionInstances()){
                for(Answer a : qInst.getAnswers()){
                    statSub(a);
                    a.resetAnswer();
                }
                SaveAsyncTask saveEmpty = new SaveAsyncTask(this, sectInst, qInst, questionInstanceId, sectionInstanceId);
                saveEmpty.execute();
                getContentResolver().delete(DatabaseContentProvider.RESOURCE_INSTANCE_URI, PoliRispostaDbContract.ResourceInstanceColumns.QUESTION_INSTANCE_ID + " = " + qInst.getId(), null);
                qInst.setPictures(new ArrayList<Resource>());
                qInst.setNotes(new ArrayList<Note>());
            }
            saveStat();
        }
        ContentFragment frag = (ContentFragment) getFragmentManager().findFragmentByTag(ContentFragment.TAG);
        if(frag!=null && ((this.questionInstanceId==questionInstanceId && this.sectionInstanceId==sectionInstanceId) || (questionInstanceId==-1 && this.sectionInstanceId==sectionInstanceId))){
            frag.invalidateFragment(true);
        }
        if(questionInstanceId==-1){
            questionInstanceId=0;
        }
        this.onQuestionSelected(sectionInstanceId, questionInstanceId);
        ListFragment listFrag = (ListFragment) getFragmentManager().findFragmentByTag(ListFragment.TAG);
        listFrag.openGroupView(sectionInstanceId);
    }

    @Override
    public void addNewElement(int sectionInstanceId, int questionInstanceId){
        boolean success = false;
        if(questionInstanceId!=-1){

            SectionInstance sInst = instance.getSections().get(sectionInstanceId);
            QuestionInstance qInst = instance.getQuestionInstance(sectionInstanceId, questionInstanceId);
            //if not repeatable do nothing!
            if(!qInst.getQuestion().isRepeat()){
                Toast.makeText(this, "Domanda non ripetibile!", Toast.LENGTH_LONG).show();
                return;
            }

            RepeatQuestionInstanceTask rep = new RepeatQuestionInstanceTask(this, sInst, qInst, questionInstanceId, sectionInstanceId);
            try {
                QuestionInstance newQInst = rep.execute().get();
                //update stats
                for(Answer a: newQInst.getAnswers()){
                    if(a.getField().isRequired()){
                        instance.setmTotalFields(instance.getmTotalFields()+1);
                        instance.setmNotFilled(instance.getmNotFilled()+1);
                    }
                    instance.setTotalFields(instance.getTotalFields()+1);
                    instance.setNotFilled(instance.getNotFilled()+1);
                }
                saveStat();
                //sInst.getQuestionInstances().add(questionInstanceId + newQInst.getIndex() - instance.getQuestionInstance(sectionInstanceId,questionInstanceId).getIndex(), newQInst);
                this.questionInstanceId = questionInstanceId + newQInst.getIndex() - instance.getQuestionInstance(sectionInstanceId,questionInstanceId).getIndex();
                this.sectionInstanceId = sectionInstanceId;
                success = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }
        else{
            //repeat entire section
            SectionInstance sInst = instance.getSections().get(sectionInstanceId);
            if(!sInst.getSection().isRepeat()){
                Toast.makeText(this, "Sezione non ripetibile!", Toast.LENGTH_LONG).show();
                return;
            }

            RepeatSectionInstanceTask rep = new RepeatSectionInstanceTask(this, sInst, instance, sectionInstanceId);

            try {
                SectionInstance newSInst = rep.execute().get();
                //update stats
                for(QuestionInstance newQInst : newSInst.getQuestionInstances()){
                    for(Answer a: newQInst.getAnswers()){
                        if(a.getField().isRequired()){
                            instance.setmTotalFields(instance.getmTotalFields()+1);
                            instance.setmNotFilled(instance.getmNotFilled()+1);
                        }
                        instance.setTotalFields(instance.getTotalFields()+1);
                        instance.setNotFilled(instance.getNotFilled()+1);
                    }
                }
                saveStat();
                this.sectionInstanceId = sectionInstanceId + newSInst.getIndex() - instance.getSections().get(sectionInstanceId).getIndex();
                this.questionInstanceId = 0;
                success = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        //recharge list and content with new things!
        if(success){
            onQuestionSelected(sectionInstanceId, questionInstanceId+1);
            ListFragment list = (ListFragment) getFragmentManager().findFragmentByTag(ListFragment.TAG);
            list.openGroupView(sectionInstanceId);
        }
    }
    @Override
    public void removeElement(int sectionInstanceId, int questionInstanceId){
        Uri questionInstanceUri = DatabaseContentProvider.QUESTION_INSTANCE_URI;
        if(questionInstanceId!=-1){
            //find all the instances of the same kind of question
            Cursor questInstanceCursor = getContentResolver().query(questionInstanceUri, null,
                    PoliRispostaDbContract.QuestionInstanceColumns.SECTION_INSTANCE_ID + "=" + instance.getSections().get(sectionInstanceId).getId() +
                            " and " + PoliRispostaDbContract.QuestionInstanceColumns.QUESTION_ID + "=" + instance.getQuestionInstance(sectionInstanceId, questionInstanceId).getQuestion().getId(),
                    null, PoliRispostaDbContract.QuestionInstanceColumns.QUESTION_ID + " , " + PoliRispostaDbContract.QuestionInstanceColumns.INDEX);

            if(questInstanceCursor.getCount()==1){
                Toast.makeText(this, "Non è possibile eliminare l'elemento, prova con il reset della domanda", Toast.LENGTH_LONG).show();
                return;
            }
            else{
                QuestionInstance removedInstance = instance.getQuestionInstance(sectionInstanceId, questionInstanceId);
                //remove question instance
                getContentResolver().delete(DatabaseContentProvider.QUESTION_INSTANCE_URI, PoliRispostaDbContract.QuestionInstanceColumns._ID + "=" + removedInstance.getId(), null);

                for(QuestionInstance qInst : instance.getSections().get(sectionInstanceId).getQuestionInstances()){
                    if(qInst.getQuestion().getId() == removedInstance.getQuestion().getId() && qInst.getIndex() > removedInstance.getIndex()){
                        qInst.setIndex(qInst.getIndex()-1);
                        ContentValues values = new ContentValues();
                        values.put(PoliRispostaDbContract.QuestionInstanceColumns.INDEX, qInst.getIndex());
                        getContentResolver().update(DatabaseContentProvider.QUESTION_INSTANCE_URI, values, PoliRispostaDbContract.QuestionInstanceColumns._ID +"=" + qInst.getId(), null);
                    }
                }

                //stats update
                int mandatoryRemoved = 0;
                int removed = 0;
                for(Answer a: removedInstance.getAnswers()){
                    statSub(a);
                    if(a.getField().isRequired()){
                        mandatoryRemoved++;
                    }
                    removed++;
                }
                instance.setTotalFields(instance.getTotalFields()-removed);
                instance.setNotFilled(instance.getNotFilled()-removed);
                instance.setmTotalFields(instance.getmTotalFields()-mandatoryRemoved);
                instance.setmNotFilled(instance.getmNotFilled()-mandatoryRemoved);
                saveStat();

                instance.getSections().get(sectionInstanceId).getQuestionInstances().remove(removedInstance);
            }

            questInstanceCursor.close();
            ListFragment list = (ListFragment) getFragmentManager().findFragmentByTag(ListFragment.TAG);
            list.openGroupView(sectionInstanceId);
            if(questionInstanceId>0){
                onQuestionSelected(sectionInstanceId, questionInstanceId-1);
            }
            else{
                onQuestionSelected(sectionInstanceId, questionInstanceId);
            }

        }
        else{
            //section delete! =(
            Uri sectionInstanceUri = DatabaseContentProvider.SECTION_INSTANCE_URI;

            //find all the instances of the same kind of question
            Cursor sectInstanceCursor = getContentResolver().query(sectionInstanceUri, null,
                    PoliRispostaDbContract.SectionInstanceColumns.FORM_INSTANCE_ID + "=" + instance.getId() +
                            " and " + PoliRispostaDbContract.SectionInstanceColumns.SECTION_ID + "=" + instance.getSections().get(sectionInstanceId).getSection().getId(),
                    null, PoliRispostaDbContract.SectionInstanceColumns.SECTION_ID + " , " + PoliRispostaDbContract.SectionInstanceColumns.INDEX);
            if(sectInstanceCursor.getCount()==1){
                Toast.makeText(this, "Non è possibile eliminare l'elemento, prova con il reset della sezione", Toast.LENGTH_LONG).show();
                return;
            }
            else{
                SectionInstance removedInstance = instance.getSections().get(sectionInstanceId);
                //remove section instance (on delete cascade!)
                getContentResolver().delete(DatabaseContentProvider.SECTION_INSTANCE_URI, PoliRispostaDbContract.SectionInstanceColumns._ID + "=" + removedInstance.getId(), null);

                //update indexes in db and data structure
                for(SectionInstance sInst : instance.getSections()){
                    if(sInst.getSection().getId() == removedInstance.getSection().getId() && sInst.getIndex() > removedInstance.getIndex()){
                        sInst.setIndex(sInst.getIndex()-1);
                        ContentValues values = new ContentValues();
                        values.put(PoliRispostaDbContract.SectionInstanceColumns.INDEX, sInst.getIndex());
                        getContentResolver().update(DatabaseContentProvider.SECTION_INSTANCE_URI, values, PoliRispostaDbContract.SectionInstanceColumns._ID +"=" + sInst.getId(), null);
                    }
                }

                //stats update
                for(QuestionInstance qInst : removedInstance.getQuestionInstances()){
                    int mandatoryRemoved = 0;
                    int removed = 0;
                    for(Answer a: qInst.getAnswers()){
                        statSub(a);
                        if(a.getField().isRequired()){
                            mandatoryRemoved++;
                        }
                        removed++;
                    }
                    instance.setTotalFields(instance.getTotalFields()-removed);
                    instance.setNotFilled(instance.getNotFilled()-removed);
                    instance.setmTotalFields(instance.getmTotalFields()-mandatoryRemoved);
                    instance.setmNotFilled(instance.getmNotFilled()-mandatoryRemoved);
                }
                saveStat();

                instance.getSections().remove(removedInstance);
            }

            sectInstanceCursor.close();
            ListFragment list = (ListFragment) getFragmentManager().findFragmentByTag(ListFragment.TAG);

            if(sectionInstanceId>0){
                list.openGroupView(sectionInstanceId-1);
                onQuestionSelected(sectionInstanceId-1, 0);
            }
            else{
                list.openGroupView(sectionInstanceId);
                onQuestionSelected(sectionInstanceId, 0);
            }
        }
    }


    //STAT modifications here!

    private void statSub(Answer a){
        boolean answered = a.isFilled();
        boolean applicable = a.isApplicable();
        //mandatory stat
        if(a.getField().isRequired()){
            if(answered && applicable){
                instance.setmFilled(instance.getmFilled() - 1);
                instance.setmNotFilled(instance.getmNotFilled()+1);
            }
            else if(!applicable){
                instance.setmNotApplicable(instance.getmNotApplicable() -1);
                instance.setmNotFilled(instance.getmNotFilled()+1);
            }
        }
        //normal stat
        if(answered && applicable){
            instance.setFilled(instance.getFilled() - 1);
            instance.setNotFilled(instance.getNotFilled()+1);
        }
        else if(!applicable){
            instance.setNotApplicable(instance.getNotApplicable() -1);
            instance.setNotFilled(instance.getNotFilled()+1);
        }
    }

    private void statAdd(Answer a){

        boolean answered = a.isFilled();
        boolean applicable = a.isApplicable();
        //mandatory stat
        if(a.getField().isRequired()){
            if(answered && applicable){
                instance.setmFilled(instance.getmFilled() + 1);
                instance.setmNotFilled(instance.getmNotFilled() - 1);
            }
            else if(!applicable){
                instance.setmNotApplicable(instance.getmNotApplicable() + 1);
                instance.setmNotFilled(instance.getmNotFilled() - 1);
            }
        }
        //normal stat
        if(answered && applicable){
            instance.setFilled(instance.getFilled() + 1);
            instance.setNotFilled(instance.getNotFilled() - 1);
        }
        else if(!applicable){
            instance.setNotApplicable(instance.getNotApplicable() + 1);
            instance.setNotFilled(instance.getNotFilled() - 1);
        }
    }

    private void saveStat(){
        //save stats in DB
        ContentValues statValues = new ContentValues();
        statValues.put(PoliRispostaDbContract.FormInstanceColumns.TOTAL, instance.getTotalFields());
        statValues.put(PoliRispostaDbContract.FormInstanceColumns.M_TOTAL, instance.getmTotalFields());
        statValues.put(PoliRispostaDbContract.FormInstanceColumns.FILLED, instance.getFilled());
        statValues.put(PoliRispostaDbContract.FormInstanceColumns.NOT_FILLED, instance.getNotFilled());
        statValues.put(PoliRispostaDbContract.FormInstanceColumns.NOT_APPLICABLE, instance.getNotApplicable());
        statValues.put(PoliRispostaDbContract.FormInstanceColumns.M_FILLED, instance.getmFilled());
        statValues.put(PoliRispostaDbContract.FormInstanceColumns.M_NOT_FILLED, instance.getmNotFilled());
        statValues.put(PoliRispostaDbContract.FormInstanceColumns.M_NOT_APPLICABLE, instance.getmNotApplicable());

        getContentResolver().update(DatabaseContentProvider.FORM_INSTANCE_URI, statValues, PoliRispostaDbContract.FormInstanceColumns._ID + " = " + instance.getId(), null);

    }

}
