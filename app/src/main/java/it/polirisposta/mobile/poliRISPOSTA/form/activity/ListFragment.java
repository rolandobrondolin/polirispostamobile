package it.polirisposta.mobile.poliRISPOSTA.form.activity;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.adapters.AdapterQuestion;
import it.polirisposta.mobile.poliRISPOSTA.model.FormInstance;


public class ListFragment extends Fragment implements ExpandableListView.OnChildClickListener {

    public static final String TAG = "ListFragmentView";
    private OnQuestionSelectedListener questionListener;
    private DataRetriever data;
    private ContextMenuQuestionModifier modifier;
    private AdapterQuestion adapter;
    private int selectedSection;
    private int selectedQuestion;
    private boolean open;

    public interface OnQuestionSelectedListener{
        public void onQuestionSelected(int sectionId, int questionId);
    }

    public interface DataRetriever{
        public FormInstance getInstanceData();
    }

    public interface ContextMenuQuestionModifier{
        public void resetElement(int sectionInstanceId, int questionInstanceId);
        public void addNewElement(int sectionInstanceId, int questionInstanceId);
        public void removeElement(int sectionInstanceId, int questionInstanceId);

    }


    public ListFragment(){
        super();
        open=false;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        selectedSection = args.getInt("sectionId");
        selectedQuestion = args.getInt("questionId");
        open=true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        adapter = new AdapterQuestion(data.getInstanceData().getSections(), getActivity());

        View rootView = inflater.inflate(R.layout.question_list, container, false);
        ((ExpandableListView) rootView).setAdapter(adapter);
        ((ExpandableListView) rootView).setOnChildClickListener(this);

        registerForContextMenu(rootView);
        if(open){
            ((ExpandableListView) rootView).expandGroup(selectedSection);
            open=false;
        }

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.questionListener = (OnQuestionSelectedListener) activity;
            this.data = (DataRetriever) activity;
            this.modifier = (ContextMenuQuestionModifier) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnNoteSelectedListener");
        }
    }


    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l) {
        this.questionListener.onQuestionSelected(i, i2);
        return false;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;

        selectedSection = ExpandableListView.getPackedPositionGroup(info.packedPosition);
        selectedQuestion = ExpandableListView.getPackedPositionChild(info.packedPosition);

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_context_form, menu);
        menu.setHeaderTitle("Opzioni form");
        if(selectedQuestion==-1){
            menu.findItem(R.id.menuAdd).setTitle(R.string.menu_section_add);
            menu.findItem(R.id.menuRemove).setTitle(R.string.menu_section_remove);
            menu.findItem(R.id.menuReset).setTitle(R.string.menu_section_reset);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        if(item.getItemId()==R.id.menuAdd){
            modifier.addNewElement(selectedSection, selectedQuestion);
        }
        else if(item.getItemId()==R.id.menuRemove){
            modifier.removeElement(selectedSection, selectedQuestion);
        }
        else if(item.getItemId()==R.id.menuReset){
            modifier.resetElement(selectedSection, selectedQuestion);
        }

        return super.onContextItemSelected(item);
    }

    public void openGroupView(int groupId){
        ExpandableListView expList = (ExpandableListView) getActivity().findViewById(R.id.questionExpandableListView);
        if(expList!=null){
            adapter = new AdapterQuestion(data.getInstanceData().getSections(), getActivity());
            expList.setAdapter(adapter);
            expList.invalidateViews();
            expList.expandGroup(groupId);
        }

    }


}
