package it.polirisposta.mobile.poliRISPOSTA.form.activity.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

import it.polirisposta.mobile.poliRISPOSTA.model.SectionInstance;

/**
 * Created by Rolando on 30/11/14.
 */
public class AdapterQuestion implements ExpandableListAdapter {
    private List<SectionInstance> sections;
    //private ArrayList<Integer> groupIndex;
    //private ArrayList<ArrayList<Integer>> childIndex;

    private Context context;


    public AdapterQuestion(List<SectionInstance> sectionInstances, Context context){
        super();
        this.context=context;
        this.sections=sectionInstances;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getGroupCount() {
        return sections.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return sections.get(groupPosition).getQuestionInstances().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return sections.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return sections.get(groupPosition).getQuestionInstances().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_expandable_list_item_2, null);
        }
        String text = sections.get(groupPosition).getSection().getTextName();
        if(sections.get(groupPosition).getIndex()>1){
            text = text.concat(" (" + sections.get(groupPosition).getIndex() + ")");
        }
        ((TextView)convertView.findViewById(android.R.id.text1)).setText(text);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_list_item_1, null);
        }
        String text = sections.get(groupPosition).getQuestionInstance(childPosition).getQuestion().getTextName();
        if(sections.get(groupPosition).getQuestionInstance(childPosition).getIndex()>1){
            text = text.concat(" (" + sections.get(groupPosition).getQuestionInstance(childPosition).getIndex() + ")");
        }
        ((TextView)convertView.findViewById(android.R.id.text1)).setText(text);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        if(sections==null){
            return true;
        }
        if(sections.size()==0){
            return true;
        }
        return false;
    }

    @Override
    public void onGroupExpanded(int i) {

    }

    @Override
    public void onGroupCollapsed(int i) {

    }

    @Override
    public long getCombinedChildId(long l, long l2) {

        return l*1000+l2;
    }

    @Override
    public long getCombinedGroupId(long l) {
        return l;
    }
}
