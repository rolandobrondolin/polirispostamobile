package it.polirisposta.mobile.poliRISPOSTA.form.activity.asynctask;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import java.util.Calendar;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.QuestionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.SectionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DateAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.FloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.GpsAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IstatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.MeasureAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.Select1Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.SelectAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.StringAnswer;

import static java.lang.Thread.sleep;

/**
 * Created by Rolando on 19/04/15.
 */
public class RepeatQuestionInstanceTask extends AsyncTask<Void, Void, QuestionInstance> {


    private ProgressDialog dialog;
    Context context;
    SectionInstance sInst;
    QuestionInstance qInst;
    int qInstId;
    int sInstId;


    public RepeatQuestionInstanceTask(Context context, SectionInstance sInst, QuestionInstance qInst, int qInstId, int sInstId) {
        super();
        this.context = context;
        this.sInst=sInst;
        this.qInst=qInst;
        this.sInstId=sInstId;
        this.qInstId=qInstId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialog.show(context, "Ripetizione domanda", "Ripetizione della domanda in corso...");
    }

    @Override
    protected void onPostExecute(QuestionInstance questionInstance) {
        super.onPostExecute(questionInstance);
        dialog.dismiss();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        dialog.cancel();
    }

    @Override
    protected QuestionInstance doInBackground(Void... params) {
        QuestionInstance dest = repeatQuestion();


        return dest;
    }

    public QuestionInstance repeatQuestion(){
        QuestionInstance origin = qInst;
        Cursor indexCursor = context.getContentResolver().query(DatabaseContentProvider.QUESTION_INSTANCE_URI, null,
                PoliRispostaDbContract.QuestionInstanceColumns.QUESTION_ID + " = " + origin.getQuestion().getId() + " and " + PoliRispostaDbContract.QuestionInstanceColumns.SECTION_INSTANCE_ID + " = " + sInst.getId(), null, null);
        int index = indexCursor.getCount() +1;
        indexCursor.close();
        QuestionInstance destination = new QuestionInstance(-1, index, origin.getQuestion());
        //clone questionInstance
        for(int i=0; i< origin.getQuestion().fieldSize(); i++){
            Field field = origin.getQuestion().getField(i);
            Answer ans = null;
            switch (field.getType()){
                case STRING:
                    ans = new StringAnswer(-1, false, true, false, "", field);
                    break;
                case INTEGER:
                    ans = new IntegerAnswer(-1, false, true, false, 0, field);
                    break;
                case POSITIVE_INTEGER:
                    ans = new PositiveIntegerAnswer(-1, false, true, false, 0, field);
                    break;
                case NEGATIVE_INTEGER:
                    ans = new NegativeIntegerAnswer(-1, false, true, false, 0, field);
                    break;
                case FLOAT:
                    ans = new FloatAnswer(-1, false, true, false, 0, field);
                    break;
                case POSITIVE_FLOAT:
                    ans = new PositiveFloatAnswer(-1, false, true, false, 0, field);
                    break;
                case NEGATIVE_FLOAT:
                    ans = new NegativeFloatAnswer(-1, false, true, false, 0, field);
                    break;
                case MEASURE:
                    ans = new MeasureAnswer(-1, false, true, false, 0, "m", field);
                    break;
                case ISTAT_CODE:
                    ans = new IstatAnswer(-1, false, true, false, "", field);
                    break;
                case GPS:
                    ans = new GpsAnswer(-1, false, true, false, "", field);
                    break;
                case DATE:
                    Calendar cal = Calendar.getInstance();
                    ans = new DateAnswer(-1, false, true, false, false, cal.getTime(), field);
                    break;
                case DATE_TIME:
                    Calendar calTime = Calendar.getInstance();
                    ans = new DateAnswer(-1, false, true, false, true, calTime.getTime(), field);
                    break;
                case SELECT:
                    ans = new SelectAnswer(-1, false, true, false, "", field);
                    break;
                case SELECT1:
                    ans = new Select1Answer(-1, false, true, false, "", field);
                case SKETCH:
                    break;
            }
            destination.addAnswer(ans);
        }
        //save new qInstance inside DB and add it to the sectionInstance
        //this must be done in the main thread!
        //sInst.getQuestionInstances().add(qInstId+1, destination);
        //question instance init in database
        Uri questionInstanceUri = DatabaseContentProvider.QUESTION_INSTANCE_URI;
        ContentValues questionInstanceValues = new ContentValues();
        questionInstanceValues.put(PoliRispostaDbContract.QuestionInstanceColumns.INDEX, destination.getIndex());
        questionInstanceValues.put(PoliRispostaDbContract.QuestionInstanceColumns.SECTION_INSTANCE_ID, sInst.getId());
        questionInstanceValues.put(PoliRispostaDbContract.QuestionInstanceColumns.QUESTION_ID, destination.getQuestion().getId());
        Uri qInstIdUri = context.getContentResolver().insert(questionInstanceUri, questionInstanceValues);
        //set returned id for the q instance
        int qId = Integer.parseInt(qInstIdUri.getLastPathSegment());
        destination.setId(qId);
        //save the empty answers
        for(Answer ans: destination.getAnswers()){
            saveAnswerInDB(ans, destination.getId());
        }
        sInst.getQuestionInstances().add(qInstId + destination.getIndex() - qInst.getIndex(), destination);
        return destination;
    }


    private void saveAnswerInDB(Answer a, int qid){
        Uri answerUri = DatabaseContentProvider.ANSWER_URI;
        ContentValues values = new ContentValues();

        //ID is auto increment!
        //values.put(AnswerColumns._ID, a.getId());
        values.put(PoliRispostaDbContract.AnswerColumns.PREFILLED, a.isPrefilled()?1:0);
        values.put(PoliRispostaDbContract.AnswerColumns.APPLICABLE, a.isApplicable()?1:0);
        values.put(PoliRispostaDbContract.AnswerColumns.TYPE, a.getType().toString());
        values.put(PoliRispostaDbContract.AnswerColumns.FIELD_ID, a.getField().getId());
        values.put(PoliRispostaDbContract.AnswerColumns.QUESTION_INSTANCE_ID, qid);
        values.put(PoliRispostaDbContract.AnswerColumns.FILLED, a.isFilled()?1:0);
        switch (a.getType()){
            case STRING:
                StringAnswer ansStr = (StringAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansStr.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case INTEGER:
                IntegerAnswer ansInt = (IntegerAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER, ansInt.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case POSITIVE_INTEGER:
                PositiveIntegerAnswer ansPosInt = (PositiveIntegerAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER, ansPosInt.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case NEGATIVE_INTEGER:
                NegativeIntegerAnswer ansNegInt = (NegativeIntegerAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER, ansNegInt.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case FLOAT:
                FloatAnswer ansFloat = (FloatAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT, ansFloat.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                break;
            case POSITIVE_FLOAT:
                PositiveFloatAnswer ansPosFloat = (PositiveFloatAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT, ansPosFloat.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                break;
            case NEGATIVE_FLOAT:
                NegativeFloatAnswer ansNegFloat = (NegativeFloatAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT, ansNegFloat.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                break;
            case MEASURE:
                MeasureAnswer measureAns = (MeasureAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT, measureAns.getAnswer());
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, measureAns.getUm());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                break;
            case ISTAT_CODE:
                IstatAnswer ansIstat = (IstatAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansIstat.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case GPS:
                GpsAnswer ansGps = (GpsAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansGps.gpsConcat());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case DATE:
            case DATE_TIME:
                DateAnswer ansDate = (DateAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansDate.getAnswer().getTime());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case SELECT:
                break;
            case SELECT1:
                Select1Answer ansSel = (Select1Answer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansSel.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case SKETCH:
                break;
        }

        Uri answerInsertUri = context.getContentResolver().insert(answerUri, values);
        //set returned id for the q instance
        int ansId = Integer.parseInt(answerInsertUri.getLastPathSegment());
        a.setId(ansId);
    }


}
