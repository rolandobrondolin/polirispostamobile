package it.polirisposta.mobile.poliRISPOSTA.form.activity.asynctask;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.util.Calendar;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;
import it.polirisposta.mobile.poliRISPOSTA.model.FormInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.Question;
import it.polirisposta.mobile.poliRISPOSTA.model.QuestionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.Resource;
import it.polirisposta.mobile.poliRISPOSTA.model.Section;
import it.polirisposta.mobile.poliRISPOSTA.model.SectionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DateAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.FloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.GpsAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IstatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.MeasureAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.Select1Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.SelectAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.StringAnswer;

/**
 * Created by Rolando on 19/04/15.
 */
public class RepeatSectionInstanceTask extends AsyncTask<Void, Void, SectionInstance> {

    private ProgressDialog dialog;
    Context context;
    SectionInstance sInst;
    FormInstance instance;
    int sInstId;


    public RepeatSectionInstanceTask(Context context, SectionInstance sInst, FormInstance formInstance, int sInstId) {
        super();
        this.context = context;
        this.sInst=sInst;
        this.sInstId=sInstId;
        this.instance = formInstance;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialog.show(context, "Ripetizione sezione", "Ripetizione della sezione in corso...");
    }

    @Override
    protected void onPostExecute(SectionInstance sectionInstance) {
        super.onPostExecute(sectionInstance);
        dialog.dismiss();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        dialog.cancel();
    }

    @Override
    protected SectionInstance doInBackground(Void... params) {
        SectionInstance dest = repeatSection();
        return dest;

    }

    public SectionInstance repeatSection(){

        Cursor indexCursor = context.getContentResolver().query(DatabaseContentProvider.SECTION_INSTANCE_URI, null,
                PoliRispostaDbContract.SectionInstanceColumns.SECTION_ID + " = " + sInst.getSection().getId() + " and " + PoliRispostaDbContract.SectionInstanceColumns.FORM_INSTANCE_ID + " = " + instance.getId(), null, null);
        int index = indexCursor.getCount() + 1;
        indexCursor.close();
        SectionInstance destination = new SectionInstance(-1, index, sInst.getSection());

        Uri sectionInstanceUri = DatabaseContentProvider.SECTION_INSTANCE_URI;
        ContentValues sectionInstanceValues = new ContentValues();
        sectionInstanceValues.put(PoliRispostaDbContract.SectionInstanceColumns.INDEX, destination.getIndex());
        sectionInstanceValues.put(PoliRispostaDbContract.SectionInstanceColumns.FORM_INSTANCE_ID, instance.getId());
        sectionInstanceValues.put(PoliRispostaDbContract.SectionInstanceColumns.SECTION_ID, destination.getSection().getId());
        Uri sInstIdUri = context.getContentResolver().insert(sectionInstanceUri, sectionInstanceValues);
        //set returned id for the q instance
        int sId = Integer.parseInt(sInstIdUri.getLastPathSegment());
        destination.setId(sId);
        //save the empty answers
        QuestionInstance addQInst;

        Section sect = sInst.getSection();
        for(Question q: sect.getQuestions()){
            addQInst = repeatQuestion(q, destination.getId());
            destination.addQuestionInstance(addQInst);
        }
        int listIndex = sInstId + destination.getIndex() - instance.getSections().get(sInstId).getIndex();
        if(listIndex >= instance.getSections().size()){
            instance.getSections().add(destination);
        }
        else{
            instance.getSections().add(listIndex, destination);
        }


        return destination;
    }

    private QuestionInstance repeatQuestion(Question q, int sectionInstanceId){
        Question origin = q;
        QuestionInstance destination = new QuestionInstance(-1, 1, origin);
        //clone questionInstance
        for(int i=0; i< origin.fieldSize(); i++){
            Field field = origin.getField(i);
            Answer ans = null;
            switch (field.getType()){
                case STRING:
                    ans = new StringAnswer(-1, false, true, false, "", field);
                    break;
                case INTEGER:
                    ans = new IntegerAnswer(-1, false, true, false, 0, field);
                    break;
                case POSITIVE_INTEGER:
                    ans = new PositiveIntegerAnswer(-1, false, true, false, 0, field);
                    break;
                case NEGATIVE_INTEGER:
                    ans = new NegativeIntegerAnswer(-1, false, true, false, 0, field);
                    break;
                case FLOAT:
                    ans = new FloatAnswer(-1, false, true, false, 0, field);
                    break;
                case POSITIVE_FLOAT:
                    ans = new PositiveFloatAnswer(-1, false, true, false, 0, field);
                    break;
                case NEGATIVE_FLOAT:
                    ans = new NegativeFloatAnswer(-1, false, true, false, 0, field);
                    break;
                case MEASURE:
                    ans = new MeasureAnswer(-1, false, true, false, 0, "m", field);
                    break;
                case ISTAT_CODE:
                    ans = new IstatAnswer(-1, false, true, false, "", field);
                    break;
                case GPS:
                    ans = new GpsAnswer(-1, false, true, false, "", field);
                    break;
                case DATE:
                    Calendar cal = Calendar.getInstance();
                    ans = new DateAnswer(-1, false, true, false, false, cal.getTime(), field);
                    break;
                case DATE_TIME:
                    Calendar calTime = Calendar.getInstance();
                    ans = new DateAnswer(-1, false, true, false, true, calTime.getTime(), field);
                    break;
                case SELECT:
                    ans = new SelectAnswer(-1, false, true, false, "", field);
                    break;
                case SELECT1:
                    ans = new Select1Answer(-1, false, true, false, "", field);
                case SKETCH:
                    break;
            }
            destination.addAnswer(ans);
        }
        //save new qInstance inside DB and add it to the sectionInstance
        //this must be done in the main thread!
        //sInst.getQuestionInstances().add(qInstId+1, destination);
        //question instance init in database
        Uri questionInstanceUri = DatabaseContentProvider.QUESTION_INSTANCE_URI;
        ContentValues questionInstanceValues = new ContentValues();
        questionInstanceValues.put(PoliRispostaDbContract.QuestionInstanceColumns.INDEX, destination.getIndex());
        questionInstanceValues.put(PoliRispostaDbContract.QuestionInstanceColumns.SECTION_INSTANCE_ID, sectionInstanceId);
        questionInstanceValues.put(PoliRispostaDbContract.QuestionInstanceColumns.QUESTION_ID, destination.getQuestion().getId());
        Uri qInstIdUri = context.getContentResolver().insert(questionInstanceUri, questionInstanceValues);
        //set returned id for the q instance
        int qId = Integer.parseInt(qInstIdUri.getLastPathSegment());
        destination.setId(qId);
        //save the empty answers
        for(Answer ans: destination.getAnswers()){
            saveAnswerInDB(ans, destination.getId());
        }

        return destination;
    }


    private void saveAnswerInDB(Answer a, int qid){
        Uri answerUri = DatabaseContentProvider.ANSWER_URI;
        ContentValues values = new ContentValues();

        //ID is auto increment!
        //values.put(AnswerColumns._ID, a.getId());
        values.put(PoliRispostaDbContract.AnswerColumns.PREFILLED, a.isPrefilled()?1:0);
        values.put(PoliRispostaDbContract.AnswerColumns.APPLICABLE, a.isApplicable()?1:0);
        values.put(PoliRispostaDbContract.AnswerColumns.TYPE, a.getType().toString());
        values.put(PoliRispostaDbContract.AnswerColumns.FIELD_ID, a.getField().getId());
        values.put(PoliRispostaDbContract.AnswerColumns.QUESTION_INSTANCE_ID, qid);
        values.put(PoliRispostaDbContract.AnswerColumns.FILLED, a.isFilled()?1:0);
        switch (a.getType()){
            case STRING:
                StringAnswer ansStr = (StringAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansStr.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case INTEGER:
                IntegerAnswer ansInt = (IntegerAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER, ansInt.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case POSITIVE_INTEGER:
                PositiveIntegerAnswer ansPosInt = (PositiveIntegerAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER, ansPosInt.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case NEGATIVE_INTEGER:
                NegativeIntegerAnswer ansNegInt = (NegativeIntegerAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER, ansNegInt.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case FLOAT:
                FloatAnswer ansFloat = (FloatAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT, ansFloat.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                break;
            case POSITIVE_FLOAT:
                PositiveFloatAnswer ansPosFloat = (PositiveFloatAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT, ansPosFloat.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                break;
            case NEGATIVE_FLOAT:
                NegativeFloatAnswer ansNegFloat = (NegativeFloatAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT, ansNegFloat.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                break;
            case MEASURE:
                MeasureAnswer measureAns = (MeasureAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT, measureAns.getAnswer());
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, measureAns.getUm());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                break;
            case ISTAT_CODE:
                IstatAnswer ansIstat = (IstatAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansIstat.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case GPS:
                GpsAnswer ansGps = (GpsAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansGps.gpsConcat());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case DATE:
            case DATE_TIME:
                DateAnswer ansDate = (DateAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansDate.getAnswer().getTime());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case SELECT:
                SelectAnswer ansSelect = (SelectAnswer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansSelect.getAnswerDB());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case SELECT1:
                Select1Answer ansSel = (Select1Answer) a;
                values.put(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING, ansSel.getAnswer());
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER);
                values.putNull(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT);
                break;
            case SKETCH:
                break;
        }

        Uri answerInsertUri = context.getContentResolver().insert(answerUri, values);
        //set returned id for the q instance
        int ansId = Integer.parseInt(answerInsertUri.getLastPathSegment());
        a.setId(ansId);
    }
}
