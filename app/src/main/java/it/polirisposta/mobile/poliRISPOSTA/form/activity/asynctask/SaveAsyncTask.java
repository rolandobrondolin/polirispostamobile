package it.polirisposta.mobile.poliRISPOSTA.form.activity.asynctask;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.util.Calendar;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;
import it.polirisposta.mobile.poliRISPOSTA.model.Note;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.Question;
import it.polirisposta.mobile.poliRISPOSTA.model.QuestionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.Resource;
import it.polirisposta.mobile.poliRISPOSTA.model.SectionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DateAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.FloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.GpsAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IstatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.MeasureAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.Select1Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.SelectAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.StringAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract.*;

/**
 * Created by Rolando on 09/04/15.
 */
public class SaveAsyncTask extends AsyncTask<Void, Void, Void> {
    Context context;
    SectionInstance sInst;
    QuestionInstance qInst;
    int qInstId;
    int sInstId;


    public SaveAsyncTask(Context context, SectionInstance sInst, QuestionInstance qInst, int qInstId, int sInstId) {
        super();
        this.context = context;
        this.sInst=sInst;
        this.qInst=qInst;
        this.sInstId=sInstId;
        this.qInstId=qInstId;
    }



    @Override
    protected Void doInBackground(Void... params) {
        for(int i=0; i<qInst.getAnswers().size(); i++){
            Answer a = qInst.getAnswer(i);
            editAnswerInDB(a, qInst.getId());
        }
        saveResourceInstances(qInst);
        saveNotes(qInst);
        return null;
    }

    private void editAnswerInDB(Answer a, int qid){
        Uri answerUri = DatabaseContentProvider.ANSWER_URI;
        ContentValues values = new ContentValues();

        //ID is auto increment!
        //values.put(AnswerColumns._ID, a.getId());
        values.put(AnswerColumns.PREFILLED, a.isPrefilled()?1:0);
        values.put(AnswerColumns.APPLICABLE, a.isApplicable()?1:0);
        values.put(AnswerColumns.TYPE, a.getType().toString());
        values.put(AnswerColumns.FIELD_ID, a.getField().getId());
        values.put(AnswerColumns.QUESTION_INSTANCE_ID, qid);
        values.put(AnswerColumns.FILLED, a.isFilled() ? 1 : 0);
        switch (a.getType()){
            case STRING:
                StringAnswer ansStr = (StringAnswer) a;
                values.put(AnswerColumns.ANSWER_STRING, ansStr.getAnswer());
                values.putNull(AnswerColumns.ANSWER_INTEGER);
                values.putNull(AnswerColumns.ANSWER_FLOAT);
                break;
            case INTEGER:
                IntegerAnswer ansInt = (IntegerAnswer) a;
                values.put(AnswerColumns.ANSWER_INTEGER, ansInt.getAnswer());
                values.putNull(AnswerColumns.ANSWER_STRING);
                values.putNull(AnswerColumns.ANSWER_FLOAT);
                break;
            case POSITIVE_INTEGER:
                PositiveIntegerAnswer ansPosInt = (PositiveIntegerAnswer) a;
                values.put(AnswerColumns.ANSWER_INTEGER, ansPosInt.getAnswer());
                values.putNull(AnswerColumns.ANSWER_STRING);
                values.putNull(AnswerColumns.ANSWER_FLOAT);
                break;
            case NEGATIVE_INTEGER:
                NegativeIntegerAnswer ansNegInt = (NegativeIntegerAnswer) a;
                values.put(AnswerColumns.ANSWER_INTEGER, ansNegInt.getAnswer());
                values.putNull(AnswerColumns.ANSWER_STRING);
                values.putNull(AnswerColumns.ANSWER_FLOAT);
                break;
            case FLOAT:
                FloatAnswer ansFloat = (FloatAnswer) a;
                values.put(AnswerColumns.ANSWER_FLOAT, ansFloat.getAnswer());
                values.putNull(AnswerColumns.ANSWER_STRING);
                values.putNull(AnswerColumns.ANSWER_INTEGER);
                break;
            case POSITIVE_FLOAT:
                PositiveFloatAnswer ansPosFloat = (PositiveFloatAnswer) a;
                values.put(AnswerColumns.ANSWER_FLOAT, ansPosFloat.getAnswer());
                values.putNull(AnswerColumns.ANSWER_STRING);
                values.putNull(AnswerColumns.ANSWER_INTEGER);
                break;
            case NEGATIVE_FLOAT:
                NegativeFloatAnswer ansNegFloat = (NegativeFloatAnswer) a;
                values.put(AnswerColumns.ANSWER_FLOAT, ansNegFloat.getAnswer());
                values.putNull(AnswerColumns.ANSWER_STRING);
                values.putNull(AnswerColumns.ANSWER_INTEGER);
                break;
            case MEASURE:
                MeasureAnswer measureAns = (MeasureAnswer) a;
                values.put(AnswerColumns.ANSWER_FLOAT, measureAns.getAnswer());
                values.put(AnswerColumns.ANSWER_STRING, measureAns.getUm());
                values.putNull(AnswerColumns.ANSWER_INTEGER);
                break;
            case ISTAT_CODE:
                IstatAnswer ansIstat = (IstatAnswer) a;
                values.put(AnswerColumns.ANSWER_STRING, ansIstat.getAnswer());
                values.putNull(AnswerColumns.ANSWER_INTEGER);
                values.putNull(AnswerColumns.ANSWER_FLOAT);
                break;
            case GPS:
                GpsAnswer ansGps = (GpsAnswer) a;
                values.put(AnswerColumns.ANSWER_STRING, ansGps.gpsConcat());
                values.putNull(AnswerColumns.ANSWER_INTEGER);
                values.putNull(AnswerColumns.ANSWER_FLOAT);
                break;
            case DATE:
            case DATE_TIME:
                DateAnswer ansDate = (DateAnswer) a;
                values.put(AnswerColumns.ANSWER_STRING, ansDate.getAnswer().getTime());
                values.putNull(AnswerColumns.ANSWER_INTEGER);
                values.putNull(AnswerColumns.ANSWER_FLOAT);
                break;
            case SELECT:
                SelectAnswer ansSel = (SelectAnswer) a;
                values.put(AnswerColumns.ANSWER_STRING, ansSel.getAnswerDB());
                values.putNull(AnswerColumns.ANSWER_INTEGER);
                values.putNull(AnswerColumns.ANSWER_FLOAT);
                break;
            case SELECT1:
                Select1Answer ansSel1 = (Select1Answer) a;
                values.put(AnswerColumns.ANSWER_STRING, ansSel1.getAnswer());
                values.putNull(AnswerColumns.ANSWER_INTEGER);
                values.putNull(AnswerColumns.ANSWER_FLOAT);
                break;
            case SKETCH:
                break;
        }
        context.getContentResolver().update(answerUri, values, AnswerColumns._ID + " = " + a.getId(), null);

    }

    private void saveResourceInstances(QuestionInstance qInst){
        Uri resourceInstanceUri = DatabaseContentProvider.RESOURCE_INSTANCE_URI;

        for(Resource res : qInst.getPictures()){
            ContentValues values = new ContentValues();
            values.put(ResourceInstanceColumns.DESCRIPTION, res.getDescription());
            values.put(ResourceInstanceColumns.TYPE, res.getType().toString());
            values.put(ResourceInstanceColumns.PATH, res.getUri().toString());
            values.put(ResourceInstanceColumns.QUESTION_INSTANCE_ID, qInst.getId());
            if(res.getId()==-1){
                Uri resDbUri = context.getContentResolver().insert(resourceInstanceUri, values);
                //set returned id for the q instance
                int resId = Integer.parseInt(resDbUri.getLastPathSegment());
                res.setId(resId);
            }
            else{
                context.getContentResolver().update(resourceInstanceUri, values, ResourceInstanceColumns._ID + " = " + res.getId(), null);
            }
        }
    }

    private void saveNotes(QuestionInstance qInst){
        Uri noteUri = DatabaseContentProvider.NOTE_URI;

        for(Note n : qInst.getNotes()){
            ContentValues values = new ContentValues();
            values.put(NoteColumns.TEXT, n.getNote());
            values.put(NoteColumns.QUESTION_INSTANCE_ID, qInst.getId());
            if (n.getId()==-1){
                Uri noteIdUri = context.getContentResolver().insert(noteUri, values);
                int noteID = Integer.parseInt(noteIdUri.getLastPathSegment());
                n.setId(noteID);
            }
            else{
                context.getContentResolver().update(noteUri, values, ResourceInstanceColumns._ID + " = " + n.getId(), null);
            }
        }
    }


}
