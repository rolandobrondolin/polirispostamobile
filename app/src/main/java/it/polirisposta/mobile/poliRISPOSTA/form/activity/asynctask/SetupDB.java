package it.polirisposta.mobile.poliRISPOSTA.form.activity.asynctask;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;
import it.polirisposta.mobile.poliRISPOSTA.model.Form;
import it.polirisposta.mobile.poliRISPOSTA.model.FormInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.Note;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.Question;
import it.polirisposta.mobile.poliRISPOSTA.model.QuestionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.Resource;
import it.polirisposta.mobile.poliRISPOSTA.model.Section;
import it.polirisposta.mobile.poliRISPOSTA.model.SectionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DataType;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DateAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.FloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.GpsAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IstatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.MeasureAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.ResourceType;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.Select1Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.SelectAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.StringAnswer;

import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract.*;
import it.polirisposta.mobile.poliRISPOSTA.util.Util;

/**
 * Created by Rolando on 01/04/15.
 */
public class SetupDB extends AsyncTask<Integer, Void, FormInstance> {
    private Integer totMandatoryFields;
    private Integer totFields;
    private ProgressDialog dialog;
    private Context context;
    private LoadCallback callback;
    private DataType dummy = DataType.STRING;

    public SetupDB(Context context, LoadCallback callback){
        super();
        this.context = context;
        this.callback = callback;
        totMandatoryFields = 0;
        totFields = 0;
    }

    public interface LoadCallback{
        public void loadCompletedCallback();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialog.show(context, "Modifica form", "Caricamento form in corso");
    }

    @Override
    protected void onCancelled() {
        dialog.cancel();
        super.onCancelled();
    }

    @Override
    protected void onPostExecute(FormInstance formInstance) {
        super.onPostExecute(formInstance);
        dialog.cancel();
        this.callback.loadCompletedCallback();
    }
    //value params:
    //0 -> new/edit
    //1 -> form instance id or form id
    @Override
    protected FormInstance doInBackground(Integer... params) {
        FormInstance ret = null;
        if(params[0] == 0){ //new!
            ret = newOpen(params[1], params[2]);
        }
        else{ //edit!
            ret = editOpen(params[1]);
        }

        return ret;
    }

    //TODO: handle multiple questions and multiple sections
    public FormInstance editOpen(int formInstanceID){

        FormInstance instance = null;
        Form form;
        int formID;
        //search for instances, then retrieve the model by id coming from the instances
        Uri formInstanceUri = DatabaseContentProvider.FORM_INSTANCE_URI;
        Uri sectionInstanceUri = DatabaseContentProvider.SECTION_INSTANCE_URI;
        Uri questionInstanceUri = DatabaseContentProvider.QUESTION_INSTANCE_URI;
        Uri formUri = DatabaseContentProvider.FORM_URI;
        Uri sectionUri = DatabaseContentProvider.SECTION_URI;
        Uri questionUri = DatabaseContentProvider.QUESTION_URI;
        Uri resourceInstanceUri = DatabaseContentProvider.RESOURCE_INSTANCE_URI;
        Uri noteUri = DatabaseContentProvider.NOTE_URI;

        ContentResolver resolver = context.getContentResolver();
        Cursor formInstCursor = resolver.query(formInstanceUri, null, FormInstanceColumns._ID + " = " + formInstanceID, null, null);
        if(formInstCursor!=null){
            formInstCursor.moveToFirst();
            int formInstID = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns._ID));
            //taskId missing actually!
            int taskId = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.TASK_ID));
            int formFkId = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.FORM_ID));
            String timestamp = formInstCursor.getString(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.TIMESTAMP));
            String deviceId = formInstCursor.getString(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.DEVICE_ID));
            boolean finalized = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.FINALIZED))==1;
            //retrieve stat info
            int totalFields = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.TOTAL));
            int mtotalFields = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.M_TOTAL));
            int filledFields = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.FILLED));
            int notFilledFields = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.NOT_FILLED));
            int notApplicableFields = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.NOT_APPLICABLE));
            int mFilled = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.M_FILLED));
            int mNotFilled = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.M_NOT_FILLED));
            int mNotApplicable = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.M_NOT_APPLICABLE));


            formInstCursor.close();
            form = null;
            Cursor formCursor = resolver.query(formUri, null, FormColumns._ID + " = " + formFkId, null, null);
            if(formCursor!=null){
                formCursor.moveToFirst();
                formID = formCursor.getInt(formCursor.getColumnIndexOrThrow(FormColumns._ID));
                String formName = formCursor.getString(formCursor.getColumnIndexOrThrow(FormColumns.NAME));
                String formTextName = formCursor.getString(formCursor.getColumnIndexOrThrow(FormColumns.TEXT_NAME));
                String formDescription = formCursor.getString(formCursor.getColumnIndexOrThrow(FormColumns.DESCRIPTION));
                formCursor.close();
                form = new Form(formID, formName, formTextName, formDescription);
            }
            instance = new FormInstance(formInstID, form, timestamp, deviceId, taskId, finalized);

            instance.setTotalFields(totalFields);
            instance.setmTotalFields(mtotalFields);
            instance.setFilled(filledFields);
            instance.setNotFilled(notFilledFields);
            instance.setNotApplicable(notApplicableFields);
            instance.setmFilled(mFilled);
            instance.setmNotFilled(mNotFilled);
            instance.setmNotApplicable(mNotApplicable);

            //retrieve section instances and sections
            Cursor sectInstCursor = resolver.query(sectionInstanceUri, null, SectionInstanceColumns.FORM_INSTANCE_ID + " = " + instance.getId(), null, SectionInstanceColumns.SECTION_ID + ", " + SectionInstanceColumns.INDEX);
            if(sectInstCursor!=null){
                while(sectInstCursor.moveToNext()){

                    //data for instance
                    int sectInstID = sectInstCursor.getInt(sectInstCursor.getColumnIndexOrThrow(SectionInstanceColumns._ID));
                    int sectIndex = sectInstCursor.getInt(sectInstCursor.getColumnIndexOrThrow(SectionInstanceColumns.INDEX));
                    //int sectFormInstanceId = formInstCursor.getInt(sectInstCursor.getColumnIndexOrThrow(SectionInstanceColumns.FORM_INSTANCE_ID));
                    int sectionId = sectInstCursor.getInt(sectInstCursor.getColumnIndexOrThrow(SectionInstanceColumns.SECTION_ID));

                    //find the unique section related
                    Cursor sectionCursor = resolver.query(sectionUri, null, SectionColumns._ID + " = " + sectionId, null, null);
                    Section sect = null;
                    if(sectionCursor!= null){
                        sectionCursor.moveToFirst();
                        int sectionID = sectionCursor.getInt(sectionCursor.getColumnIndexOrThrow(SectionColumns._ID));
                        String sectionName = sectionCursor.getString(sectionCursor.getColumnIndexOrThrow(SectionColumns.NAME));
                        String sectionTextName = sectionCursor.getString(sectionCursor.getColumnIndexOrThrow(SectionColumns.TEXT_NAME));
                        String sectionDescription = sectionCursor.getString(sectionCursor.getColumnIndexOrThrow(SectionColumns.DESCRIPTION));
                        int sectionReq = sectionCursor.getInt(sectionCursor.getColumnIndexOrThrow(SectionColumns.REQUIRED));
                        int sectionRep = sectionCursor.getInt(sectionCursor.getColumnIndexOrThrow(SectionColumns.REPEAT));
                        //set section info
                        sect = new Section(sectionID, sectionName, sectionTextName, sectionDescription, sectionReq==1, sectionRep==1);
                        form.addSection(sect);
                        sectionCursor.close();
                    }
                    SectionInstance sectInst = new SectionInstance(sectInstID, sectIndex, sect);
                    instance.addSectionInstance(sectInst);
                    //now find question instances for each section instance!
                    Cursor questInstanceCursor = resolver.query(questionInstanceUri, null, QuestionInstanceColumns.SECTION_INSTANCE_ID + "=" + sectInst.getId(), null, QuestionInstanceColumns.QUESTION_ID + " , " + QuestionInstanceColumns.INDEX);
                    if(questInstanceCursor!=null){
                        while(questInstanceCursor.moveToNext()){
                            int questInstID = questInstanceCursor.getInt(questInstanceCursor.getColumnIndexOrThrow(QuestionInstanceColumns._ID));
                            int questIndex = questInstanceCursor.getInt(questInstanceCursor.getColumnIndexOrThrow(QuestionInstanceColumns.INDEX));
                            //int questSectionInstanceId = formInstCursor.getInt(questInstanceCursor.getColumnIndexOrThrow(QuestionInstanceColumns.SECTION_INSTANCE_ID));
                            int questionId = questInstanceCursor.getInt(questInstanceCursor.getColumnIndexOrThrow(QuestionInstanceColumns.QUESTION_ID));
                            Cursor questionCursor = resolver.query(questionUri, null, QuestionColumns._ID + " = " + questionId, null, null);
                            Question question = null;
                            if(questionCursor!=null){
                                questionCursor.moveToFirst();
                                int questionID = questionCursor.getInt(questionCursor.getColumnIndexOrThrow(QuestionColumns._ID));
                                String questionName = questionCursor.getString(questionCursor.getColumnIndexOrThrow(QuestionColumns.NAME));
                                String questionTextName = questionCursor.getString(questionCursor.getColumnIndexOrThrow(QuestionColumns.TEXT_NAME));
                                String questionDescription = questionCursor.getString(questionCursor.getColumnIndexOrThrow(QuestionColumns.DESCRIPTION));
                                int questionReq = questionCursor.getInt(questionCursor.getColumnIndexOrThrow(QuestionColumns.REQUIRED));
                                int questionRep = questionCursor.getInt(questionCursor.getColumnIndexOrThrow(QuestionColumns.REPEAT));
                                question = new Question(questionID, questionName,questionTextName, questionDescription, questionReq==1, questionRep==1);
                                //load relative images
                                loadBundleImages(question);
                                sect.addQuestion(question);
                                questionCursor.close();
                            }
                            QuestionInstance questInstance = new QuestionInstance(questInstID, questIndex, question);
                            //find resources inside the question instance
                            Cursor resourceInstanceCursor = resolver.query(resourceInstanceUri, null, ResourceInstanceColumns.QUESTION_INSTANCE_ID + "=" + questInstance.getId() + " and " + ResourceInstanceColumns.TYPE + " = '" + ResourceType.IMAGE + "'", null, null);
                            if(resourceInstanceCursor!=null){
                                while(resourceInstanceCursor.moveToNext()){
                                    String imgPath = resourceInstanceCursor.getString(resourceInstanceCursor.getColumnIndexOrThrow(ResourceInstanceColumns.PATH));
                                    int imgId = resourceInstanceCursor.getInt(resourceInstanceCursor.getColumnIndexOrThrow(ResourceInstanceColumns._ID));
                                    String imgDescription = resourceInstanceCursor.getString(resourceInstanceCursor.getColumnIndexOrThrow(ResourceInstanceColumns.DESCRIPTION));
                                    String imgType = resourceInstanceCursor.getString(resourceInstanceCursor.getColumnIndexOrThrow(ResourceInstanceColumns.TYPE));
                                    Uri uri = Uri.parse(imgPath);
                                    //TODO: modify this for audio also
                                    Resource img = new Resource(uri, imgDescription, ResourceType.IMAGE, imgId);
                                    questInstance.addPicture(img);
                                }
                                resourceInstanceCursor.close();
                            }
                            //retrieve notes!
                            Cursor noteCursor = resolver.query(noteUri, null, NoteColumns.QUESTION_INSTANCE_ID + " = " + questInstance.getId(), null, null);
                            if(noteCursor!=null){
                                while(noteCursor.moveToNext()){
                                    int noteID = noteCursor.getInt(noteCursor.getColumnIndexOrThrow(NoteColumns._ID));
                                    String noteString = noteCursor.getString(noteCursor.getColumnIndexOrThrow(NoteColumns.TEXT));
                                    Note note = new Note(noteID, noteString);
                                    questInstance.addNote(note);
                                }
                                noteCursor.close();
                            }


                            sectInst.addQuestionInstance(questInstance);

                            generateAnswers(questInstance);
                        }
                        questInstanceCursor.close();
                    }
                }
                sectInstCursor.close();
            }

        }
        return instance;

    }

    private FormInstance newOpen(int formID, int taskID){
        int id;
        Form form;
        FormInstance instance = null;
        int sectionID;
        int questionID;
        Uri formInstanceUri = DatabaseContentProvider.FORM_INSTANCE_URI;
        Uri sectionInstanceUri = DatabaseContentProvider.SECTION_INSTANCE_URI;
        Uri questionInstanceUri = DatabaseContentProvider.QUESTION_INSTANCE_URI;

        ContentResolver resolver = context.getContentResolver();
        String[] projection = {FormColumns._ID, FormColumns.NAME, FormColumns.TEXT_NAME, FormColumns.DESCRIPTION};
        Uri uri = Uri.parse(DatabaseContentProvider.FORM_URI + "/"+formID);
        Cursor cursor = resolver.query(uri, projection , null, null, null);
        if(cursor!=null){
            cursor.moveToFirst();
            formID = cursor.getInt(cursor.getColumnIndexOrThrow(FormColumns._ID));
            String formName = cursor.getString(cursor.getColumnIndexOrThrow(FormColumns.NAME));
            String formTextName = cursor.getString(cursor.getColumnIndexOrThrow(FormColumns.TEXT_NAME));
            String formDescription = cursor.getString(cursor.getColumnIndexOrThrow(FormColumns.DESCRIPTION));
            cursor.close();
            //set form attributes
            form = new Form(formID, formName, formTextName, formDescription);
            Long time = System.currentTimeMillis();
            String android_id = Settings.Secure.getString(resolver, Settings.Secure.ANDROID_ID);
            //set form instance attributes
            //TODO: set finalized correctly and task id
            instance = new FormInstance(0, form, time.toString(), android_id, taskID, false);
            //save instance in database
            ContentValues formInstValues = new ContentValues();
            //formInstValues.put(FormInstanceColumns._ID, formInstanceID);
            formInstValues.put(FormInstanceColumns.FORM_ID, form.getId());
            formInstValues.put(FormInstanceColumns.TIMESTAMP, instance.getTimestamp());
            formInstValues.put(FormInstanceColumns.DEVICE_ID, instance.getDeviceId());
            formInstValues.put(FormInstanceColumns.FINALIZED, instance.isFinalized()?1:0);
            formInstValues.put(FormInstanceColumns.TASK_ID, instance.getTaskId());
            formInstValues.put(FormInstanceColumns.TOTAL, 0);
            formInstValues.put(FormInstanceColumns.M_TOTAL, 0);
            formInstValues.put(FormInstanceColumns.FILLED, 0);
            formInstValues.put(FormInstanceColumns.NOT_FILLED, 0);
            formInstValues.put(FormInstanceColumns.NOT_APPLICABLE, 0);
            formInstValues.put(FormInstanceColumns.M_FILLED, 0);
            formInstValues.put(FormInstanceColumns.M_NOT_FILLED, 0);
            formInstValues.put(FormInstanceColumns.M_NOT_APPLICABLE, 0);

            Uri formElem = resolver.insert(formInstanceUri, formInstValues);
            //set correct id
            id = Integer.parseInt(formElem.getLastPathSegment());
            if(id < Util.BASE_ID){
                ContentValues val = new ContentValues();
                val.put(FormInstanceColumns._ID, Util.BASE_ID);
                resolver.update(DatabaseContentProvider.FORM_INSTANCE_URI, val, FormInstanceColumns._ID + " = " + id, null);
                instance.setId(Util.BASE_ID);
            }
            else{
                instance.setId(Integer.parseInt(formElem.getLastPathSegment()));
            }

            //find related sections!
            Uri sectionUri = DatabaseContentProvider.SECTION_URI;
            cursor = resolver.query(sectionUri, null, SectionColumns.FORM_ID + "= " + form.getId(), null, null);
            if(cursor!=null){
                while (cursor.moveToNext()){
                    sectionID = cursor.getInt(cursor.getColumnIndexOrThrow(SectionColumns._ID));
                    String sectionName = cursor.getString(cursor.getColumnIndexOrThrow(SectionColumns.NAME));
                    String sectionTextName = cursor.getString(cursor.getColumnIndexOrThrow(SectionColumns.TEXT_NAME));
                    String sectionDescription = cursor.getString(cursor.getColumnIndexOrThrow(SectionColumns.DESCRIPTION));
                    int sectionReq = cursor.getInt(cursor.getColumnIndexOrThrow(SectionColumns.REQUIRED));
                    int sectionRep = cursor.getInt(cursor.getColumnIndexOrThrow(SectionColumns.REPEAT));
                    //set section info
                    Section sect = new Section(sectionID, sectionName, sectionTextName, sectionDescription, sectionReq==1, sectionRep==1);
                    //TODO: modify index of section instance
                    SectionInstance sectInst = new SectionInstance(-1, 1, sect);
                    instance.addSectionInstance(sectInst);
                    form.addSection(sect);

                    //save section instance in DB
                    ContentValues sectInstValues = new ContentValues();
                    //sectInstValues.put(SectionInstanceColumns._ID, sectInst.getId());
                    sectInstValues.put(SectionInstanceColumns.INDEX, 1);
                    sectInstValues.put(SectionInstanceColumns.FORM_INSTANCE_ID, instance.getId());
                    sectInstValues.put(SectionInstanceColumns.SECTION_ID, sectInst.getSection().getId());
                    Uri sectInstUri = resolver.insert(sectionInstanceUri, sectInstValues);
                    //retrieve id of the section instance created
                    int sectInstId = Integer.parseInt(sectInstUri.getLastPathSegment());
                    //set correct id
                    if(sectInstId < Util.BASE_ID){
                        ContentValues val = new ContentValues();
                        val.put(SectionInstanceColumns._ID, Util.BASE_ID);
                        resolver.update(DatabaseContentProvider.SECTION_INSTANCE_URI, val, SectionInstanceColumns._ID + " = " + sectInstId, null);
                        sectInst.setId(Util.BASE_ID);
                    }
                    else{
                        sectInst.setId(sectInstId);
                    }

                    //find questions!!
                    Uri questionUri = DatabaseContentProvider.QUESTION_URI;
                    Cursor questionCursor = resolver.query(questionUri, null, QuestionColumns.SECTION_ID + "= " + sect.getId(), null, QuestionColumns._ID);

                    if(questionCursor!=null){
                        while (questionCursor.moveToNext()){
                            questionID = questionCursor.getInt(cursor.getColumnIndexOrThrow(QuestionColumns._ID));
                            String questionName = questionCursor.getString(cursor.getColumnIndexOrThrow(QuestionColumns.NAME));
                            String questionTextName = questionCursor.getString(cursor.getColumnIndexOrThrow(QuestionColumns.TEXT_NAME));
                            String questionDescription = questionCursor.getString(cursor.getColumnIndexOrThrow(QuestionColumns.DESCRIPTION));
                            int questionReq = questionCursor.getInt(cursor.getColumnIndexOrThrow(QuestionColumns.REQUIRED));
                            int questionRep = questionCursor.getInt(cursor.getColumnIndexOrThrow(QuestionColumns.REPEAT));
                            Question question = new Question(questionID, questionName,questionTextName, questionDescription, questionReq==1, questionRep==1);
                            //load relative images
                            loadBundleImages(question);
                            sect.addQuestion(question);
                            //question instance setup
                            QuestionInstance qInstance = new QuestionInstance(-1, 1, question);

                            sectInst.addQuestionInstance(qInstance);
                            //question instance init in database
                            ContentValues questionInstanceValues = new ContentValues();
                            //questionInstanceValues.put(QuestionInstanceColumns._ID, qInstance.getId());
                            questionInstanceValues.put(QuestionInstanceColumns.INDEX, 1);
                            questionInstanceValues.put(QuestionInstanceColumns.SECTION_INSTANCE_ID, sectInst.getId());
                            questionInstanceValues.put(QuestionInstanceColumns.QUESTION_ID, qInstance.getQuestion().getId());
                            Uri qInstIdUri = resolver.insert(questionInstanceUri, questionInstanceValues);
                            //set returned id for the q instance
                            int qId = Integer.parseInt(qInstIdUri.getLastPathSegment());

                            //set correct id
                            if(qId < Util.BASE_ID){
                                ContentValues val = new ContentValues();
                                val.put(QuestionInstanceColumns._ID, Util.BASE_ID);
                                resolver.update(DatabaseContentProvider.QUESTION_INSTANCE_URI, val, QuestionInstanceColumns._ID + " = " + qId, null);
                                qInstance.setId(Util.BASE_ID);
                            }
                            else{
                                qInstance.setId(qId);
                            }

                            generateAnswers(qInstance);
                        }
                        questionCursor.close();
                    }
                }
                cursor.close();
            }
        }
        //conclude the correct init of the stats
        instance.setTotalFields(totFields);
        instance.setNotFilled(totFields);
        instance.setmTotalFields(totMandatoryFields);
        instance.setmNotFilled(totMandatoryFields);


        ContentValues formInstValues = new ContentValues();

        formInstValues.put(FormInstanceColumns.TOTAL, totFields);
        formInstValues.put(FormInstanceColumns.NOT_FILLED, totFields);
        formInstValues.put(FormInstanceColumns.M_TOTAL, totMandatoryFields);
        formInstValues.put(FormInstanceColumns.M_NOT_FILLED, totMandatoryFields);

        context.getContentResolver().update(DatabaseContentProvider.FORM_INSTANCE_URI, formInstValues, FormInstanceColumns._ID + " = " + instance.getId(), null);

        return instance;
    }

    private void loadBundleImages(Question q){
        Uri pictures = DatabaseContentProvider.RESOURCE_URI;
        Cursor resCursor = context.getContentResolver().query(pictures, null, ResourceColumns.QUESTION_ID + " = " + q.getId(), null, null);
        while(resCursor.moveToNext()){
            int id = resCursor.getInt(resCursor.getColumnIndexOrThrow(ResourceColumns._ID));
            ResourceType type = ResourceType.getResourceTypeFromString(resCursor.getString(resCursor.getColumnIndexOrThrow(ResourceColumns.TYPE)));
            Uri path = Uri.parse(resCursor.getString(resCursor.getColumnIndexOrThrow(ResourceColumns.PATH)));
            String description = resCursor.getString(resCursor.getColumnIndexOrThrow(ResourceColumns.DESCRIPTION));
            if(type == ResourceType.IMAGE) {
                Resource res = new Resource(path, description, type, id);
                q.addPicture(res);
            }
        }
        resCursor.close();
    }

    private void generateAnswers(QuestionInstance questionInst){
        boolean flagNew = false;
        Uri fieldsUri = DatabaseContentProvider.FIELD_URI;
        Uri answerUri = DatabaseContentProvider.ANSWER_URI;
        //search for the section and then for the question!!
        //here we query for the fields!!
        //Look at form instances!!
        Question q = questionInst.getQuestion();

        if(q.fieldSize()==0){

            Cursor fieldCursor = context.getContentResolver().query(fieldsUri, null, FieldColumns.QUESTION_ID + "= " + q.getId(), null, null);
            if(fieldCursor!=null){
                while(fieldCursor.moveToNext()){
                    int id = fieldCursor.getInt(fieldCursor.getColumnIndexOrThrow(FieldColumns._ID));
                    String name = fieldCursor.getString(fieldCursor.getColumnIndexOrThrow(FieldColumns.NAME));
                    String textName = fieldCursor.getString(fieldCursor.getColumnIndexOrThrow(FieldColumns.TEXT_NAME));
                    String type = fieldCursor.getString(fieldCursor.getColumnIndexOrThrow(FieldColumns.TYPE));
                    int req = fieldCursor.getInt(fieldCursor.getColumnIndexOrThrow(FieldColumns.REQUIRED));
                    int rep = fieldCursor.getInt(fieldCursor.getColumnIndexOrThrow(FieldColumns.REPEAT));
                    Field field = new Field(id, name, textName, dummy.getDataTypeFromString(type), req==1? true:false, rep==1? true:false);
                    Answer ans = null;
                    Cursor ansCursor = context.getContentResolver().query(answerUri, null, AnswerColumns.FIELD_ID + "= " + field.getId() + " and " + AnswerColumns.QUESTION_INSTANCE_ID + " = " + questionInst.getId(), null, null);
                    //query for form instance and field -> if result is empty then create new...
                    //check for further setup for each field
                    int ansId = 0;
                    boolean prefilled = false;
                    boolean applicable = false;
                    boolean filled = false;
                    ContentValues values = new ContentValues();

                    //compute stat values!
                    totFields++;
                    totMandatoryFields+=req;

                    if(ansCursor.getCount()!=0){
                        ansCursor.moveToFirst();
                        ansId = ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns._ID));
                        prefilled = ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns.PREFILLED))==1;
                        applicable = ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns.APPLICABLE))==1;
                        filled =  ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns.FILLED))==1;
                    }
                    else{
                        //ID is auto increment!
                        //values.put(AnswerColumns._ID, a.getId());
                        values.put(AnswerColumns.PREFILLED, 0);
                        values.put(AnswerColumns.APPLICABLE, 1);
                        values.put(AnswerColumns.TYPE, field.getType().toString());
                        values.put(AnswerColumns.FIELD_ID, field.getId());
                        values.put(AnswerColumns.QUESTION_INSTANCE_ID, questionInst.getId());
                        values.put(AnswerColumns.FILLED, 0);
                        values.put(AnswerColumns.ANSWER_STRING, "");
                        values.put(AnswerColumns.ANSWER_INTEGER, 0);
                        values.put(AnswerColumns.ANSWER_FLOAT, 0);
                        flagNew = true;
                    }
                    switch(field.getType()){
                        case STRING:
                            if(ansCursor.getCount()!=0){
                                String answerStr =  ansCursor.getString(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_STRING));
                                ans = new StringAnswer(ansId, prefilled, applicable, filled, answerStr, field);
                            }
                            else{
                                ans = new StringAnswer(-1, false, true, false, "", field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case INTEGER:
                            if(ansCursor.getCount()!=0){
                                Integer answerInt =  ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_INTEGER));
                                ans = new IntegerAnswer(ansId, prefilled, applicable, filled, answerInt, field);
                            }
                            else{
                                ans = new IntegerAnswer(-1, false, true, false, 0, field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case POSITIVE_INTEGER:
                            if(ansCursor.getCount()!=0){
                                Integer answerInt =  ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_INTEGER));
                                ans = new PositiveIntegerAnswer(ansId, prefilled, applicable, filled, answerInt, field);
                            }
                            else{
                                ans = new PositiveIntegerAnswer(-1, false, true, false, 0, field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case NEGATIVE_INTEGER:
                            if(ansCursor.getCount()!=0){
                                Integer answerInt =  ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_INTEGER));
                                ans = new NegativeIntegerAnswer(ansId, prefilled, applicable, filled, answerInt, field);
                            }
                            else{
                                ans = new NegativeIntegerAnswer(-1, false, true, false, 0, field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case FLOAT:
                            if(ansCursor.getCount()!=0){
                                float answerFloat =  ansCursor.getFloat(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_FLOAT));
                                ans = new FloatAnswer(ansId, prefilled, applicable, filled, answerFloat, field);
                            }
                            else{
                                ans = new FloatAnswer(-1, false, true, false, 0, field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case POSITIVE_FLOAT:
                            if(ansCursor.getCount()!=0){
                                float answerFloat =  ansCursor.getFloat(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_FLOAT));
                                ans = new PositiveFloatAnswer(ansId, prefilled, applicable, filled, answerFloat, field);
                            }
                            else{
                                ans = new PositiveFloatAnswer(-1, false, true, false, 0, field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case NEGATIVE_FLOAT:
                            if(ansCursor.getCount()!=0){
                                float answerFloat =  ansCursor.getFloat(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_FLOAT));
                                ans = new NegativeFloatAnswer(ansId, prefilled, applicable, filled, answerFloat, field);
                            }
                            else{
                                ans = new NegativeFloatAnswer(-1, false, true, false, 0, field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case MEASURE:
                            if(ansCursor.getCount()!=0){
                                float answerMeasure =  ansCursor.getFloat(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_FLOAT));
                                String um = ansCursor.getString(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_STRING));
                                ans = new MeasureAnswer(ansId, prefilled, applicable, filled, answerMeasure, um, field);
                            }
                            else{
                                ans = new MeasureAnswer(-1, false, true, false, 0, "m", field);
                                values.put(AnswerColumns.ANSWER_STRING, "m");
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }

                            break;
                        case ISTAT_CODE:
                            if(ansCursor.getCount()!=0){
                                String answerIstat =  ansCursor.getString(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_STRING));
                                ans = new IstatAnswer(ansId, prefilled, applicable, filled, answerIstat, field);

                            }
                            else{
                                ans = new IstatAnswer(-1, false, true, false, "", field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case GPS:
                            if(ansCursor.getCount()!=0){
                                String answerGPS =  ansCursor.getString(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_STRING));
                                ans = new GpsAnswer(ansId, prefilled, applicable, filled, answerGPS, field);
                            }
                            else{
                                ans = new GpsAnswer(-1, false, true, false, "", field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case DATE:
                            Calendar cal = Calendar.getInstance();
                            if(ansCursor.getCount()!=0){
                                String answerDate =  ansCursor.getString(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_STRING));
                                try{
                                    Long date = Long.parseLong(answerDate);
                                    ans = new DateAnswer(ansId, prefilled, applicable, filled, false, new Date(date), field);
                                }
                                catch (NumberFormatException e){
                                    ans = new DateAnswer(ansId, prefilled, applicable, filled, false, cal.getTime() , field);
                                }
                            }
                            else{
                                ans = new DateAnswer(-1, false, true, false, false, cal.getTime(), field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case DATE_TIME:
                            Calendar calTime = Calendar.getInstance();
                            if(ansCursor.getCount()!=0){
                                String answerDateTime =  ansCursor.getString(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_STRING));
                                try{
                                    Long date = Long.parseLong(answerDateTime);
                                    ans = new DateAnswer(ansId, prefilled, applicable, filled, true, new Date(date), field);
                                }
                                catch (NumberFormatException e){
                                    ans = new DateAnswer(ansId, prefilled, applicable, filled, true, calTime.getTime() , field);
                                }
                            }
                            else{
                                ans = new DateAnswer(-1, false, true, false, true, calTime.getTime(), field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            break;
                        case SELECT:
                            Uri ChoiceUri = DatabaseContentProvider.CHOICE_URI;
                            Cursor choiceCursor = context.getContentResolver().query(ChoiceUri, null, ChoiceColumns.FIELD_ID + "= " + field.getId(), null, null);
                            ArrayList<String> choices = new ArrayList<String>();
                            ArrayList<Boolean> other = new ArrayList<Boolean>();
                            while (choiceCursor.moveToNext()){

                                String choiceText = choiceCursor.getString(choiceCursor.getColumnIndexOrThrow(ChoiceColumns.TEXT));
                                Boolean otherField = choiceCursor.getInt(choiceCursor.getColumnIndexOrThrow(ChoiceColumns.OTHER))==1;
                                choices.add(choiceText);
                                other.add(otherField);
                            }
                            field.setChoices(choices, other);

                            if(ansCursor.getCount()!=0){
                                ansCursor.moveToFirst();
                                String answerStr =  ansCursor.getString(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_STRING));
                                ans = new SelectAnswer(ansId, prefilled, applicable, filled, answerStr, field);

                            }
                            else {
                                ans = new SelectAnswer(-1, false, true, false, "", field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            choiceCursor.close();
                            break;
                        case SELECT1:
                            Uri ChoiceUri2 = DatabaseContentProvider.CHOICE_URI;
                            Cursor choiceCursor2 = context.getContentResolver().query(ChoiceUri2, null, ChoiceColumns.FIELD_ID + "= " + field.getId(), null, null);
                            ArrayList<String> choices2 = new ArrayList<String>();
                            ArrayList<Boolean> other2 = new ArrayList<Boolean>();
                            while (choiceCursor2.moveToNext()){

                                String choiceText = choiceCursor2.getString(choiceCursor2.getColumnIndexOrThrow(ChoiceColumns.TEXT));
                                Boolean otherField = choiceCursor2.getInt(choiceCursor2.getColumnIndexOrThrow(ChoiceColumns.OTHER))==1;
                                choices2.add(choiceText);
                                other2.add(otherField);
                            }
                            field.setChoices(choices2, other2);

                            if(ansCursor.getCount()!=0){
                                ansCursor.moveToFirst();
                                String answerStr =  ansCursor.getString(ansCursor.getColumnIndexOrThrow(AnswerColumns.ANSWER_STRING));
                                ans = new Select1Answer(ansId, prefilled, applicable, filled, answerStr, field);
                            }
                            else {
                                ans = new Select1Answer(-1, false, true, false, "", field);
                                Uri uri = context.getContentResolver().insert(answerUri, values);
                                ans.setId(Integer.parseInt(uri.getLastPathSegment()));
                            }
                            choiceCursor2.close();
                            break;
                        case SKETCH:
                            break;
                    }
                    if(flagNew && ans.getId() < Util.BASE_ID){
                        //set correct id
                        ContentValues val = new ContentValues();
                        val.put(QuestionInstanceColumns._ID, Util.BASE_ID);
                        context.getContentResolver().update(DatabaseContentProvider.ANSWER_URI, val, AnswerColumns._ID + " = " + ans.getId(), null);
                        ans.setId(Util.BASE_ID);

                    }
                    if(ans!=null) {
                        questionInst.addAnswer(ans);
                        q.addField(field);
                    }
                    ansCursor.close();

                }
                fieldCursor.close();
            }
        }
    }



}
