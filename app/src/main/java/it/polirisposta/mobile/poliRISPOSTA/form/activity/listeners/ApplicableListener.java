package it.polirisposta.mobile.poliRISPOSTA.form.activity.listeners;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.ContentFragment;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DataType;

/**
 * Created by Rolando on 06/04/15.
 */
public class ApplicableListener implements CompoundButton.OnCheckedChangeListener{
    View fieldView;
    DataType type;
    ContentFragment content;

    public ApplicableListener(View fieldView, DataType type, ContentFragment content){
        this.fieldView=fieldView;
        this.type=type;
        this.content=content;
    }

    /**
     * Called when the checked state of a compound button has changed.
     *
     * @param buttonView The compound button view whose state has changed.
     * @param isChecked  The new checked state of buttonView.
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (type){
            case STRING:
            case INTEGER:
            case POSITIVE_INTEGER:
            case NEGATIVE_INTEGER:
            case FLOAT:
            case POSITIVE_FLOAT:
            case ISTAT_CODE:
            case NEGATIVE_FLOAT:
                fieldView.findViewById(R.id.textEditText).setEnabled(!isChecked);
                break;
            case MEASURE:
                fieldView.findViewById(R.id.measureSpinner).setEnabled(!isChecked);
                fieldView.findViewById(R.id.measureEditText).setEnabled(!isChecked);
                break;
            case GPS:
                fieldView.findViewById(R.id.gpsGetPosition).setEnabled(!isChecked);
                break;
            case DATE:
                fieldView.findViewById(R.id.datePicker).setEnabled(!isChecked);
                break;
            case DATE_TIME:
                fieldView.findViewById(R.id.datePicker).setEnabled(!isChecked);
                fieldView.findViewById(R.id.timePicker).setEnabled(!isChecked);
                break;
            case SELECT:
                LinearLayout l = (LinearLayout) fieldView.findViewById(R.id.checkGroup);
                for(int i=0; i<l.getChildCount();i++){
                    if(l.getChildAt(i) instanceof CheckBox){
                        l.getChildAt(i).setEnabled(!isChecked);
                    }
                    else if(l.getChildAt(i) instanceof LinearLayout){
                        LinearLayout other = (LinearLayout) l.getChildAt(i);
                        other.findViewById(R.id.otherCheckBox).setEnabled(!isChecked);
                        other.findViewById(R.id.otherText).setEnabled(!isChecked);
                    }
                }
                break;
            case SELECT1:
                RadioGroup group = (RadioGroup) fieldView.findViewById(R.id.radioGroup);
                for(int i=0; i<group.getChildCount(); i++){
                    if(group.getChildAt(i) instanceof RadioButton){
                        group.getChildAt(i).setEnabled(!isChecked);
                    }
                    else if(group.getChildAt(i) instanceof LinearLayout){
                        LinearLayout other = (LinearLayout) group.getChildAt(i);
                        other.findViewById(R.id.otherRadio).setEnabled(!isChecked);
                        other.findViewById(R.id.otherText).setEnabled(!isChecked);
                    }
                }

                break;
            case SKETCH:
                break;
        }
        content.correctnessChecking(fieldView, type);

    }

}
