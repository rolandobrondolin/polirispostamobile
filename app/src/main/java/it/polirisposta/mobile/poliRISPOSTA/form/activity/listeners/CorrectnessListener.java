package it.polirisposta.mobile.poliRISPOSTA.form.activity.listeners;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;

import it.polirisposta.mobile.poliRISPOSTA.form.activity.ContentFragment;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DataType;

/**
 * Created by Rolando on 06/04/15.
 */
public class CorrectnessListener implements TextWatcher {
    View fieldView;
    ContentFragment content;
    DataType type;

    public CorrectnessListener(View fieldView, ContentFragment content, DataType type){
        this.fieldView=fieldView;
        this.content = content;
        this.type = type;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        content.correctnessChecking(fieldView, type);
    }
}
