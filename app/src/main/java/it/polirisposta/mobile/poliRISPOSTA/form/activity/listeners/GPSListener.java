package it.polirisposta.mobile.poliRISPOSTA.form.activity.listeners;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import it.polirisposta.mobile.poliRISPOSTA.R;

/**
 * Created by Rolando on 17/04/15.
 */
//TODO: implement other methods of location listener
public class GPSListener implements LocationListener{

    Context context;
    View field;
    String result;
    ProgressDialog diag;

    public GPSListener(Context context, View field, ProgressDialog diag) {
        super();
        this.context=context;
        this.field = field;
        this.diag = diag;
    }

    @Override
    public void onLocationChanged(Location location) {
        GPSListener.this.result = location.getLatitude() + " " + location.getLongitude() + " " + location.getAccuracy();

        if(diag!=null){
            if(diag.isShowing() && field.getParent()!=null){
                diag.dismiss();
                TextView text = (TextView) field.findViewById(R.id.gpsDisplayPosition);
                text.setText(result);
            }
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


}
