package it.polirisposta.mobile.poliRISPOSTA.form.activity.listeners;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.QuestionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.Resource;

/**
 * Created by Rolando on 20/04/15.
 */
public class ImageListener implements View.OnClickListener {

    private Context context;
    private Uri resourceUri;
    private QuestionInstance qInst;
    private View image;

    public ImageListener(Context context, Uri resourceUri, QuestionInstance qInst, View image){
        this.context=context;
        this.resourceUri = resourceUri;
        this.qInst=qInst;
        this.image = image;
    }



    @Override
    public void onClick(View v) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        ImageView img = new ImageView(context);
        img.setImageResource(R.drawable.add_picture);

        try {
            InputStream stream = context.getContentResolver().openInputStream(resourceUri);
            Bitmap thumbImage = BitmapFactory.decodeStream(stream);
            img.setImageBitmap(thumbImage);
            stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        builder.setView(img);
        builder.setNegativeButton("Rimuovi", new RemoveImage());

        builder.setNeutralButton("Annulla", null);
        builder.show();
    }

    public class RemoveImage implements DialogInterface.OnClickListener {

        /**
         * This method will be invoked when a button in the dialog is clicked.
         *
         * @param dialog The dialog that received the click.
         * @param which  The button that was clicked (e.g.
         *               {@link android.content.DialogInterface#BUTTON1}) or the position
         */
        @Override
        public void onClick(DialogInterface dialog, int which) {
            //if(which==0){
                ImageListener.this.context.getContentResolver().delete(DatabaseContentProvider.RESOURCE_INSTANCE_URI, PoliRispostaDbContract.ResourceInstanceColumns.PATH +
                        " = '" + ImageListener.this.resourceUri.toString() + "' and " + PoliRispostaDbContract.ResourceInstanceColumns.QUESTION_INSTANCE_ID + " = " +
                        qInst.getId(), null);
                Resource removed = null;
                for(Resource r : qInst.getPictures()){
                    if(r.getUri().compareTo(ImageListener.this.resourceUri)==0){
                        removed = r;
                        break;
                    }
                }
                if(removed!=null){
                    qInst.getPictures().remove(removed);
                    ((LinearLayout)image.getParent()).removeView(image);
                }
            }
        //}
    }


}
