package it.polirisposta.mobile.poliRISPOSTA.form.activity.listeners;

import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;

import it.polirisposta.mobile.poliRISPOSTA.R;

/**
 * Created by Rolando on 06/04/15.
 */
public class SpinnerListener implements AdapterView.OnItemSelectedListener{

    int oldSelection;

    public SpinnerListener(int old){
        super();
        this.oldSelection = old;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        LinearLayout fieldView = (LinearLayout) parent.getParent();
        EditText txt = (EditText) fieldView.findViewById(R.id.measureEditText);
        if(!txt.getText().toString().equals("") && oldSelection!=position){
            double value = Double.parseDouble(txt.getText().toString());
            value = value*Math.pow(10, oldSelection-position);
            txt.setText(Double.toString(value));

        }
        oldSelection=position;
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
