package it.polirisposta.mobile.poliRISPOSTA.launch.activity;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by rolando on 25/05/15.
 */
public class DownloadAdapter implements ExpandableListAdapter{
    private ArrayList<DownloadElement> localForms;
    private ArrayList<DownloadElement> remoteForms;
    private ArrayList<DownloadElement> tasks;
    private Context context;

    public DownloadAdapter(Context context) {
        localForms= new ArrayList<>();
        remoteForms = new ArrayList<>();
        tasks = new ArrayList<>();
        this.context = context;
    }

    public ArrayList<DownloadElement> getLocalForms() {
        return localForms;
    }

    public void setLocalForms(ArrayList<DownloadElement> localForms) {
        this.localForms = localForms;
    }

    public ArrayList<DownloadElement> getRemoteForms() {
        return remoteForms;
    }

    public void setRemoteForms(ArrayList<DownloadElement> remoteForms) {
        this.remoteForms = remoteForms;
    }

    public ArrayList<DownloadElement> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<DownloadElement> tasks) {
        this.tasks = tasks;
    }

    public void addElement(DownloadElement element){
        switch (element.getType()) {
            case TASK:
                tasks.add(element);
                break;
            case FORM:
                remoteForms.add(element);
                break;
            case LOCAL_FORM:
                localForms.add(element);
                break;
        }
    }



    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getGroupCount() {
        return 3;
    }

    @Override
    public int getChildrenCount(int i) {
        if(i==0){
            return localForms.size();
        }
        else if (i==1){
            return remoteForms.size();
        }
        else if (i==2){
            return tasks.size();
        }
        return 0;
    }

    @Override
    public Object getGroup(int i) {
        if(i==0){
            return "Form in locale";
        }
        else if (i==1){
            return "Form in remoto";
        }
        else if (i==2){
            return "Task in remoto";
        }
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        if(i==0){
            return localForms.get(i1);
        }
        else if (i==1){
            return remoteForms.get(i1);
        }
        else if (i==2){
            return tasks.get(i1);
        }
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_expandable_list_item_1, null);
        }
        String text = "";
        if(i==0){
            text = "Form in locale";
        }
        else if (i==1){
            text = "Form in remoto";
        }
        else if (i==2){
            text = "Task in remoto";
        }
        ((TextView)convertView.findViewById(android.R.id.text1)).setText(text);
        return convertView;

    }

    @Override
    public View getChildView(int i, int i1, boolean b, View convertView, ViewGroup viewGroup) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_list_item_2, null);
        }
        String text = "";
        String text2 = "";
        if(i==0){
            text = localForms.get(i1).getTextname();
            text2 = localForms.get(i1).getDescription();
        }
        else if (i==1){
            text = remoteForms.get(i1).getTextname();
            text2 = remoteForms.get(i1).getDescription();
        }
        else if (i==2){
            text = tasks.get(i1).getTextname();
            text2 = tasks.get(i1).getDescription();
        }

        ((TextView)convertView.findViewById(android.R.id.text1)).setText(text);
        ((TextView)convertView.findViewById(android.R.id.text2)).setText(text2);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int i) {

    }

    @Override
    public void onGroupCollapsed(int i) {

    }

    @Override
    public long getCombinedChildId(long l, long l1) {
        return l*1000+l1;
    }

    @Override
    public long getCombinedGroupId(long l) {
        return l;
    }
}
