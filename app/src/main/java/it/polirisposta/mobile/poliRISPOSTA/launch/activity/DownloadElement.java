package it.polirisposta.mobile.poliRISPOSTA.launch.activity;

/**
 * Created by rolando on 25/05/15.
 */
public class DownloadElement {
    private int id;
    private String textname;
    private String description;
    private DownloadType type;

    public DownloadElement(int id, String textname, String description, DownloadType type) {
        this.id = id;
        this.textname = textname;
        this.description = description;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTextname() {
        return textname;
    }

    public void setTextname(String textname) {
        this.textname = textname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DownloadType getType() {
        return type;
    }

    public void setType(DownloadType type) {
        this.type = type;
    }
}
