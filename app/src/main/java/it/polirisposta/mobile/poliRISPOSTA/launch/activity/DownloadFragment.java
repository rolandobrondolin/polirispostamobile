package it.polirisposta.mobile.poliRISPOSTA.launch.activity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask.FormDownloadAsyncTask;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask.NetworkAsyncTask;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask.TaskDownloadAsyncTask;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;

/**
 * Created by Rolando on 27/04/15.
 */
public class DownloadFragment extends android.support.v4.app.Fragment implements NetworkAsyncTask.PostResults {
    DownloadAdapter adapter;
    LoginData data;
    HashMap<RequestType, NetworkAsyncTask> activeTasks;
    int selectedElement;
    int selectedGroup;
    boolean visibleToUser = false;
    boolean creationCompleted = false;

    public interface LoginData{
        String getAddress();
        int getGroupID();
        String getPassword();
        String getUsername();
        String getSessionName();
        String getSessionID();
        String getCsrfTokenName();
        String getCsrfTokenID();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.data = (LoginData) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnRetrieveQuestion and SaveContent");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_download_manager, container, false);

        adapter = new DownloadAdapter(this.getActivity());
        adapter.setLocalForms(getLocalElements());
        //call the search routine in DB
        ExpandableListView list = (ExpandableListView) rootView.findViewById(R.id.downloadExpandableListView);
        list.setAdapter(adapter);
        list.expandGroup(0);
        list.expandGroup(1);
        list.expandGroup(2);

        activeTasks = new HashMap<>();
        registerForContextMenu(list);

        creationCompleted = true;
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!visibleToUser){
            return;
        }
        connect();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.visibleToUser = isVisibleToUser;
        if(creationCompleted && isVisibleToUser){
            connect();
        }
    }

    private void connect(){
        if(data.getAddress().equals("")){
            Toast.makeText(getActivity(), "Erorre! Inserire i dati di accesso al server!", Toast.LENGTH_LONG).show();
            return;
        }
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if(isConnected){

            if(activeTasks.get(RequestType.GET_FORMS)!= null){
                if(!activeTasks.get(RequestType.GET_FORMS).getStatus().equals(AsyncTask.Status.RUNNING)){
                    NetworkAsyncTask task = new NetworkAsyncTask(getActivity(), RequestType.GET_FORMS, null, this, data.getAddress(), data.getUsername(), data.getGroupID(), data.getPassword(), data.getSessionName(), data.getSessionID(), data.getCsrfTokenName(), data.getCsrfTokenID(), false);
                    task.execute();
                    activeTasks.put(RequestType.GET_FORMS, task);
                }
            }
            else{
                NetworkAsyncTask task = new NetworkAsyncTask(getActivity(), RequestType.GET_FORMS, null, this, data.getAddress(),  data.getUsername(), data.getGroupID(), data.getPassword(), data.getSessionName(), data.getSessionID(), data.getCsrfTokenName(), data.getCsrfTokenID(), false);
                task.execute();
                activeTasks.put(RequestType.GET_FORMS, task);
            }
            if(activeTasks.get(RequestType.GET_TASKS)!=null){
                if(!activeTasks.get(RequestType.GET_TASKS).getStatus().equals(AsyncTask.Status.RUNNING)){
                    NetworkAsyncTask task = new NetworkAsyncTask(getActivity(), RequestType.GET_TASKS, null, this, data.getAddress(),  data.getUsername(), data.getGroupID(), data.getPassword(), data.getSessionName(), data.getSessionID(), data.getCsrfTokenName(), data.getCsrfTokenID(), false);
                    task.execute();
                    activeTasks.put(RequestType.GET_TASKS, task);
                }
            }
            else{
                NetworkAsyncTask task = new NetworkAsyncTask(getActivity(), RequestType.GET_TASKS, null, this, data.getAddress(),  data.getUsername(), data.getGroupID(), data.getPassword() , data.getSessionName(), data.getSessionID(), data.getCsrfTokenName(), data.getCsrfTokenID(), false);
                task.execute();
                activeTasks.put(RequestType.GET_TASKS, task);
            }


        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;

        selectedElement = ExpandableListView.getPackedPositionChild(info.packedPosition);
        selectedGroup = ExpandableListView.getPackedPositionGroup(info.packedPosition);

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_context_download, menu);
        if(selectedElement!=-1){
            menu.setHeaderTitle("Opzioni download");
            if(selectedGroup==0 ){
                menu.findItem(R.id.menuDownloadForm).setVisible(false);
                menu.findItem(R.id.menuRemove).setVisible(true);
                menu.findItem(R.id.menuDownloadTask).setVisible(false);
            }
            if(selectedGroup==1 ){
                menu.findItem(R.id.menuDownloadForm).setVisible(true);
                menu.findItem(R.id.menuRemove).setVisible(false);
                menu.findItem(R.id.menuDownloadTask).setVisible(false);
            }
            if(selectedGroup==2 ){
                menu.findItem(R.id.menuDownloadForm).setVisible(false);
                menu.findItem(R.id.menuRemove).setVisible(false);
                menu.findItem(R.id.menuDownloadTask).setVisible(true);
            }
        }
        else{
            menu.findItem(R.id.menuDownloadForm).setVisible(false);
            menu.findItem(R.id.menuRemove).setVisible(false);
            menu.findItem(R.id.menuDownloadTask).setVisible(false);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.menuRemove){
            //remove form and tasks with that form id inside
            int formID = adapter.getLocalForms().get(selectedElement).getId();

            Cursor taskModuleCursor = getActivity().getContentResolver().query(DatabaseContentProvider.TASK_MODULE_URI, null, PoliRispostaDbContract.TaskModuleColumns.FORM_ID + " = " + formID, null, null);
            if (taskModuleCursor!=null){
                HashMap<Integer, Integer> bah = new HashMap<>();
                while(taskModuleCursor.moveToNext()){
                    int taskID = taskModuleCursor.getInt(taskModuleCursor.getColumnIndexOrThrow(PoliRispostaDbContract.TaskModuleColumns.TASK_ID));
                    bah.put(taskID, taskID);
                }
                taskModuleCursor.close();
                adapter.getLocalForms().remove(selectedElement);

                ExpandableListView list = (ExpandableListView) getView().findViewById(R.id.downloadExpandableListView);
                list.invalidateViews();
                list.invalidate();
                list.setAdapter(adapter);
                list.expandGroup(0);
                list.expandGroup(1);
                list.expandGroup(2);
                for(Integer i: bah.keySet()){
                    getActivity().getContentResolver().delete(DatabaseContentProvider.TASK_URI, PoliRispostaDbContract.TaskColumns._ID + " = " + i, null);
                }
            }

            getActivity().getContentResolver().delete(DatabaseContentProvider.FORM_URI, PoliRispostaDbContract.FormColumns._ID + " = " + formID, null);

        }
        else if(item.getItemId()==R.id.menuDownloadForm){
            //download form
            FormDownloadAsyncTask task = new FormDownloadAsyncTask(adapter.getRemoteForms().get(selectedElement).getId(), getActivity(), this, data.getAddress(), data.getSessionName(), data.getSessionID(), data.getCsrfTokenName(), data.getCsrfTokenID());
            task.execute();
        }
        else if(item.getItemId()==R.id.menuDownloadTask){
            //download task
            TaskDownloadAsyncTask task = new TaskDownloadAsyncTask(adapter.getTasks().get(selectedElement).getId(), getActivity(), this, data.getAddress(), data.getSessionName(), data.getSessionID(), data.getCsrfTokenName(), data.getCsrfTokenID());
            task.execute();
        }

        return super.onContextItemSelected(item);
    }


    private ArrayList<DownloadElement> getLocalElements(){
        ArrayList<DownloadElement> listElements = new ArrayList<>();
        ContentResolver resolver = getActivity().getContentResolver();

        Cursor forms = resolver.query(DatabaseContentProvider.FORM_URI, null, null, null, null);
        if (forms!=null){
            while(forms.moveToNext()){
                int id = forms.getInt(forms.getColumnIndexOrThrow(PoliRispostaDbContract.FormColumns._ID));
                String textName = forms.getString(forms.getColumnIndexOrThrow(PoliRispostaDbContract.FormColumns.TEXT_NAME));
                String description = forms.getString(forms.getColumnIndexOrThrow(PoliRispostaDbContract.FormColumns.DESCRIPTION));
                DownloadType type = DownloadType.LOCAL_FORM;

                DownloadElement elem = new DownloadElement(id, textName, description, type);
                listElements.add(elem);
            }

            forms.close();


        }


        return listElements;
    }

    @Override
    public void updateView(RequestType request, JSONObject response) {
        switch (request){
            case GET_FORMS:
                parseFormsJSON(response);
                break;
            case GET_TASKS:
                parseTasksJSON(response);
                break;
            default:
                break;
        }

    }

    @Override
    public void updateView(RequestType request, boolean response) {
        if(response){
            Toast.makeText(getActivity(), "Download completato correttamente!", Toast.LENGTH_LONG).show();
            ExpandableListView list = (ExpandableListView) getView().findViewById(R.id.downloadExpandableListView);
            ArrayList<DownloadElement> localForms = getLocalElements();
            adapter.setLocalForms(localForms);
            list.invalidateViews();
            list.invalidate();
            list.setAdapter(adapter);
            list.expandGroup(0);
            list.expandGroup(1);
            list.expandGroup(2);
        }
        else{
            Toast.makeText(getActivity(), "Download non completato, riprovare!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void reportError(String message) {
        Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

    }

    private void parseFormsJSON(JSONObject jsonObject){
        JSONArray array;
        try {
            ArrayList<DownloadElement> listDwnElem = new ArrayList<>();
            array = jsonObject.getJSONArray("data");
            for(int i=0; i<array.length(); i++){
                JSONArray elem = array.getJSONArray(i);
                int id = elem.getInt(0);
                String textname = elem.getString(1);
                String description = elem.getString(2);
                DownloadElement dwnElem = new DownloadElement(id, textname, description, DownloadType.FORM);
                listDwnElem.add(dwnElem);
            }
            adapter.setRemoteForms(listDwnElem);
            ExpandableListView list = (ExpandableListView) getActivity().findViewById(R.id.downloadExpandableListView);
            list.collapseGroup(1);
            list.expandGroup(1);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void parseTasksJSON(JSONObject jsonObject){
        JSONArray array;
        try {
            ArrayList<DownloadElement> listTskElem = new ArrayList<>();
            array = jsonObject.getJSONArray("data");
            for(int i=0; i<array.length(); i++){
                JSONArray elem = array.getJSONArray(i);
                int id = elem.getInt(0);
                String name = elem.getString(1);
                String description = elem.getString(2);
                DownloadElement dwnElem = new DownloadElement(id, name, description, DownloadType.TASK);
                listTskElem.add(dwnElem);
            }
            adapter.setTasks(listTskElem);
            ExpandableListView list = (ExpandableListView) getActivity().findViewById(R.id.downloadExpandableListView);
            list.collapseGroup(2);
            list.expandGroup(2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
