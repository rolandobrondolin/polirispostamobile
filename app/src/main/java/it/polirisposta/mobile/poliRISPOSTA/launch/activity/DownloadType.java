package it.polirisposta.mobile.poliRISPOSTA.launch.activity;

/**
 * Created by rolando on 25/05/15.
 */
public enum DownloadType {
    TASK ("task"),
    FORM ("form"),
    LOCAL_FORM("local");

    private final String type;

    DownloadType(String value) {
        type = value;
    }

    public boolean equalsType(String type){
        return (type==null)? false: this.type.equals(type);
    }

    public DownloadType getDataTypeFromString (String type){
        switch (type) {
            case "task":
                return TASK;
            case "form":
                return FORM;
            default:
                return LOCAL_FORM;
        }
    }
}
