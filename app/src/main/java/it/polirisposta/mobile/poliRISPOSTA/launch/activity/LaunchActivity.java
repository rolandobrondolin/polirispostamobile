package it.polirisposta.mobile.poliRISPOSTA.launch.activity;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;


import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.FormActivity;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask.NetworkAsyncTask;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.search.activity.SearchActivity;


public class LaunchActivity extends ActionBarActivity implements ActionBar.TabListener, DownloadFragment.LoginData, NetworkAsyncTask.PostResults, LocationListener {

    private AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    private ViewPager mViewPager;

    private String address = "";
    private String password = "";
    private String username = "";
    private int groupID = 0;
    private String sessionName = "";
    private String sessionID = "";
    private String csrfTokenName = "";
    private String csrfTokenID = "";
    LocationManager locationManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //if(savedInstanceState==null){
            setContentView(R.layout.activity_launch);
            // Create the adapter that will return a fragment for each of the three primary sections
            // of the app.
            mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());

            // Set up the action bar.
            final ActionBar actionBar = getSupportActionBar();

            actionBar.setHomeButtonEnabled(false);

            // Specify that we will be displaying tabs in the action bar.
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

            mViewPager = (ViewPager) findViewById(R.id.pager);
            mViewPager.setAdapter(mAppSectionsPagerAdapter);
            mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    actionBar.setSelectedNavigationItem(position);
                }
            });

            // For each of the sections in the app, add a tab to the action bar.
            for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
                actionBar.addTab(actionBar.newTab().setText(mAppSectionsPagerAdapter.getPageTitle(i)).setTabListener(this));
            }
        //}


        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    }

    @Override
    public void onStart(){
        super.onStart();
        // Register the listener with the Location Manager to receive location updates
        //locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, this, null);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 120000, 20, this);
    }

    @Override
    public void onStop(){
        super.onStop();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onResume(){
        Cursor settingsCursor = getContentResolver().query(DatabaseContentProvider.SETTINGS_URI, null, null, null, null);
        if (settingsCursor != null) {
            if(settingsCursor.getCount()!=0){
                settingsCursor.moveToFirst();
                address = settingsCursor.getString(settingsCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SettingsColumns.ADDRESS));
                username = settingsCursor.getString(settingsCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SettingsColumns.USERNAME));
                password = settingsCursor.getString(settingsCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SettingsColumns.PASSWORD));
                sessionName = settingsCursor.getString(settingsCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SettingsColumns.SESSION_NAME));
                sessionID = settingsCursor.getString(settingsCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SettingsColumns.SESSION_ID));
                csrfTokenName = settingsCursor.getString(settingsCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SettingsColumns.CSRFTOKEN_NAME));
                csrfTokenID = settingsCursor.getString(settingsCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SettingsColumns.CSRFTOKEN));
                groupID = settingsCursor.getInt(settingsCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SettingsColumns.GROUP_ID));

                if(groupID!=0 && !csrfTokenID.equals("") && !sessionID.equals("")){
                    //i am logged in!
                    super.onResume();
                    return;
                }

                //try login
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                if(isConnected){
                    NetworkAsyncTask task = new NetworkAsyncTask(this, RequestType.LOGIN, null, this, getAddress(), getUsername(), getGroupID(), getPassword(), getSessionName(), getSessionID(), getCsrfTokenName(), getCsrfTokenID(), true);
                    task.execute();
                }
                else{
                    groupID = 0;
                }

            }
            else{
                address = "";
                username = "";
                groupID = 0;
                password = "";
                sessionID = "";
                csrfTokenID = "";
                csrfTokenName = "";
            }
            settingsCursor.close();
        }
        else{
            address = "";
            username = "";
            groupID = 0;
            password = "";
            sessionID = "";
            csrfTokenID = "";
            sessionName = "";
        }
        //TODO:check data inside database for correspondance to the current group id
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_launch, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            final LinearLayout dialogLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.settings_dialog_layout, null);

            EditText txt = (EditText) dialogLayout.findViewById(R.id.addressEdit);
            txt.setText(address);
            txt = (EditText) dialogLayout.findViewById(R.id.usernameEdit);
            txt.setText(username);
            txt = (EditText) dialogLayout.findViewById(R.id.passwordEdit);
            txt.setText(password);


            final AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Impostazioni");
            builder.setView(dialogLayout)
                    .setPositiveButton("Salva", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // sign in the user ...
                            final String oldUsername = username;
                            final int oldGroupID = groupID;
                            address = ((EditText) dialogLayout.findViewById(R.id.addressEdit)).getText().toString();
                            username = ((EditText) dialogLayout.findViewById(R.id.usernameEdit)).getText().toString();
                            password = ((EditText) dialogLayout.findViewById(R.id.passwordEdit)).getText().toString();

                            Cursor settingsCursor = LaunchActivity.this.getContentResolver().query(DatabaseContentProvider.SETTINGS_URI, null, null, null, null);
                            if (settingsCursor != null) {
                                final ContentValues values = new ContentValues();
                                values.put(PoliRispostaDbContract.SettingsColumns.ADDRESS, address);
                                values.put(PoliRispostaDbContract.SettingsColumns.USERNAME, username);
                                values.put(PoliRispostaDbContract.SettingsColumns.PASSWORD, password);
                                values.put(PoliRispostaDbContract.SettingsColumns.GROUP_ID, 0);
                                values.put(PoliRispostaDbContract.SettingsColumns.SESSION_ID, "");
                                values.put(PoliRispostaDbContract.SettingsColumns.SESSION_NAME, "");
                                values.put(PoliRispostaDbContract.SettingsColumns.CSRFTOKEN, "");
                                values.put(PoliRispostaDbContract.SettingsColumns.CSRFTOKEN_NAME, "");
                                final int count = settingsCursor.getCount();

                                settingsCursor.close();
                                dialog.dismiss();

                                if(!username.equals(oldUsername)){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(LaunchActivity.this);
                                    builder.setMessage("Stai per cambiare utente, questo cancellerà i dati attualmente salvati!").setTitle("Attenzione!");

                                    builder.setPositiveButton("Annulla", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            /*Cursor settingsCursor = LaunchActivity.this.getContentResolver().query(DatabaseContentProvider.SETTINGS_URI, null, null, null, null);
                                            if (settingsCursor != null) {
                                                ContentValues values = new ContentValues();
                                                values.put(PoliRispostaDbContract.SettingsColumns.USERNAME, oldUsername);
                                                values.put(PoliRispostaDbContract.SettingsColumns.GROUP_ID, oldGroupID);
                                                username = oldUsername;
                                                groupID = oldGroupID;
                                                LaunchActivity.this.getContentResolver().update(DatabaseContentProvider.SETTINGS_URI, values, PoliRispostaDbContract.SettingsColumns._ID + " = 1", null);

                                            }*/
                                            dialog.cancel();
                                        }
                                    });
                                    builder.setNegativeButton("Procedi", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            if (count == 0) {
                                                LaunchActivity.this.getContentResolver().insert(DatabaseContentProvider.SETTINGS_URI, values);
                                            } else {
                                                //launch a logout?
                                                LaunchActivity.this.getContentResolver().update(DatabaseContentProvider.SETTINGS_URI, values, PoliRispostaDbContract.SettingsColumns._ID + " = 1", null);
                                            }
                                            getContentResolver().delete(DatabaseContentProvider.TASK_URI, PoliRispostaDbContract.TaskColumns.USER_DEFINED + " != " + 1, null);

                                            Intent intent = new Intent(LaunchActivity.this, LaunchActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }
                                    });

                                    AlertDialog sureDialog = builder.create();
                                    sureDialog.show();
                                }
                                else{
                                    if (count == 0) {
                                        LaunchActivity.this.getContentResolver().insert(DatabaseContentProvider.SETTINGS_URI, values);
                                    } else {
                                        //launch a logout?
                                        LaunchActivity.this.getContentResolver().update(DatabaseContentProvider.SETTINGS_URI, values, PoliRispostaDbContract.SettingsColumns._ID + " = 1", null);
                                    }
                                    Intent intent = new Intent(LaunchActivity.this, LaunchActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }

                            }


                        }
                    })
                    .setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            builder.create().show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }


    public static class AppSectionsPagerAdapter extends FragmentStatePagerAdapter {

        Fragment todo, map, fragment;

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            todo = new ToDoFragment();
            map = new MapFragment();
            fragment = new DownloadFragment();
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    //Fragment todo = new ToDoFragment();
                    //Bundle todoArgs = new Bundle();
                    //todoArgs.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
                    //todo.setArguments(todoArgs);
                    return todo;
                case 1:
                    //Fragment map = new MapFragment();
                    //Bundle mapArgs = new Bundle();
                    //mapArgs.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
                    //map.setArguments(mapArgs);
                    return map;
                default:
                    //Fragment fragment = new DownloadFragment();
                    //Bundle args = new Bundle();
                    //args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
                    //fragment.setArguments(args);
                    return fragment;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return "Todo";
                case 1:
                    return "Mappa";
                default:
                    return "Downloads";
            }
        }

        public Fragment getDownloadFragment() {
            return fragment;
        }

        public Fragment getMap() {
            return map;
        }

        public Fragment getTodo() {
            return todo;
        }
    }

    @Override
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public int getGroupID() {
        return groupID;
    }

    @Override
    public String getSessionName() {
        return sessionName;
    }


    @Override
    public String getSessionID() {
        return sessionID;
    }


    @Override
    public String getCsrfTokenName() {
        return csrfTokenName;
    }


    @Override
    public String getCsrfTokenID() {
        return csrfTokenID;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ToDoFragment frag = (ToDoFragment) mAppSectionsPagerAdapter.getTodo();
        frag.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void updateView(RequestType request, JSONObject response) {
        try {
            boolean auth = response.getBoolean("authenticated");
            if(auth){
                //groupID = response.getInt("group_id");
                JSONArray arrayGroups = response.getJSONArray("group_id");
                sessionName = response.getString("session_cookie_name");
                sessionID = response.getString("session");
                csrfTokenName = response.getString("csrftoken_cookie_name");
                csrfTokenID = response.getString("csrftoken");
                int len = arrayGroups.length();
                groupID = arrayGroups.getInt(len-1);
                Cursor settingsCursor = LaunchActivity.this.getContentResolver().query(DatabaseContentProvider.SETTINGS_URI, null, null, null, null);
                if(settingsCursor!=null){
                    if(settingsCursor.getCount()>0){
                        ContentValues values = new ContentValues();
                        values.put(PoliRispostaDbContract.SettingsColumns.GROUP_ID, groupID);
                        values.put(PoliRispostaDbContract.SettingsColumns.SESSION_ID, sessionID);
                        values.put(PoliRispostaDbContract.SettingsColumns.SESSION_NAME, sessionName);
                        values.put(PoliRispostaDbContract.SettingsColumns.CSRFTOKEN, csrfTokenID);
                        values.put(PoliRispostaDbContract.SettingsColumns.CSRFTOKEN_NAME, csrfTokenName);
                        getContentResolver().update(DatabaseContentProvider.SETTINGS_URI, values, null, null);
                    }
                    settingsCursor.close();
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateView(RequestType request, boolean response) {

    }

    @Override
    public void reportError(String message) {

    }

//Location updates here!
    @Override
    public void onLocationChanged(Location location) {

        if(groupID!=0){
            double lat = location.getLatitude();
            double lon = location.getLongitude();
            double prec = location.getAccuracy();

            final JSONObject obj = new JSONObject();
            try {
                obj.put("group_id", groupID);
                obj.put("lat", lat);
                obj.put("lon", lon);
                obj.put("precision", prec);

                Runnable runner = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            int TIMEOUT_MILLISEC = 10000;  // = 10 seconds
                            HttpParams httpParams = new BasicHttpParams();
                            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
                            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
                            DefaultHttpClient client = new DefaultHttpClient(httpParams);

                            CookieStore cookieStore = new BasicCookieStore();
                            BasicClientCookie cookie = new BasicClientCookie(sessionName, sessionID);
                            BasicClientCookie csrfCookie = new BasicClientCookie(csrfTokenName, csrfTokenID);
                            URL pippo = new URL(address + "/tabletinterface/upload/position");
                            cookie.setDomain(pippo.getHost());
                            csrfCookie.setDomain(pippo.getHost());
                            cookieStore.addCookie(cookie);
                            cookieStore.addCookie(csrfCookie);
                            client.setCookieStore(cookieStore);

                            HttpPost request = new HttpPost(address + "/tabletinterface/upload/position");
                            //request.addHeader("Cookie",sessionName + " = " + sessionID + "; " + csrfTokenName + " = " + csrfToken + ";");
                            request.setHeader("Referer", pippo.getHost());
                            request.setHeader("X-CSRFToken", csrfTokenID);

                            request.setEntity(new ByteArrayEntity(obj.toString().getBytes("UTF8")));
                            HttpResponse response = client.execute(request);

                            String taskOut = EntityUtils.toString(response.getEntity());
                            JSONObject result = new JSONObject(taskOut);
                            String resContent = result.getString("success");

                            if(!resContent.equals("true")){
                                throw new JSONException("Wrong response");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (ClientProtocolException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                };

                Thread t = new Thread(runner);
                t.start();


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
