package it.polirisposta.mobile.poliRISPOSTA.launch.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask.NetworkAsyncTask;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.TaskMapInfo;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DataType;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.GpsAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.task.activity.TaskActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends android.support.v4.app.Fragment implements OnMapReadyCallback, NetworkAsyncTask.PostResults {

    private MapView map;
    private GoogleMap gMap;
    private MapMarkerAdapter adapter;
    private boolean mapReady;
    private DownloadFragment.LoginData data;
    private HashMap<String, TaskMapInfo> taskInfo = new HashMap<>();
    private boolean isVisible;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.data = (DownloadFragment.LoginData) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnRetrieveQuestion and SaveContent");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        map = (MapView) rootView.findViewById(R.id.map);
        map.onCreate(savedInstanceState);
        mapReady = false;
        map.getMapAsync(this);
        isVisible = false;
        if(savedInstanceState!=null){
            isVisible = savedInstanceState.getBoolean("visible");
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        map.onResume();
        if(mapReady){
            gMap.clear();
            populate();
            drawMap();
            if(isVisible){
                setUserVisibleHint(isVisible);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        gMap = googleMap;

        if(!mapReady){
            populate();
        }
        drawMap();
        mapReady = true;
        if(isVisible){
            setUserVisibleHint(isVisible);
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if(mapReady && isVisibleToUser){
            gMap.clear();
            populate();
            drawMap();
            NetworkAsyncTask task = new NetworkAsyncTask(getActivity(), RequestType.GET_AREA, new ArrayList<String>(), this, data.getAddress(), data.getUsername(), data.getGroupID(), data.getPassword(), data.getSessionName(), data.getSessionID(), data.getCsrfTokenName(), data.getCsrfTokenID(), true);
            task.execute();
            //Launch the position update thread
            NetworkAsyncTask task2 = new NetworkAsyncTask(getActivity(), RequestType.GET_POSITION, new ArrayList<String>(), MapFragment.this, data.getAddress(), data.getUsername(), data.getGroupID(), data.getPassword(), data.getSessionName(), data.getSessionID(), data.getCsrfTokenName(), data.getCsrfTokenID(), true);
            task2.execute();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        map.onSaveInstanceState(outState);
        outState.putBoolean("visible", isVisible);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        map.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        map.onDestroy();
        super.onDestroy();
    }

    private void populate(){
        Cursor c = getActivity().getContentResolver().query(DatabaseContentProvider.ANSWER_URI, null, PoliRispostaDbContract.AnswerColumns.TYPE + " = '" + DataType.GPS + "'", null, null);
        if(c!=null){
            while(c.moveToNext()){
                int qInstID = c.getInt(c.getColumnIndexOrThrow(PoliRispostaDbContract.AnswerColumns.QUESTION_INSTANCE_ID));
                String gps = c.getString(c.getColumnIndexOrThrow(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING));
                boolean prefilled = c.getInt(c.getColumnIndexOrThrow(PoliRispostaDbContract.AnswerColumns.PREFILLED))==1;
                boolean filled = c.getInt(c.getColumnIndexOrThrow(PoliRispostaDbContract.AnswerColumns.FILLED))==1;
                GpsAnswer ans = new GpsAnswer(gps);
                ans.setPrefilled(prefilled);
                ans.setFilled(filled);
                String lat = ans.getLat();
                String lon = ans.getLon();
                //useless to add empty markers
                if(ans.isFilled() || ans.isPrefilled()){
                    //find question instance, then section instance, then form instance, then task instance
                    Cursor qCursor = getActivity().getContentResolver().query(DatabaseContentProvider.QUESTION_INSTANCE_URI, null, PoliRispostaDbContract.QuestionInstanceColumns._ID + " = " + qInstID, null, null);
                    if(qCursor!=null){
                        qCursor.moveToFirst();
                        int sInstID = qCursor.getInt(qCursor.getColumnIndexOrThrow(PoliRispostaDbContract.QuestionInstanceColumns.SECTION_INSTANCE_ID));
                        Cursor sCursor = getActivity().getContentResolver().query(DatabaseContentProvider.SECTION_INSTANCE_URI, null, PoliRispostaDbContract.SectionInstanceColumns._ID + " = " + sInstID, null, null);
                        if(sCursor!=null){
                            sCursor.moveToFirst();
                            int fInstID = sCursor.getInt(sCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SectionInstanceColumns.FORM_INSTANCE_ID));
                            Cursor fCursor = getActivity().getContentResolver().query(DatabaseContentProvider.FORM_INSTANCE_URI, null, PoliRispostaDbContract.FormInstanceColumns._ID + " = " + fInstID, null, null);
                            if(fCursor!=null){
                                fCursor.moveToFirst();
                                int formID = fCursor.getInt(fCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormInstanceColumns.FORM_ID));
                                int taskID = fCursor.getInt(fCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormInstanceColumns.TASK_ID));
                                //stats here (maybe)!
                                Cursor formCursor = getActivity().getContentResolver().query(DatabaseContentProvider.FORM_URI, null, PoliRispostaDbContract.FormColumns._ID + " = " + formID, null, null);
                                if(formCursor!=null){
                                    formCursor.moveToFirst();
                                    String formName = formCursor.getString(formCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormColumns.TEXT_NAME));
                                    Cursor taskCursor = getActivity().getContentResolver().query(DatabaseContentProvider.TASK_URI, null, PoliRispostaDbContract.TaskColumns._ID + " = " + taskID, null, null);
                                    if(taskCursor!=null){
                                        taskCursor.moveToFirst();
                                        String taskName = taskCursor.getString(taskCursor.getColumnIndexOrThrow(PoliRispostaDbContract.TaskColumns.NAME));
                                        //create new marker, create new taskMapInfo, add all
                                        TaskMapInfo tInfo = new TaskMapInfo(taskID, taskName, formName, lat, lon);
                                        String markerID = gMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(ans.getLat()), Double.parseDouble(ans.getLon())))).getId();
                                        taskInfo.put(markerID, tInfo);
                                        taskCursor.close();
                                    }
                                    formCursor.close();
                                }
                                fCursor.close();
                            }

                            sCursor.close();
                        }
                        qCursor.close();
                    }
                }

            }
            c.close();
        }
    }

    private void drawMap(){
        gMap.getUiSettings().setZoomControlsEnabled(true);
        gMap.getUiSettings().setCompassEnabled(true);
        gMap.getUiSettings().setMyLocationButtonEnabled(true);

        gMap.getUiSettings().setRotateGesturesEnabled(true);
        gMap.getUiSettings().setScrollGesturesEnabled(true);
        gMap.getUiSettings().setTiltGesturesEnabled(true);
        gMap.getUiSettings().setZoomGesturesEnabled(true);
        gMap.setMyLocationEnabled(true);

        //set adapter!
        adapter = new MapMarkerAdapter(taskInfo, getActivity());
        gMap.setInfoWindowAdapter(adapter);
        gMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                TaskMapInfo tInfo = adapter.getTaskMapInfo(marker.getId());
                if (tInfo != null) {
                    Intent intent = new Intent(MapFragment.this.getActivity(), TaskActivity.class);
                    intent.putExtra("taskID", tInfo.getTaskDBID());
                    startActivity(intent);
                }
            }
        });

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (location != null)
        {
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }



    }

    @Override
    public void updateView(RequestType request, JSONObject response) {
        if(request.equals(RequestType.GET_AREA)){
            try {
                JSONArray array = response.getJSONArray("data");
                for(int i = 0; i< array.length(); i++){
                    JSONArray elem = array.getJSONArray(i);
                    double lat1 = elem.getDouble(2);
                    double lon1 = elem.getDouble(3);
                    double lat2 = elem.getDouble(4);
                    double lon2 = elem.getDouble(5);
                    if(mapReady){
                        PolygonOptions rectOptions = new PolygonOptions()
                                .add(new LatLng(lat1, lon1),
                                        new LatLng(lat1, lon2),
                                        new LatLng(lat2, lon2),
                                        new LatLng(lat2, lon1),
                                        new LatLng(lat1, lon1)).fillColor(Color.argb(37, 0, 0, 255)).strokeColor(Color.BLACK).strokeWidth((float) 1.75);

                        // Get back the mutable Polygon
                        gMap.addPolygon(rectOptions);
                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if(request.equals(RequestType.GET_POSITION)){
            try {
                JSONArray array = response.getJSONArray("data");
                for(int i = 0; i< array.length(); i++){
                    JSONArray elem = array.getJSONArray(i);
                    int id = elem.getInt(0);
                    int groupID = elem.getInt(1);
                    String time = elem.getString(2);
                    double prec = elem.getDouble(3);
                    double lat1 = elem.getDouble(4);
                    double lon1 = elem.getDouble(5);

                    if(mapReady && groupID!=data.getGroupID()){
                        gMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat1, lon1))
                                .title("Gruppo" + groupID)
                                .snippet(time)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateView(RequestType request, boolean response) {

    }

    @Override
    public void reportError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }
}
