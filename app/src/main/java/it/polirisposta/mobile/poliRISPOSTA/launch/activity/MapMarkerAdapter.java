package it.polirisposta.mobile.poliRISPOSTA.launch.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.model.TaskMapInfo;
import it.polirisposta.mobile.poliRISPOSTA.util.Util;

/**
 * Created by Rolando on 02/05/15.
 */
public class MapMarkerAdapter implements GoogleMap.InfoWindowAdapter {

    private HashMap<String, TaskMapInfo> taskInfos;
    private View myContentsView;

    public MapMarkerAdapter(HashMap<String, TaskMapInfo> taskInfos, Context context) {
        this.taskInfos = taskInfos;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myContentsView = inflater.inflate(R.layout.map_infoview, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        String id = marker.getId();

        if(taskInfos.get(id)==null){
            TextView v = (TextView) myContentsView.findViewById(R.id.taskMapTextView);
            v.setText(marker.getTitle());

            v = (TextView) myContentsView.findViewById(R.id.formMapTextView);
            v.setText("");
            v.setVisibility(View.INVISIBLE);

            v = (TextView) myContentsView.findViewById(R.id.latMapTextView);
            v.setText("Lat: " + marker.getPosition().latitude);

            v = (TextView) myContentsView.findViewById(R.id.lonMapTextView);
            v.setText("Lon: " + marker.getPosition().longitude);
        }
        else{
            TextView v = (TextView) myContentsView.findViewById(R.id.taskMapTextView);
            v.setText(taskInfos.get(id).getTaskName());

            v = (TextView) myContentsView.findViewById(R.id.formMapTextView);
            v.setText(taskInfos.get(id).getFormName());
            v.setVisibility(View.VISIBLE);

            v = (TextView) myContentsView.findViewById(R.id.latMapTextView);
            v.setText("Lat: " + taskInfos.get(id).getLat());

            v = (TextView) myContentsView.findViewById(R.id.lonMapTextView);
            v.setText("Lon: " + taskInfos.get(id).getLon());

        }

        return myContentsView;
    }

    public TaskMapInfo getTaskMapInfo(String id){
        return taskInfos.get(id);
    }

}
