package it.polirisposta.mobile.poliRISPOSTA.launch.activity;

/**
 * Created by rolando on 25/05/15.
 */
public enum RequestType {
    GET_FORMS,
    GET_TASKS,
    GET_FORM,
    GET_TASK,
    GET_AREA,
    GET_POSITION,
    POST_INSTANCE,
    POST_POSITION,
    LOGIN



}
