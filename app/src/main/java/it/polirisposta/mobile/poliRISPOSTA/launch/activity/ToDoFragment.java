package it.polirisposta.mobile.poliRISPOSTA.launch.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.FormActivity;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask.CheckTaskCompletionAsyncTask;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask.NetworkAsyncTask;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask.SendTaskAsyncTask;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.FormInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.Task;
import it.polirisposta.mobile.poliRISPOSTA.task.activity.NewTaskActivity;
import it.polirisposta.mobile.poliRISPOSTA.task.activity.TaskActivity;

/**
 * Created by Rolando on 27/04/15.
 */
public class ToDoFragment extends Fragment implements NetworkAsyncTask.PostResults, CheckTaskCompletionAsyncTask.CheckResults, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    int selectedTask;
    DownloadFragment.LoginData data;
    ArrayList<Mapping> tasks;
    MainAdapter adapter;
    final int RESOLVE_CONNECTION_REQUEST_CODE = 1000;
    GoogleApiClient mGoogleApiClient;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.data = (DownloadFragment.LoginData) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnRetrieveQuestion and SaveContent");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_todo, container, false);
        View listView = rootView.findViewById(R.id.editListView);



        ((ListView)listView).setAdapter(adapter);

        ((ListView)listView).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), TaskActivity.class);
                Mapping mapping = tasks.get(position);
                intent.putExtra("taskID", mapping.getTaskID());
                startActivity(intent);
            }
        });

        Button b = (Button) rootView.findViewById(R.id.newTaskButton);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ToDoFragment.this.getActivity(), NewTaskActivity.class);
                startActivity(intent);
            }
        });

        registerForContextMenu(listView);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        View listView = (ListView) getActivity().findViewById(R.id.editListView);
        tasks = new ArrayList<Mapping>();
        populateList();
        adapter = new MainAdapter(getActivity(), tasks);
        ((ListView)listView).setAdapter(adapter);
        ((ListView)listView).invalidateViews();
    }

    private void populateList(){
        int id = 0;
        Uri taskUri = DatabaseContentProvider.TASK_URI;

        Cursor taskCursor = getActivity().getContentResolver().query(taskUri, null, null, null, null);
        if(taskCursor!=null){
            while(taskCursor.moveToNext()){
                int taskId = taskCursor.getInt(taskCursor.getColumnIndexOrThrow(PoliRispostaDbContract.TaskColumns._ID));
                int listId = id;
                id++;
                String formTextName = taskCursor.getString(taskCursor.getColumnIndexOrThrow(PoliRispostaDbContract.TaskColumns.NAME));
                String formDescription = taskCursor.getString(taskCursor.getColumnIndexOrThrow(PoliRispostaDbContract.TaskColumns.DESCRIPTION));
                boolean userDefined = taskCursor.getInt(taskCursor.getColumnIndexOrThrow(PoliRispostaDbContract.TaskColumns.USER_DEFINED))==1;
                boolean sent = taskCursor.getInt(taskCursor.getColumnIndexOrThrow(PoliRispostaDbContract.TaskColumns.SENT))==1;
                Mapping m = new Mapping(listId, taskId, formTextName, formDescription, userDefined, sent);
                tasks.add(m);
            }
            taskCursor.close();
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getActivity().getMenuInflater().inflate(R.menu.menu_context_todo, menu);
        menu.setHeaderTitle("Opzioni todo");
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        selectedTask = info.position;

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.menuRemove){
            Mapping removed = tasks.remove(selectedTask);
            ((ListView)getActivity().findViewById(R.id.editListView)).invalidateViews();

            getActivity().getContentResolver().delete(DatabaseContentProvider.TASK_URI, PoliRispostaDbContract.TaskColumns._ID + " = " + removed.getTaskID(), null);

        }
        else if(item.getItemId() == R.id.menuSend){
            //check all forms are finalized
            //check all forms are inserted correctly
            CheckTaskCompletionAsyncTask checker = new CheckTaskCompletionAsyncTask(tasks.get(selectedTask).getTaskID(), getActivity(), this);
            checker.execute();

        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void updateView(RequestType request, JSONObject response) {

    }

    @Override
    public void updateView(RequestType request, boolean response) {
        if(response){
            tasks.get(selectedTask).setSent(true);
            ListView list = (ListView) getActivity().findViewById(R.id.editListView);
            list.invalidateViews();
            list.setAdapter(adapter);
            ContentValues values = new ContentValues();
            values.put(PoliRispostaDbContract.TaskColumns.SENT, tasks.get(selectedTask).isSent()?1:0);
            getActivity().getContentResolver().update(DatabaseContentProvider.TASK_URI, values, PoliRispostaDbContract.TaskColumns._ID + " = " + tasks.get(selectedTask).getTaskID(), null);
            Toast.makeText(getActivity(), "Task inviato con successo!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void reportError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void checked(String response) {
        //create JSON and send!
        if(response.equals("ok")){
            if(tasks.get(selectedTask).isSent()){
                Toast.makeText(getActivity(),"Task già inviato al server!", Toast.LENGTH_LONG).show();
            }
            else{
                mGoogleApiClient.connect();
            }
        }
        else if(response.equals("ko")){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Attenzione!");
            builder.setMessage("Il task contiene dei form non completati! Inviare lo stesso?");
            builder.setPositiveButton("Annulla", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.setNegativeButton("Invia", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    if(tasks.get(selectedTask).isSent()){
                        Toast.makeText(getActivity(),"Task già inviato al server!", Toast.LENGTH_LONG).show();
                    }
                    else{
                        mGoogleApiClient.connect();
                    }
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

        }
        else if(response.equals("strong ko")){
            Toast.makeText(getActivity(), "Alcuni form non sono finalizzati, impossibile inviare!", Toast.LENGTH_LONG).show();
        }
        else{
            //if empty!
            Toast.makeText(getActivity(), "Task vuoto, impossibile inviare!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onConnected(Bundle bundle) {
        SendTaskAsyncTask task = new SendTaskAsyncTask(tasks.get(selectedTask).getTaskID(), getActivity(), this, data.getAddress(), mGoogleApiClient, data.getSessionName(), data.getSessionID(), data.getCsrfTokenName(), data.getCsrfTokenID());
        task.execute();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(), RESOLVE_CONNECTION_REQUEST_CODE);
            } catch (IntentSender.SendIntentException e) {
                // Unable to resolve, message user appropriately
            }
        } else {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), getActivity(), 0).show();
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case RESOLVE_CONNECTION_REQUEST_CODE:
                if (resultCode == LaunchActivity.RESULT_OK) {
                    mGoogleApiClient.connect();
                }
                break;
        }
    }

    public class Mapping{
        private int id;
        private int taskID;
        private String name;
        private String description;
        private boolean userDefined;
        private boolean sent;

        public Mapping(int listID, int taskID, String name, String description, boolean userDefined, boolean sent) {
            this.id = listID;
            this.taskID = taskID;
            this.name = name;
            this.description = description;
            this.userDefined = userDefined;
            this.sent = sent;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getTaskID() {
            return taskID;
        }

        public void setTaskID(int taskID) {
            this.taskID = taskID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isUserDefined() {
            return userDefined;
        }

        public void setUserDefined(boolean userDefined) {
            this.userDefined = userDefined;
        }

        public boolean isSent() {
            return sent;
        }

        public void setSent(boolean sent) {
            this.sent = sent;
        }
    }



}
