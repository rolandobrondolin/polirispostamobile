package it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polirisposta.mobile.poliRISPOSTA.form.activity.asynctask.SetupDB;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.RequestType;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.FormInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;

/**
 * Created by rolando on 02/06/15.
 */
public class CheckTaskCompletionAsyncTask extends AsyncTask<Void, Void, String>{


    private int taskID;
    private Context context;
    private CheckResults check;

    private ProgressDialog dialog;

    public interface CheckResults{
        void checked(String response);
    }


    public CheckTaskCompletionAsyncTask(int taskID, Context context, CheckResults check) {
        this.taskID = taskID;
        this.context = context;
        this.check = check;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialog.show(context, "Invio task in corso...", "Attendere");
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        dialog.cancel();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try{
            dialog.dismiss();
            check.checked(s);
        }
        catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(Void... voids) {

        //pick up task id and for each form the compliation statistics
        //when all is answered or something not applicable proceed returning true
        //if something is missing return false, the activity handles it and asks for
        //confirmation

        //verify also that at least a form is present!

        ContentResolver resolver = context.getContentResolver();
        Cursor instanceCursor = resolver.query(DatabaseContentProvider.FORM_INSTANCE_URI, null, PoliRispostaDbContract.FormInstanceColumns.TASK_ID + " = " + taskID, null, null);
        if (instanceCursor != null) {
            if(instanceCursor.getCount()==0){
                instanceCursor.close();
                return "empty";
            }
            while (instanceCursor.moveToNext()) {
                //retrieve stat info
                int notFilledFields = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormInstanceColumns.NOT_FILLED));
                int mNotFilled = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormInstanceColumns.M_NOT_FILLED));
                int finalized = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormInstanceColumns.FINALIZED));

                if(finalized==0){
                    return "strong ko";
                }
                if(notFilledFields>0 || mNotFilled>0){
                    return "ko";
                }

            }
            instanceCursor.close();
        }
        return "ok";
    }


}
