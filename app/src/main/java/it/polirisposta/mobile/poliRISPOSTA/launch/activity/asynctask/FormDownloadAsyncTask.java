package it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import it.polirisposta.mobile.poliRISPOSTA.launch.activity.RequestType;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract.*;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DataType;

/**
 * Created by rolando on 28/05/15.
 */
public class FormDownloadAsyncTask extends AsyncTask<Void, Void, String> {

    private int formID;
    private Context context;
    private NetworkAsyncTask.PostResults post;
    private RequestType req = RequestType.GET_FORM;
    private String formURL;
    private String sectionURL;
    private String questionURL;
    private String fieldURL;
    private String choiceURL;
    private String resourceURL;

    private ProgressDialog dialog;

    String sessionID;
    String sessionName;
    String csrfToken;
    String csrfTokenName;


    public FormDownloadAsyncTask(int formID, Context context, NetworkAsyncTask.PostResults post, String address, String sessionID, String sessionName, String csrfToken, String csrfTokenName) {
        this.formID = formID;
        this.context = context;
        this.post = post;
        formURL = address + "/tabletinterface/forms/";
        sectionURL = address + "/tabletinterface/sections/";
        questionURL = address + "/tabletinterface/questions/";
        fieldURL = address + "/tabletinterface/fields/";
        choiceURL = address + "/tabletinterface/choices/";
        resourceURL = address + "/tabletinterface/resources/";

        this.sessionID = sessionID;
        this.sessionName = sessionName;
        this.csrfToken = csrfToken;
        this.csrfTokenName = csrfTokenName;

    }

    @Override
    protected String doInBackground(Void... voids) {
        int imagesNum = 0;
        Cursor formCursor = context.getContentResolver().query(DatabaseContentProvider.FORM_URI, null, FormColumns._ID + " = " + formID, null, null);
        if(formCursor!=null){
            if(formCursor.getCount()>0){
                //do nothing, form already there!
                formCursor.close();
                return "local";
                //context.getContentResolver().delete(DatabaseContentProvider.FORM_URI, PoliRispostaDbContract.FormColumns._ID + " = " + formID, null);
            }

        }
        try {
            //TODO:check user!


            JSONObject form = connect(formURL+formID);
            JSONArray array = form.getJSONArray("data");
            for(int i=0; i<array.length(); i++){
                JSONArray elem = array.getJSONArray(i);
                int id = elem.getInt(0);
                String name = elem.getString(1);
                String textname = elem.getString(2);
                String description = elem.getString(3);
                ContentValues formValues = new ContentValues();
                formValues.put(FormColumns._ID, id);
                formValues.put(FormColumns.NAME, name);
                formValues.put(FormColumns.DESCRIPTION, description);
                formValues.put(FormColumns.TEXT_NAME, textname);
                context.getContentResolver().insert(DatabaseContentProvider.FORM_URI, formValues);

                JSONObject sections = connect(sectionURL+formID);
                JSONArray sectionsArray = sections.getJSONArray("data");
                for (int j = 0; j < sectionsArray.length(); j++){
                    JSONArray sectionElem = sectionsArray.getJSONArray(j);
                    int sectionID = sectionElem.getInt(0);
                    String sectionName = sectionElem.getString(1);
                    String sectionTextName = sectionElem.getString(2);
                    String sectionDescription = sectionElem.getString(3);
                    int sectionRequired = sectionElem.getInt(4);
                    int sectionRepeat = sectionElem.getInt(5);
                    int formFK = sectionElem.getInt(6);
                    ContentValues sectionValues = new ContentValues();
                    sectionValues.put(SectionColumns._ID, sectionID);
                    sectionValues.put(SectionColumns.NAME, sectionName);
                    sectionValues.put(SectionColumns.TEXT_NAME, sectionTextName);
                    sectionValues.put(SectionColumns.DESCRIPTION, sectionDescription);
                    sectionValues.put(SectionColumns.REQUIRED, sectionRequired);
                    sectionValues.put(SectionColumns.REPEAT, sectionRepeat);
                    sectionValues.put(SectionColumns.FORM_ID, formFK);
                    context.getContentResolver().insert(DatabaseContentProvider.SECTION_URI, sectionValues);

                    JSONObject questions = connect(questionURL+sectionID);
                    JSONArray questionsArray = questions.getJSONArray("data");
                    for (int k =0; k< questionsArray.length(); k++){
                        JSONArray questionElem = questionsArray.getJSONArray(k);
                        int questionID = questionElem.getInt(0);
                        String questionName = questionElem.getString(1);
                        String questionTextName = questionElem.getString(2);
                        String questionDescription = questionElem.getString(3);
                        int questionRequired = questionElem.getInt(4);
                        int questionRepeat = questionElem.getInt(5);
                        int sectionFK = questionElem.getInt(6);
                        ContentValues questionValues = new ContentValues();
                        questionValues.put(QuestionColumns._ID, questionID);
                        questionValues.put(QuestionColumns.NAME, questionName);
                        questionValues.put(QuestionColumns.TEXT_NAME, questionTextName);
                        questionValues.put(QuestionColumns.DESCRIPTION, questionDescription);
                        questionValues.put(QuestionColumns.REQUIRED, questionRequired);
                        questionValues.put(QuestionColumns.REPEAT, questionRepeat);
                        questionValues.put(QuestionColumns.SECTION_ID, sectionFK);

                        context.getContentResolver().insert(DatabaseContentProvider.QUESTION_URI, questionValues);

                        //fields
                        JSONObject fields = connect(fieldURL+questionID);
                        JSONArray fieldsArray = fields.getJSONArray("data");
                        for (int l =0; l< fieldsArray.length(); l++){
                            JSONArray fieldElem = fieldsArray.getJSONArray(l);
                            int fieldID = fieldElem.getInt(0);
                            String fieldName = fieldElem.getString(1);
                            String fieldTextName = fieldElem.getString(2);
                            String fieldType = fieldElem.getString(3);
                            int fieldRequired = fieldElem.getInt(4);
                            int fieldRepeat = fieldElem.getInt(5);
                            int fieldCond = fieldElem.getInt(6);
                            //TODO: missing conditioning download!
                            int questionFK = fieldElem.getInt(10);
                            ContentValues fieldValues = new ContentValues();
                            fieldValues.put(FieldColumns._ID, fieldID);
                            fieldValues.put(FieldColumns.NAME, fieldName);
                            fieldValues.put(FieldColumns.TEXT_NAME, fieldTextName);
                            fieldValues.put(FieldColumns.TYPE, fieldType);
                            fieldValues.put(FieldColumns.REQUIRED, fieldRequired);
                            fieldValues.put(FieldColumns.REPEAT, fieldRepeat);
                            fieldValues.put(FieldColumns.COND, fieldCond);
                            fieldValues.putNull(FieldColumns.COND_STATEMENT);
                            fieldValues.putNull(FieldColumns.OP1);
                            fieldValues.putNull(FieldColumns.OP2);
                            fieldValues.put(FieldColumns.QUESTION_ID, questionFK);
                            context.getContentResolver().insert(DatabaseContentProvider.FIELD_URI, fieldValues);
                            //go for choices!
                            if(fieldType.equals(DataType.SELECT.toString()) || fieldType.equals(DataType.SELECT1.toString())){
                                JSONObject choices = connect(choiceURL+fieldID);
                                JSONArray choiceArray = choices.getJSONArray("data");
                                for (int m =0; m< choiceArray.length(); m++) {
                                    JSONArray choiceElem = choiceArray.getJSONArray(m);
                                    int choiceID = choiceElem.getInt(0);
                                    String choiceText = choiceElem.getString(1);
                                    int choiceOther = choiceElem.getInt(2);
                                    int fieldFK = choiceElem.getInt(3);
                                    ContentValues choiceValues = new ContentValues();
                                    choiceValues.put(ChoiceColumns._ID, choiceID);
                                    choiceValues.put(ChoiceColumns.TEXT, choiceText);
                                    choiceValues.put(ChoiceColumns.OTHER, choiceOther);
                                    choiceValues.put(ChoiceColumns.FIELD_ID, fieldFK);
                                    context.getContentResolver().insert(DatabaseContentProvider.CHOICE_URI, choiceValues);

                                }
                            }

                        }
                        try{
                            JSONObject resources = connect(resourceURL + questionID);
                            JSONArray resorucesArray = resources.getJSONArray("data");
                            for(int m = 0; m<resorucesArray.length(); m++){
                                JSONArray resourceElem = resorucesArray.getJSONArray(m);
                                int resourceID = resourceElem.getInt(0);
                                String resDescription = resourceElem.getString(1);
                                String resType = resourceElem.getString(2);
                                String resPath = resourceElem.getString(3);
                                int resQuestionFK = resourceElem.getInt(4);

                                int timeoutConnection = 10000;
                                HttpParams httpParameters = new BasicHttpParams();
                                HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
                                HttpClient httpClient = new DefaultHttpClient(httpParameters);
                                HttpGet httpGet = new HttpGet(resPath);
                                HttpResponse httpResponse = httpClient.execute(httpGet);
                                HttpEntity httpEntity = httpResponse.getEntity();

                                InputStream in = httpEntity.getContent();
                                Bitmap map = BitmapFactory.decodeStream(in);
                                //String absPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + formID + "_" + imagesNum + ".png";
                                String absPath = MediaStore.Images.Media.insertImage(context.getContentResolver(), map, name + "_" + imagesNum + ".png", resDescription);
                                resPath = absPath;
                                //save into DB
                                ContentValues resContentValues = new ContentValues();
                                resContentValues.put(ResourceColumns._ID, resourceID);
                                resContentValues.put(ResourceColumns.PATH, resPath);
                                resContentValues.put(ResourceColumns.DESCRIPTION, resDescription);
                                resContentValues.put(ResourceColumns.TYPE, resType);
                                resContentValues.put(ResourceColumns.QUESTION_ID, resQuestionFK);

                                context.getContentResolver().insert(DatabaseContentProvider.RESOURCE_URI, resContentValues);
                            }
                        }
                        catch (JSONException e){
                            e.printStackTrace();
                        }
                    }

                }


            }


        } catch (Exception e) {
            e.printStackTrace();
            context.getContentResolver().delete(DatabaseContentProvider.FORM_URI, PoliRispostaDbContract.FormColumns._ID + " = " + formID, null);
            return "ko";
        }

        return "ok";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try{
            dialog.dismiss();
            if(s.equals("ok")){
                post.updateView(req, true);
                return;
            }
            if(s.equals("local")){
                post.reportError("Form già presente sul tablet!");
                return;
            }
            post.reportError("Download non completato, riprovare!");
        }
        catch (IllegalArgumentException e){
            e.printStackTrace();
        }

        //call done interface
    }

    @Override
    protected void onCancelled() {
        context.getContentResolver().delete(DatabaseContentProvider.FORM_URI, PoliRispostaDbContract.FormColumns._ID + " = " + formID, null);
        dialog.cancel();
        post.updateView(req, false);
        super.onCancelled();
    }

    private JSONObject connect(String url) throws IOException, JSONException{
        url = url + "/";
        int timeoutConnection = 10000;
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);

        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie = new BasicClientCookie(sessionID, sessionName);
        BasicClientCookie csrfCookie = new BasicClientCookie(csrfToken, csrfTokenName);

        URL pippo = new URL(url);
        cookie.setDomain(pippo.getHost());
        csrfCookie.setDomain(pippo.getHost());

        cookieStore.addCookie(cookie);
        cookieStore.addCookie(csrfCookie);

        HttpGet httpGet = new HttpGet(url);

        httpClient.setCookieStore(cookieStore);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        String output = EntityUtils.toString(httpEntity);
        JSONObject obj =  new JSONObject(output);
        return obj;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialog.show(context, "Download form in corso...", "Attendere");
    }

}
