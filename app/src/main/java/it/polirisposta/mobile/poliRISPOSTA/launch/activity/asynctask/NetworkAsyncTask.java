package it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;

import it.polirisposta.mobile.poliRISPOSTA.launch.activity.RequestType;

/**
 * Created by rolando on 25/05/15.
 */
public class NetworkAsyncTask extends AsyncTask<Void, Void, String> {
    RequestType request;
    ArrayList<String> params;
    PostResults post;
    private ProgressDialog dialog;
    Context context;
    String address;
    String username;
    String password;
    int groupID;
    String sessionID;
    String sessionName;
    String csrfToken;
    String csrfTokenName;
    boolean silent;

    public interface PostResults{
        void updateView(RequestType request, JSONObject response);
        void updateView(RequestType request, boolean response);
        void reportError(String message);
    }


    public NetworkAsyncTask(Context context, RequestType request, ArrayList<String> params, PostResults post, String address, String username, int groupID, String password, String sessionID, String sessionName, String csrfToken, String csrfTokenName, boolean silent){
        this.request = request;
        this.params = params;
        this.post = post;
        this.address = address;
        this.groupID = groupID;
        this.password = password;
        this.context = context;
        this.silent = silent;
        this.username = username;
        this.sessionID = sessionID;
        this.sessionName = sessionName;
        this.csrfToken = csrfToken;
        this.csrfTokenName = csrfTokenName;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(!silent){
            dialog = ProgressDialog.show(context, "Connessione al server...", "Attendere");
        }
    }

    @Override
    protected String doInBackground(Void... voids) {
        String url = "";
        if(address==""){
            return "";
        }
        switch (request){
            case GET_FORMS:
                url = address + "/tabletinterface/forms";
                break;
            case GET_TASKS:
                url = address + "/tabletinterface/groups/" + groupID;
                break;
            case GET_FORM:
                url = address + "/tabletinterface/forms/" + params.get(1);
                break;
            case GET_AREA:
                url = address + "/tabletinterface/areas/" + groupID;
                break;
            case GET_POSITION:
                url = address + "/tabletinterface/positions";
                break;
            case POST_POSITION:





                break;
            case LOGIN:
                url = address + "/tabletinterface/login/";
                JSONObject json = new JSONObject();
                String loginOut = "";
                try {

                    json.put("username", username);
                    json.put("password", password);
                    int TIMEOUT_MILLISEC = 10000;  // = 10 seconds
                    HttpParams httpParams = new BasicHttpParams();
                    HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
                    HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
                    HttpClient client = new DefaultHttpClient(httpParams);

                    HttpPost request = new HttpPost(url);
                    request.setEntity(new ByteArrayEntity(json.toString().getBytes("UTF8")));
                    HttpResponse response = client.execute(request);
                    loginOut = EntityUtils.toString(response.getEntity());

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return loginOut;
            default:
                break;

        }

        url = url + "/";

        if(url.equals("")){
            return null;
        }
        String output = null;
        try {
            int timeoutConnection = 10000;
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);

            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            CookieStore cookieStore = new BasicCookieStore();
            BasicClientCookie cookie = new BasicClientCookie(sessionID, sessionName);
            BasicClientCookie csrfCookie = new BasicClientCookie(csrfToken, csrfTokenName);

            URL pippo = new URL(url);
            cookie.setDomain(pippo.getHost());
            csrfCookie.setDomain(pippo.getHost());

            cookieStore.addCookie(cookie);
            cookieStore.addCookie(csrfCookie);

            HttpGet httpGet = new HttpGet(url);

            httpClient.setCookieStore(cookieStore);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            output = EntityUtils.toString(httpEntity);

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }

    @Override
    protected void onPostExecute(String string) {
        JSONObject obj;
        try{
            if (!silent) {
                dialog.dismiss();
            }
            if (string != null) {
                try {
                    obj = new JSONObject(string);
                    post.updateView(request, obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (!silent) {
                    post.reportError("Errore di connessione, riprovare più tardi!");
                }
            }
        }
        catch (IllegalArgumentException e){
            e.printStackTrace();
        }
        super.onPostExecute(string);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if(!silent){
            dialog.cancel();
        }

    }
}
