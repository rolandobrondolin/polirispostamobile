package it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;


import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.MetadataChangeSet;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import it.polirisposta.mobile.poliRISPOSTA.form.activity.asynctask.SetupDB;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.RequestType;
import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.FormInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.Note;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract.*;
import it.polirisposta.mobile.poliRISPOSTA.model.QuestionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.Resource;
import it.polirisposta.mobile.poliRISPOSTA.model.SectionInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DateAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.FloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.GpsAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.IstatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.MeasureAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.NegativeIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveFloatAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.PositiveIntegerAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.Select1Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.SelectAnswer;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.StringAnswer;

/**
 * Created by rolando on 01/06/15.
 */
public class SendTaskAsyncTask extends AsyncTask<Void, Void, String> {

    private int taskID;
    private Context context;
    private NetworkAsyncTask.PostResults post;
    private RequestType req = RequestType.POST_INSTANCE;
    private String postURL;
    private GoogleApiClient apiClient;
    private ProgressDialog dialog;

    String sessionID;
    String sessionName;
    String csrfToken;
    String csrfTokenName;

    public SendTaskAsyncTask(int taskID, Context context, NetworkAsyncTask.PostResults post, String address, GoogleApiClient apiClient, String sessionID, String sessionName, String csrfToken, String csrfTokenName) {
        this.taskID = taskID;
        this.context = context;
        this.post = post;
        this.postURL = address + "/tabletinterface/upload/task/";
        this.apiClient = apiClient;

        this.sessionID = sessionID;
        this.sessionName = sessionName;
        this.csrfToken = csrfToken;
        this.csrfTokenName = csrfTokenName;
    }


    @Override
    protected String doInBackground(Void... voids) {
        boolean sPrefilled = false;
        boolean qPrefilled = false;
        boolean fPrefilled = false;
        //we are sure all is fine with the form (checked before)
        JSONObject sendObject = new JSONObject();
        try {
            ContentResolver resolver = context.getContentResolver();
            Cursor taskCursor = resolver.query(DatabaseContentProvider.TASK_URI, null, TaskColumns._ID + " = " + taskID, null, null);
            if (taskCursor != null) {
                taskCursor.moveToFirst();
                int id = taskCursor.getInt(taskCursor.getColumnIndexOrThrow(TaskColumns._ID));
                boolean userDefined = taskCursor.getInt(taskCursor.getColumnIndexOrThrow(TaskColumns.USER_DEFINED)) == 1;
                int groupID = taskCursor.getInt(taskCursor.getColumnIndexOrThrow(TaskColumns.GROUP_ID));
                if(!userDefined){
                    sendObject.put("id", id);
                }
                else{
                    String taskName = taskCursor.getString(taskCursor.getColumnIndexOrThrow(TaskColumns.NAME));
                    String taskDescription = taskCursor.getString(taskCursor.getColumnIndexOrThrow(TaskColumns.DESCRIPTION));
                    sendObject.put("group_id", groupID);
                    sendObject.put("name", taskName);
                    sendObject.put("description", taskDescription);
                    sendObject.put("prefilled", 0);

                    Cursor moduleCursor = resolver.query(DatabaseContentProvider.TASK_MODULE_URI, null, TaskModuleColumns.TASK_ID + " = " + taskID, null, null);
                    if(moduleCursor!=null){
                        JSONArray moduleArray = new JSONArray();
                        while (moduleCursor.moveToNext()){
                            //form id, num instances
                            int formIDModule = moduleCursor.getInt(moduleCursor.getColumnIndexOrThrow(TaskModuleColumns.FORM_ID));
                            int nInstances = moduleCursor.getInt(moduleCursor.getColumnIndexOrThrow(TaskModuleColumns.N_INSTANCES));
                            JSONObject moduleObject = new JSONObject();
                            moduleObject.put("form", formIDModule);
                            moduleObject.put("n_instances", nInstances);
                            moduleArray.put(moduleObject);
                        }
                        moduleCursor.close();
                        sendObject.put("task_modules", moduleArray);
                    }
                }
                sendObject.put("user_defined", userDefined);

                taskCursor.close();

                JSONArray formArray = new JSONArray();

                Cursor instanceCursor = resolver.query(DatabaseContentProvider.FORM_INSTANCE_URI, null, FormInstanceColumns.TASK_ID + " = " + taskID, null, null);
                if (instanceCursor != null) {
                    while (instanceCursor.moveToNext()) {
                        JSONObject formObject = new JSONObject();
                        int instanceID = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns._ID));
                        SetupDB retrieveInstance = new SetupDB(context, null);
                        FormInstance instance = retrieveInstance.editOpen(instanceID);


                        Long date = Long.parseLong(instance.getTimestamp());
                        Date date1 = new Date(date);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ITALY);
                        formObject.put("timestamp", sdf.format(date1));
                        formObject.put("device_id", instance.getDeviceId());
                        formObject.put("total_fields", instance.getTotalFields());
                        formObject.put("total_mandatory_fields", instance.getmTotalFields());
                        formObject.put("filled_fields", instance.getFilled());
                        formObject.put("not_filled_fields", instance.getNotFilled());
                        formObject.put("not_applicable_fields", instance.getNotApplicable());
                        formObject.put("mandatory_filled_fields", instance.getmFilled());
                        formObject.put("mandatory_not_filled_fields", instance.getmNotFilled());
                        formObject.put("mandatory_not_applicable_fields", instance.getmNotApplicable());
                        formObject.put("form_id", instance.getForm().getId());
                        formObject.put("form_name", instance.getForm().getName());
                        formObject.put("finalized", instance.isFinalized());
                        JSONArray sectionArray = new JSONArray();

                        for(SectionInstance s : instance.getSections()){
                            JSONObject sectionObject = new JSONObject();
                            //go through sections!
                            sectionObject.put("index", s.getIndex());
                            sectionObject.put("section_id", s.getSection().getId());
                            sectionObject.put("section_name", s.getSection().getName());
                            sectionObject.put("s_index",s.getIndex());

                            JSONArray questionArray = new JSONArray();
                            for(QuestionInstance q: s.getQuestionInstances()){
                                JSONObject questionObject = new JSONObject();

                                questionObject.put("index", q.getIndex());
                                questionObject.put("question_id", q.getQuestion().getId());
                                questionObject.put("question_name", q.getQuestion().getName());
                                questionObject.put("q_index", q.getIndex());
                                //resources (with link to drive instance)
                                //TODO: save to drive!
                                JSONArray resArray = new JSONArray();
                                for(Resource r: q.getPictures()){
                                    JSONObject resObj = new JSONObject();
                                    resObj.put("type", r.getType());
                                    resObj.put("path", instance.getDeviceId() + r.getUri().getLastPathSegment());
                                    resArray.put(resObj);
                                    try {
                                        DriveApi.DriveContentsResult contentsResult = Drive.DriveApi.newDriveContents(apiClient).await();

                                        if (!contentsResult.getStatus().isSuccess()) {
                                            break;
                                        }
                                        // Read the contents and open its output stream for writing, then
                                        // write a short message.
                                        DriveContents originalContents = contentsResult.getDriveContents();
                                        OutputStream os = originalContents.getOutputStream();

                                        InputStream dbInputStream = context.getContentResolver().openInputStream(r.getUri());

                                        byte[] buffer = new byte[1024];
                                        int length;
                                        int counter = 0;
                                        while((length = dbInputStream.read(buffer)) > 0) {
                                            ++counter;
                                            os.write(buffer, 0, length);
                                        }

                                        dbInputStream.close();
                                        os.flush();
                                        os.close();

                                        MetadataChangeSet originalMetadata = new MetadataChangeSet.Builder().setTitle(instance.getDeviceId() + r.getUri().getLastPathSegment()).setMimeType("image/jpeg").build();
                                        // Create the file in the root folder, again calling await() to
                                        // block until the request finishes.
                                        DriveFolder rootFolder = Drive.DriveApi.getRootFolder(apiClient);
                                        DriveFolder.DriveFileResult fileResult = rootFolder.createFile(apiClient, originalMetadata, originalContents).await();

                                        if (!fileResult.getStatus().isSuccess()) {
                                            break;
                                        }
                                        // Finally, fetch the metadata for the newly created file, again
                                        // calling await to block until the request finishes.
                                        DriveResource.MetadataResult metadataResult = fileResult.getDriveFile().getMetadata(apiClient).await();

                                        if (!metadataResult.getStatus().isSuccess()) {
                                            break;
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                questionObject.put("resource_instance", resArray);
                                //notes
                                JSONArray notesArray = new JSONArray();
                                for(Note n : q.getNotes()){
                                    notesArray.put(n.getNote());
                                }
                                questionObject.put("note", notesArray);
                                //answers
                                JSONArray answersArray = new JSONArray();
                                for(Answer a: q.getAnswers()){

                                    JSONObject answerObject = new JSONObject();
                                    answerObject.put("type", a.getType().toString());
                                    answerObject.put("field_id", a.getField().getId());
                                    answerObject.put("field_name", a.getField().getName());
                                    answerObject.put("prefilled", a.isPrefilled());
                                    answerObject.put("filled", a.isFilled());
                                    answerObject.put("applicable", a.isApplicable());
                                    //check prefilled, if yes for one, then the question was sent to us and exists with that id on the server
                                    //and the same also for the section
                                    if(a.isPrefilled()){
                                        sPrefilled = true;
                                        qPrefilled = true;
                                        fPrefilled = true;
                                        answerObject.put("id", a.getId());
                                    }

                                    switch (a.getType()){
                                        case STRING:
                                            StringAnswer ansStr = (StringAnswer) a;
                                            answerObject.put("answer_string",ansStr.getAnswer());
                                            answerObject.put("answer_integer", null);
                                            answerObject.put("answer_float", null);
                                            break;
                                        case INTEGER:
                                            IntegerAnswer ansInt = (IntegerAnswer) a;
                                            answerObject.put("answer_string",null);
                                            answerObject.put("answer_integer",ansInt.getAnswer());
                                            answerObject.put("answer_float",null);
                                            break;
                                        case POSITIVE_INTEGER:
                                            PositiveIntegerAnswer ansPosInt = (PositiveIntegerAnswer) a;
                                            answerObject.put("answer_string",null);
                                            answerObject.put("answer_integer",ansPosInt.getAnswer());
                                            answerObject.put("answer_float",null);
                                            break;
                                        case NEGATIVE_INTEGER:
                                            NegativeIntegerAnswer ansNegInt = (NegativeIntegerAnswer) a;
                                            answerObject.put("answer_string",null);
                                            answerObject.put("answer_integer",ansNegInt.getAnswer());
                                            answerObject.put("answer_float",null);
                                            break;
                                        case FLOAT:
                                            FloatAnswer ansFloat = (FloatAnswer) a;
                                            answerObject.put("answer_string",null);
                                            answerObject.put("answer_integer",null);
                                            answerObject.put("answer_float",ansFloat.getAnswer());
                                            break;
                                        case POSITIVE_FLOAT:
                                            PositiveFloatAnswer ansPosFloat = (PositiveFloatAnswer) a;
                                            answerObject.put("answer_string",null);
                                            answerObject.put("answer_integer",null);
                                            answerObject.put("answer_float",ansPosFloat.getAnswer());
                                            break;
                                        case NEGATIVE_FLOAT:
                                            NegativeFloatAnswer ansNegFloat = (NegativeFloatAnswer) a;
                                            answerObject.put("answer_string",null);
                                            answerObject.put("answer_integer",null);
                                            answerObject.put("answer_float",ansNegFloat.getAnswer());
                                            break;
                                        case MEASURE:
                                            MeasureAnswer measureAns = (MeasureAnswer) a;
                                            answerObject.put("answer_string",measureAns.getUm());
                                            answerObject.put("answer_integer",null);
                                            answerObject.put("answer_float",measureAns.getAnswer());
                                            break;
                                        case ISTAT_CODE:
                                            IstatAnswer ansIstat = (IstatAnswer) a;
                                            answerObject.put("answer_string",ansIstat.getAnswer());
                                            answerObject.put("answer_integer",null);
                                            answerObject.put("answer_float",null);
                                            break;
                                        case GPS:
                                            GpsAnswer ansGps = (GpsAnswer) a;
                                            answerObject.put("answer_string",ansGps.gpsConcat());
                                            answerObject.put("answer_integer",null);
                                            answerObject.put("answer_float",null);
                                            break;
                                        case DATE:
                                        case DATE_TIME:
                                            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ITALY);
                                            DateAnswer ansDate = (DateAnswer) a;
                                            answerObject.put("answer_string", sdf2.format(ansDate.getAnswer()));
                                            answerObject.put("answer_integer",null);
                                            answerObject.put("answer_float",null);
                                            break;
                                        case SELECT:
                                            SelectAnswer ansSel = (SelectAnswer) a;
                                            answerObject.put("answer_string",ansSel.getAnswerDB());
                                            answerObject.put("answer_integer",null);
                                            answerObject.put("answer_float",null);
                                            break;
                                        case SELECT1:
                                            Select1Answer ansSel1 = (Select1Answer) a;
                                            answerObject.put("answer_string",ansSel1.getAnswer());
                                            answerObject.put("answer_integer",null);
                                            answerObject.put("answer_float",null);
                                            break;
                                        case SKETCH:
                                            break;
                                    }

                                    answersArray.put(answerObject);
                                }
                                //check prefilled and add id in case
                                if(qPrefilled){
                                    questionObject.put("id", q.getId());
                                    qPrefilled = false;
                                }
                                questionObject.put("answer", answersArray);
                                questionArray.put(questionObject);
                            }
                            //check prefilled and add id in case
                            if(sPrefilled){
                                sectionObject.put("id", s.getId());
                                sPrefilled = false;
                            }
                            sectionObject.put("question_instance", questionArray);
                            sectionArray.put(sectionObject);
                        }
                        if(fPrefilled){
                            formObject.put("id", instanceID);
                            fPrefilled = false;
                        }
                        formObject.put("section_instance", sectionArray);
                        formArray.put(formObject);
                    }
                    instanceCursor.close();
                    sendObject.put("instances", formArray);
                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
            apiClient.disconnect();
            return "ko";
        } catch (NullPointerException e){
            e.printStackTrace();
            return "ko";
        }


        String taskOut = "";
        //send all!
        try {
            JSONObject obj = sendObject;
            //obj.put("data", sendObject);

            String imageFileName = "pippo.json";
            File image = File.createTempFile(imageFileName, null, context.getCacheDir());
            FileOutputStream outputStream;

            try {
                outputStream = new FileOutputStream(image.getPath());
                Log.e("PATH JSON", image.getPath());
                OutputStreamWriter myOutWriter = new OutputStreamWriter(outputStream);
                myOutWriter.append(obj.toString());
                myOutWriter.close();
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            int TIMEOUT_MILLISEC = 10000;  // = 10 seconds
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            DefaultHttpClient client = new DefaultHttpClient(httpParams);

            CookieStore cookieStore = new BasicCookieStore();
            BasicClientCookie cookie = new BasicClientCookie(sessionID, sessionName);
            BasicClientCookie csrfCookie = new BasicClientCookie(csrfToken, csrfTokenName);
            URL pippo = new URL(postURL);
            cookie.setDomain(pippo.getHost());
            csrfCookie.setDomain(pippo.getHost());
            cookieStore.addCookie(cookie);
            cookieStore.addCookie(csrfCookie);
            client.setCookieStore(cookieStore);

            HttpPost request = new HttpPost(postURL);
            //request.addHeader("Cookie",sessionName + " = " + sessionID + "; " + csrfTokenName + " = " + csrfToken + ";");
            request.setHeader("Referer", pippo.getHost());
            request.setHeader("X-CSRFToken", csrfToken);

            request.setEntity(new ByteArrayEntity(obj.toString().getBytes("UTF8")));
            HttpResponse response = client.execute(request);

            taskOut = EntityUtils.toString(response.getEntity());
            JSONObject result = new JSONObject(taskOut);
            String resContent = result.getString("success");

            if(resContent.equals("true")){
                apiClient.disconnect();
                return "ok";
            }
        } catch (IOException e) {
            e.printStackTrace();
            apiClient.disconnect();
            return "ko";
        } catch (JSONException e) {
            e.printStackTrace();
            apiClient.disconnect();
            return "ko";
        }

        apiClient.disconnect();
        return "ko";
    }



    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try{
            dialog.dismiss();
            if (s.equals("ok")) {
                post.updateView(req, true);
                return;
            }
            //if(s.equals("ko")){
            post.reportError("task non inviato, riprovare!");
            //}
        /*else{
            //Log.e("JSON", s);

            int maxLogSize = 1000;
            for(int i = 0; i <= s.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i+1) * maxLogSize;
                end = end > s.length() ? s.length() : end;
                Log.e("LINE" + i, s.substring(start, end));
            }

        }*/
        }
        catch (IllegalArgumentException e){
            e.printStackTrace();
        }

        //call done interface
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        try {
            dialog = ProgressDialog.show(context, "Invio task in corso...", "Attendere");
        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        dialog.cancel();
        post.updateView(req, false);
    }

}
