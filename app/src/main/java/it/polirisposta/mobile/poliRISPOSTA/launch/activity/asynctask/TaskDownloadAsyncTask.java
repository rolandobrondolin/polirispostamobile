package it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import it.polirisposta.mobile.poliRISPOSTA.launch.activity.RequestType;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract.*;
import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DataType;
import it.polirisposta.mobile.poliRISPOSTA.util.Util;

/**
 * Created by rolando on 30/05/15.
 */
public class TaskDownloadAsyncTask extends AsyncTask<Void, Void, String> {

    private int taskID;
    private Context context;
    private NetworkAsyncTask.PostResults post;
    private RequestType req = RequestType.GET_FORM;
    private String taskURL;
    private String moduleURL;
    private String listInstancesURL;
    private String instanceURL;
    private String sInstURL;
    private String qInstURL;
    private String ansURL;

    private int totalFields = 0;
    private int mtotalFields = 0;
    private int filledFields = 0;
    private int notFilledFields = 0;
    private int notApplicableFields = 0;
    private int mFilled = 0;
    private int mNotFilled = 0;
    private int mNotApplicable = 0;

    private ProgressDialog dialog;

    String sessionID;
    String sessionName;
    String csrfToken;
    String csrfTokenName;

    public TaskDownloadAsyncTask(int taskID, Context context, NetworkAsyncTask.PostResults post, String address, String sessionID, String sessionName, String csrfToken, String csrfTokenName) {
        this.taskID = taskID;
        this.context = context;
        this.post = post;
        this.taskURL = address + "/tabletinterface/tasks/";
        this.moduleURL = address + "/tabletinterface/taskmodules/";
        this.listInstancesURL = address + "/tabletinterface/instances/forms/";
        this.instanceURL = address + "/tabletinterface/instances/form/";
        this.sInstURL = address + "/tabletinterface/instances/section/";
        this.qInstURL = address + "/tabletinterface/instances/question/";
        this.ansURL = address + "/tabletinterface/instances/answer/";

        this.sessionID = sessionID;
        this.sessionName = sessionName;
        this.csrfToken = csrfToken;
        this.csrfTokenName = csrfTokenName;
    }

    @Override
    protected String doInBackground(Void... voids) {
        Cursor taskCursor = context.getContentResolver().query(DatabaseContentProvider.TASK_URI, null, TaskColumns._ID + " = " + taskID, null, null);
        if(taskCursor!=null){
            if(taskCursor.getCount()!= 0){
                taskCursor.close();
                return "local";
            }
        }
        boolean prefilled = false;
        boolean formCreated = false;
        try{
            //do things!
            JSONObject taskObj = connect(taskURL + taskID);
            JSONArray taskArray = taskObj.getJSONArray("data");
            for(int i=0; i<taskArray.length(); i++){
                JSONArray taskElem = taskArray.getJSONArray(i);
                int taskID = taskElem.getInt(0);
                String taskName = taskElem.getString(1);
                Date date = Date.valueOf(taskElem.getString(2));
                String taskDescription = taskElem.getString(3);
                int groupID = taskElem.getInt(4);
                prefilled = taskElem.getBoolean(5);

                ContentValues taskValues = new ContentValues();
                taskValues.put(TaskColumns._ID, taskID);
                taskValues.put(TaskColumns.NAME, taskName);
                taskValues.put(TaskColumns.DESCRIPTION, taskDescription);
                taskValues.put(TaskColumns.GROUP_ID, groupID);
                taskValues.put(TaskColumns.GENERATED, prefilled?1:0);
                taskValues.put(TaskColumns.SENT, 0);
                taskValues.put(TaskColumns.USER_DEFINED, 0);
                context.getContentResolver().insert(DatabaseContentProvider.TASK_URI, taskValues);

                JSONObject moduleObj = connect(moduleURL + taskID);
                JSONArray moduleArray = moduleObj.getJSONArray("data");
                for(int j=0; j<moduleArray.length(); j++){
                    JSONArray moduleElem = moduleArray.getJSONArray(j);
                    int moduleID = moduleElem.getInt(0);
                    int taskFK = moduleElem.getInt(1);
                    int formFK = moduleElem.getInt(2);
                    int nInstances = moduleElem.getInt(3);
                    Cursor formCursor = context.getContentResolver().query(DatabaseContentProvider.FORM_URI, null, FormColumns._ID + " = " + formFK, null, null);
                    if(formCursor!=null){
                        if(formCursor.getCount()== 0){
                            formCursor.close();
                            context.getContentResolver().delete(DatabaseContentProvider.TASK_URI, TaskColumns._ID + " = " + taskID, null);
                            return "missing";
                        }
                    }
                    ContentValues moduleValues = new ContentValues();
                    moduleValues.put(TaskModuleColumns._ID, moduleID);
                    moduleValues.put(TaskModuleColumns.FORM_ID, formFK);
                    moduleValues.put(TaskModuleColumns.TASK_ID, taskFK);
                    moduleValues.put(TaskModuleColumns.N_INSTANCES, nInstances);

                    context.getContentResolver().insert(DatabaseContentProvider.TASK_MODULE_URI, moduleValues);
                }

                if(prefilled){
                    //download prefilled! and add all the other stuffs...
                    //traverse each section and each question... If already inside continue searching in the fieds
                    //if there are some matching in the answers ok (look at the matching of the fields), otherwise create
                    //the new answer

                    //download phase
                    JSONObject forms = connect(this.listInstancesURL + taskID);
                    JSONArray instances = forms.getJSONArray("data");
                    for (int a = 0; a<instances.length(); a++) {
                        JSONArray arr = instances.getJSONArray(a);

                        JSONObject jsonInstance = connect(this.instanceURL + arr.getString(0));
                        JSONArray jInstArray = jsonInstance.getJSONArray("data");
                        for (int j = 0; j < jInstArray.length(); j++) {
                            JSONArray elem = jInstArray.getJSONArray(j);
                            int instanceID = elem.getInt(0);
                            int actualTaskID = elem.getInt(1);
                            int formID = elem.getInt(2);
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ITALY);
                            long timestamp = 0;
                            try{
                                timestamp = sdf.parse(elem.getString(3)).getTime();
                            }
                            catch (ParseException e){
                                sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ITALY);
                                timestamp = sdf.parse(elem.getString(3)).getTime();
                            }

                            String deviceID = elem.getString(4);


                            ContentValues instanceValues = new ContentValues();
                            instanceValues.put(FormInstanceColumns._ID, instanceID);
                            instanceValues.put(FormInstanceColumns.TASK_ID, actualTaskID);
                            instanceValues.put(FormInstanceColumns.FORM_ID, formID);
                            instanceValues.put(FormInstanceColumns.TIMESTAMP, Long.toString(timestamp));
                            instanceValues.put(FormInstanceColumns.DEVICE_ID, deviceID);
                            instanceValues.put(FormInstanceColumns.FINALIZED, false);

                            instanceValues.put(FormInstanceColumns.TOTAL, 0);
                            instanceValues.put(FormInstanceColumns.M_TOTAL, 0);
                            instanceValues.put(FormInstanceColumns.FILLED, 0);
                            instanceValues.put(FormInstanceColumns.NOT_FILLED, 0);
                            instanceValues.put(FormInstanceColumns.NOT_APPLICABLE, 0);
                            instanceValues.put(FormInstanceColumns.M_FILLED, 0);
                            instanceValues.put(FormInstanceColumns.M_NOT_FILLED, 0);
                            instanceValues.put(FormInstanceColumns.M_NOT_APPLICABLE, 0);
                            context.getContentResolver().insert(DatabaseContentProvider.FORM_INSTANCE_URI, instanceValues);
                            formCreated = true;
                            JSONObject jsonSection = connect(this.sInstURL + instanceID);
                            JSONArray jSectArray = jsonSection.getJSONArray("data");
                            for (int k = 0; k < jSectArray.length(); k++) {
                                JSONArray sectArray = jSectArray.getJSONArray(k);
                                int sectInstID = sectArray.getInt(0);
                                int sIndex = sectArray.getInt(1);
                                int sectID = sectArray.getInt(3);

                                ContentValues sValues = new ContentValues();
                                sValues.put(SectionInstanceColumns._ID, sectInstID);
                                sValues.put(SectionInstanceColumns.INDEX, sIndex);
                                sValues.put(SectionInstanceColumns.FORM_INSTANCE_ID, instanceID);
                                sValues.put(SectionInstanceColumns.SECTION_ID, sectID);

                                context.getContentResolver().insert(DatabaseContentProvider.SECTION_INSTANCE_URI, sValues);

                                JSONObject jsonQuestion = connect(this.qInstURL + sectInstID);
                                JSONArray jQuestArray = jsonQuestion.getJSONArray("data");
                                for (int l = 0; l < jQuestArray.length(); l++) {
                                    JSONArray questArray = jQuestArray.getJSONArray(l);

                                    int questInstID = questArray.getInt(0);
                                    int qIndex = questArray.getInt(1);
                                    int questID = questArray.getInt(3);

                                    ContentValues qValues = new ContentValues();
                                    qValues.put(QuestionInstanceColumns._ID, questInstID);
                                    qValues.put(QuestionInstanceColumns.INDEX, qIndex);
                                    qValues.put(QuestionInstanceColumns.SECTION_INSTANCE_ID, sectInstID);
                                    qValues.put(QuestionInstanceColumns.QUESTION_ID, questID);

                                    context.getContentResolver().insert(DatabaseContentProvider.QUESTION_INSTANCE_URI, qValues);


                                    JSONObject jsonAnswer = connect(this.ansURL + questInstID);
                                    JSONArray jAnsArray = jsonAnswer.getJSONArray("data");
                                    for (int m = 0; m < jAnsArray.length(); m++) {
                                        JSONArray ansArray = jAnsArray.getJSONArray(m);

                                        int ansID = ansArray.getInt(0);
                                        int ansPref = ansArray.getBoolean(1)?1:0;
                                        int applicable = ansArray.getBoolean(2)?1:0;
                                        String type = ansArray.getString(3);
                                        int filled = ansArray.getBoolean(4)?1:0;
                                        int fieldID = ansArray.getInt(8);

                                        ContentValues ansVal = new ContentValues();
                                        ansVal.put(AnswerColumns._ID, ansID);
                                        ansVal.put(AnswerColumns.PREFILLED, ansPref);
                                        ansVal.put(AnswerColumns.APPLICABLE, applicable);
                                        ansVal.put(AnswerColumns.TYPE, type);
                                        ansVal.put(AnswerColumns.FIELD_ID, fieldID);
                                        ansVal.put(AnswerColumns.QUESTION_INSTANCE_ID, questInstID);
                                        ansVal.put(AnswerColumns.FILLED, filled);


                                        if (ansArray.isNull(5)) {
                                            ansVal.putNull(AnswerColumns.ANSWER_STRING);
                                        } else {
                                            if (type.equals(DataType.DATE.toString()) || type.equals(DataType.DATE_TIME.toString())) {
                                                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ITALY);
                                                ansVal.put(AnswerColumns.ANSWER_STRING, Long.toString(sdf2.parse(ansArray.getString(5)).getTime()));
                                            } else {
                                                ansVal.put(AnswerColumns.ANSWER_STRING, ansArray.getString(5));
                                            }
                                        }

                                        if (ansArray.isNull(6)) {
                                            ansVal.putNull(AnswerColumns.ANSWER_INTEGER);
                                        } else {
                                            ansVal.put(AnswerColumns.ANSWER_INTEGER, ansArray.getInt(6));
                                        }
                                        if (ansArray.isNull(7)) {
                                            ansVal.putNull(AnswerColumns.ANSWER_FLOAT);
                                        } else {
                                            ansVal.put(AnswerColumns.ANSWER_FLOAT, new Float(ansArray.getDouble(7)));
                                        }

                                        context.getContentResolver().insert(DatabaseContentProvider.ANSWER_URI, ansVal);

                                    }

                                }

                            }
                            //now generate the remainings! =)
                            setUpFormInstance(instanceID);
                        }
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            context.getContentResolver().delete(DatabaseContentProvider.TASK_URI, TaskColumns._ID + " = " + taskID, null);
            if(prefilled && formCreated){
                context.getContentResolver().delete(DatabaseContentProvider.FORM_INSTANCE_URI, FormInstanceColumns.TASK_ID + " = " + taskID, null);
            }
            return "ko";
        }
        //DB save


        return "ok";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialog.show(context, "Download task in corso...", "Attendere");

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try{
            dialog.dismiss();
            if (s.equals("ok")) {
                post.updateView(req, true);
                return;
            }
            if (s.equals("local")) {
                post.reportError("Task già presente sul tablet!");
                return;
            }
            if (s.equals("missing")) {
                post.reportError("Scaricare i form necessari per il completamento del task!");
            }
            post.reportError("Download non completato, riprovare!");
        }
        catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        dialog.cancel();
        post.updateView(req, false);

    }

    private JSONObject connect(String url) throws IOException, JSONException {
        url = url + "/";
        int timeoutConnection = 10000;
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);

        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie = new BasicClientCookie(sessionID, sessionName);
        BasicClientCookie csrfCookie = new BasicClientCookie(csrfToken, csrfTokenName);

        URL pippo = new URL(url);
        cookie.setDomain(pippo.getHost());
        csrfCookie.setDomain(pippo.getHost());

        cookieStore.addCookie(cookie);
        cookieStore.addCookie(csrfCookie);

        HttpGet httpGet = new HttpGet(url);

        httpClient.setCookieStore(cookieStore);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        String output = EntityUtils.toString(httpEntity);
        JSONObject obj =  new JSONObject(output);
        return obj;
    }


    private void setUpFormInstance(int formInstanceID) {
        totalFields = 0;
        mtotalFields = 0;
        filledFields = 0;
        notFilledFields = 0;
        notApplicableFields = 0;
        mFilled = 0;
        mNotFilled = 0;
        mNotApplicable = 0;
        int formID;
        int sectionID;
        Uri formInstanceUri = DatabaseContentProvider.FORM_INSTANCE_URI;
        Uri sectionInstanceUri = DatabaseContentProvider.SECTION_INSTANCE_URI;
        Uri sectionUri = DatabaseContentProvider.SECTION_URI;


        ContentResolver resolver = context.getContentResolver();
        Cursor formInstCursor = resolver.query(formInstanceUri, null, FormInstanceColumns._ID + " = " + formInstanceID, null, null);
        if (formInstCursor != null) {
            formInstCursor.moveToFirst();
            formID = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(FormInstanceColumns.FORM_ID));
            //retrieve stat info

            formInstCursor.close();

            //search for section
            Cursor sCursor = resolver.query(sectionUri, null, SectionColumns.FORM_ID + "= " + formID, null, null);
            if (sCursor != null) {
                while (sCursor.moveToNext()) {
                    sectionID = sCursor.getInt(sCursor.getColumnIndexOrThrow(SectionColumns._ID));
                    int sectInstId;
                    //search if instance is present, if no create one!
                    Cursor sInstCursor = resolver.query(sectionInstanceUri, null, SectionInstanceColumns.SECTION_ID + " = " + sectionID + " and " + SectionInstanceColumns.FORM_INSTANCE_ID + " = " + formInstanceID, null, null);
                    if (sInstCursor != null) {
                        if (sInstCursor.getCount() == 0) {
                            ContentValues sectInstValues = new ContentValues();
                            sectInstValues.put(SectionInstanceColumns.INDEX, 1);
                            sectInstValues.put(SectionInstanceColumns.FORM_INSTANCE_ID, formInstanceID);
                            sectInstValues.put(SectionInstanceColumns.SECTION_ID, sectionID);
                            Uri sectInstUri = resolver.insert(sectionInstanceUri, sectInstValues);
                            //retrieve id of the section instance created
                            sectInstId = Integer.parseInt(sectInstUri.getLastPathSegment());
                            //set correct id
                            if (sectInstId < Util.BASE_ID) {
                                ContentValues val = new ContentValues();
                                val.put(SectionInstanceColumns._ID, Util.BASE_ID);
                                resolver.update(DatabaseContentProvider.SECTION_INSTANCE_URI, val, SectionInstanceColumns._ID + " = " + sectInstId, null);
                                sectInstId = Util.BASE_ID;
                            }
                            //call for questions
                            saveQuestions(sectInstId, sectionID);
                        }
                        else {
                            //retrieve the ids and call for question for each one
                            while(sInstCursor.moveToNext()) {
                                sectInstId = sInstCursor.getInt(sInstCursor.getColumnIndexOrThrow(SectionInstanceColumns._ID));
                                saveQuestions(sectInstId, sectionID);
                            }
                        }
                        sInstCursor.close();
                    }

                }
                sCursor.close();
            }
            //conclude the correct init of the stats
            ContentValues formInstValues = new ContentValues();

            formInstValues.put(FormInstanceColumns.TOTAL, totalFields);
            formInstValues.put(FormInstanceColumns.FILLED, filledFields);
            formInstValues.put(FormInstanceColumns.NOT_FILLED, notFilledFields);
            formInstValues.put(FormInstanceColumns.NOT_APPLICABLE, notApplicableFields);

            formInstValues.put(FormInstanceColumns.M_TOTAL, mtotalFields);
            formInstValues.put(FormInstanceColumns.M_FILLED, mFilled);
            formInstValues.put(FormInstanceColumns.M_NOT_FILLED, mNotFilled);
            formInstValues.put(FormInstanceColumns.M_NOT_APPLICABLE, mNotApplicable);

            context.getContentResolver().update(DatabaseContentProvider.FORM_INSTANCE_URI, formInstValues, FormInstanceColumns._ID + " = " + formInstanceID, null);
        }
    }

    private void saveQuestions(int sectionInstanceID, int sectionID) {
        Uri questionInstanceUri = DatabaseContentProvider.QUESTION_INSTANCE_URI;
        Uri questionUri = DatabaseContentProvider.QUESTION_URI;
        ContentResolver resolver = context.getContentResolver();
        int questionID;

        Cursor questionCursor = resolver.query(questionUri, null, QuestionColumns.SECTION_ID + "= " + sectionID, null, QuestionColumns._ID);
        if (questionCursor != null) {
            while (questionCursor.moveToNext()) {
                questionID = questionCursor.getInt(questionCursor.getColumnIndexOrThrow(QuestionColumns._ID));
                //question instance setup
                //question instance init in database

                Cursor qInstCursor = resolver.query(questionInstanceUri, null, QuestionInstanceColumns.QUESTION_ID + " = " + questionID + " and " + QuestionInstanceColumns.SECTION_INSTANCE_ID + " = " + sectionInstanceID, null, null);
                if (qInstCursor != null) {
                    if (qInstCursor.getCount() == 0) {
                        ContentValues questionInstanceValues = new ContentValues();
                        //questionInstanceValues.put(QuestionInstanceColumns._ID, qInstance.getId());
                        questionInstanceValues.put(QuestionInstanceColumns.INDEX, 1);
                        questionInstanceValues.put(QuestionInstanceColumns.SECTION_INSTANCE_ID, sectionInstanceID);
                        questionInstanceValues.put(QuestionInstanceColumns.QUESTION_ID, questionID);
                        Uri qInstIdUri = resolver.insert(questionInstanceUri, questionInstanceValues);
                        //set returned id for the q instance
                        int qId = Integer.parseInt(qInstIdUri.getLastPathSegment());

                        //set correct id
                        if (qId < Util.BASE_ID) {
                            ContentValues val = new ContentValues();
                            val.put(QuestionInstanceColumns._ID, Util.BASE_ID);
                            resolver.update(DatabaseContentProvider.QUESTION_INSTANCE_URI, val, QuestionInstanceColumns._ID + " = " + qId, null);
                            qId = Util.BASE_ID;
                        }

                        generateAnswers(qId, questionID);

                    } else {
                        //retrieve all the ids and then generate the answers!
                        while(qInstCursor.moveToNext()) {
                            int qId = qInstCursor.getInt(qInstCursor.getColumnIndexOrThrow(QuestionInstanceColumns._ID));
                            generateAnswers(qId, questionID);
                        }
                    }

                    qInstCursor.close();
                }
            }
            questionCursor.close();
        }
    }


    private void generateAnswers(int questionInstanceID, int questionID) {
        DataType dummy = DataType.STRING;
        boolean flagNew = false;
        Uri fieldsUri = DatabaseContentProvider.FIELD_URI;
        Uri answerUri = DatabaseContentProvider.ANSWER_URI;
        //search for the section and then for the question!!
        //here we query for the fields!!
        //Look at form instances!!
        Cursor fieldCursor = context.getContentResolver().query(fieldsUri, null, FieldColumns.QUESTION_ID + "= " + questionID, null, null);
        if (fieldCursor != null) {
            while (fieldCursor.moveToNext()) {
                int id = fieldCursor.getInt(fieldCursor.getColumnIndexOrThrow(FieldColumns._ID));
                String name = fieldCursor.getString(fieldCursor.getColumnIndexOrThrow(FieldColumns.NAME));
                String textName = fieldCursor.getString(fieldCursor.getColumnIndexOrThrow(FieldColumns.TEXT_NAME));
                String type = fieldCursor.getString(fieldCursor.getColumnIndexOrThrow(FieldColumns.TYPE));
                int req = fieldCursor.getInt(fieldCursor.getColumnIndexOrThrow(FieldColumns.REQUIRED));
                int rep = fieldCursor.getInt(fieldCursor.getColumnIndexOrThrow(FieldColumns.REPEAT));
                Field field = new Field(id, name, textName, dummy.getDataTypeFromString(type), req == 1 ? true : false, rep == 1 ? true : false);
                Cursor ansCursor = context.getContentResolver().query(answerUri, null, AnswerColumns.FIELD_ID + "= " + field.getId() + " and " + AnswerColumns.QUESTION_INSTANCE_ID + " = " + questionInstanceID, null, null);
                //query for form instance and field -> if result is empty then create new...
                //check for further setup for each field
                int ansId = 0;
                boolean prefilled = false;
                boolean applicable = false;
                boolean filled = false;
                ContentValues values = new ContentValues();

                //compute stat values!
                totalFields++;
                mtotalFields +=req;



                if (ansCursor.getCount() != 0) {
                    ansCursor.moveToFirst();
                    ansId = ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns._ID));
                    prefilled = ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns.PREFILLED)) == 1;
                    applicable = ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns.APPLICABLE)) == 1;
                    filled = ansCursor.getInt(ansCursor.getColumnIndexOrThrow(AnswerColumns.FILLED)) == 1;

                    if(applicable) {
                        if(filled) {
                            filledFields++;
                        }
                        else {
                            notFilledFields++;
                        }
                    }
                    else {
                        notApplicableFields++;
                    }
                    if(field.isRequired()){
                        if(applicable) {
                            if(filled) {
                                mFilled++;
                            }
                            else {
                                mNotFilled++;
                            }
                        }
                        else {
                            mNotApplicable++;
                        }
                    }

                } else {
                    //ID is auto increment!
                    //values.put(AnswerColumns._ID, a.getId());
                    values.put(AnswerColumns.PREFILLED, 0);
                    values.put(AnswerColumns.APPLICABLE, 1);
                    values.put(AnswerColumns.TYPE, field.getType().toString());
                    values.put(AnswerColumns.FIELD_ID, field.getId());
                    values.put(AnswerColumns.QUESTION_INSTANCE_ID, questionInstanceID);
                    values.put(AnswerColumns.FILLED, 0);
                    values.put(AnswerColumns.ANSWER_STRING, "");
                    values.put(AnswerColumns.ANSWER_INTEGER, 0);
                    values.put(AnswerColumns.ANSWER_FLOAT, 0);
                    flagNew = true;

                    notFilledFields++;
                    if(field.isRequired()){
                        mNotFilled++;
                    }
                }
                Uri uri = null;
                switch (field.getType()) {
                    case STRING:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case INTEGER:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case POSITIVE_INTEGER:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case NEGATIVE_INTEGER:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case FLOAT:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case POSITIVE_FLOAT:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case NEGATIVE_FLOAT:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case MEASURE:
                        if (ansCursor.getCount() == 0) {
                            values.put(AnswerColumns.ANSWER_STRING, "m");
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case ISTAT_CODE:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case GPS:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case DATE:
                        Calendar cal = Calendar.getInstance();
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case DATE_TIME:
                        Calendar calTime = Calendar.getInstance();
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case SELECT:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case SELECT1:
                        if (ansCursor.getCount() == 0) {
                            uri = context.getContentResolver().insert(answerUri, values);
                        }
                        break;
                    case SKETCH:
                        break;
                }
                if (flagNew) {
                    ansId = Integer.parseInt(uri.getLastPathSegment());
                    if(ansId < Util.BASE_ID) {
                        //set correct id
                        ContentValues val = new ContentValues();
                        val.put(QuestionInstanceColumns._ID, Util.BASE_ID);
                        context.getContentResolver().update(DatabaseContentProvider.ANSWER_URI, val, AnswerColumns._ID + " = " + ansId, null);
                    }
                }
                ansCursor.close();

            }
            fieldCursor.close();
        }
    }
}
