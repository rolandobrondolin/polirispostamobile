package it.polirisposta.mobile.poliRISPOSTA.model;

import java.io.Serializable;

import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DataType;

/**
 * Created by Rolando on 29/12/14.
 */
public abstract class Answer implements Serializable {
    private int id;
    private boolean prefilled;
    private boolean applicable;
    private DataType type;
    private Field field;
    private boolean filled;

    protected Answer(int id, boolean prefilled, boolean applicable, DataType type, boolean filled, Field f) {
        this.id = id;
        this.prefilled = prefilled;
        this.applicable = applicable;
        this.type = type;
        this.field = f;
        this.filled = filled;
    }

    protected Answer() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPrefilled() {
        return prefilled;
    }

    public void setPrefilled(boolean prefilled) {
        this.prefilled = prefilled;
    }

    public boolean isApplicable() {
        return applicable;
    }

    public void setApplicable(boolean applicable) {
        this.applicable = applicable;
    }

    public DataType getType() {
        return type;
    }

    public void setType(DataType type) {
        this.type = type;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public abstract void resetAnswer();
}
