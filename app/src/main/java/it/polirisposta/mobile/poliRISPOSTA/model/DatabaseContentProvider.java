package it.polirisposta.mobile.poliRISPOSTA.model;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;


import static it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract.*;


/**
 * Created by Rolando on 26/12/14.
 */
//TODO: implement table answer and form instance
//TODO: use it in content provider and to test create one form instance
//TODO: in the form activity and try to store the fields answer!
public class DatabaseContentProvider extends ContentProvider {

    static final String AUTHORITY = "it.polirisposta.mobile.data";
    private static final String FIELD_PATH = "fields";
    private static final String QUESTION_PATH = "questions";
    private static final String SECTION_PATH = "sections";
    private static final String FORM_PATH = "forms";
    private static final String CHOICE_PATH = "choices";
    private static final String FORM_INSTANCE_PATH = "form_instances";
    private static final String SECTION_INSTANCE_PATH = "section_instances";
    private static final String QUESTION_INSTANCE_PATH = "question_instances";
    private static final String ANSWER_PATH = "answers";
    private static final String RESOURCE_INSTANCE_PATH = "resource_instances";
    private static final String RESOURCE_PATH = "resources";
    private static final String TASK_PATH = "tasks";
    private static final String TASK_MODULE_PATH = "task_modules";
    private static final String NOTE_PATH = "notes";
    private static final String SETTINGS_PATH = "settings";



    //represents the code for a row inside the table
    private static final int FIELDS = 100;
    private static final int QUESTIONS = 200;
    private static final int SECTIONS = 300;
    private static final int FORMS = 400;
    private static final int CHOICES = 500;
    private static final int FORM_INSTANCES = 600;
    private static final int SECTION_INSTANCES = 700;
    private static final int QUESTION_INSTANCES = 800;
    private static final int ANSWERS = 900;
    private static final int RESOURCE_INSTANCES = 1000;
    private static final int RESOURCES = 1100;
    private static final int TASKS = 1200;
    private static final int TASK_MODULES = 1300;
    private static final int NOTES = 1400;
    private static final int SETTINGS = 1500;

    //represents the code for the id of a row inside the table
    private static final int FIELD_ID = 110;
    private static final int QUESTION_ID = 210;
    private static final int SECTION_ID = 310;
    private static final int FORM_ID = 410;
    private static final int CHOICE_ID = 510;
    private static final int FORM_INSTANCE_ID = 610;
    private static final int SECTION_INSTANCE_ID = 710;
    private static final int QUESTION_INSTANCE_ID = 810;
    private static final int ANSWER_ID = 910;
    private static final int RESOURCE_INSTANCE_ID = 1010;
    private static final int RESOURCE_ID = 1110;
    private static final int TASK_ID = 1210;
    private static final int TASK_MODULE_ID = 1310;
    private static final int NOTE_ID = 1410;

    private static final int RESOURCE_INSTANCE_URI_FIELD = 1020;

    public static final Uri FIELD_URI = Uri.parse("content://" + AUTHORITY + "/" + FIELD_PATH);
    public static final Uri QUESTION_URI = Uri.parse("content://" + AUTHORITY + "/" + QUESTION_PATH);
    public static final Uri SECTION_URI = Uri.parse("content://" + AUTHORITY + "/" + SECTION_PATH);
    public static final Uri FORM_URI = Uri.parse("content://" + AUTHORITY + "/" + FORM_PATH);
    public static final Uri CHOICE_URI = Uri.parse("content://" + AUTHORITY + "/" + CHOICE_PATH);
    public static final Uri FORM_INSTANCE_URI = Uri.parse("content://" + AUTHORITY + "/" + FORM_INSTANCE_PATH);
    public static final Uri SECTION_INSTANCE_URI = Uri.parse("content://" + AUTHORITY + "/" + SECTION_INSTANCE_PATH);
    public static final Uri QUESTION_INSTANCE_URI = Uri.parse("content://" + AUTHORITY + "/" + QUESTION_INSTANCE_PATH);
    public static final Uri ANSWER_URI = Uri.parse("content://" + AUTHORITY + "/" + ANSWER_PATH);
    public static final Uri RESOURCE_INSTANCE_URI = Uri.parse("content://" + AUTHORITY + "/" + RESOURCE_INSTANCE_PATH);
    public static final Uri RESOURCE_URI = Uri.parse("content://" + AUTHORITY + "/" + RESOURCE_PATH);
    public static final Uri TASK_URI = Uri.parse("content://" + AUTHORITY + "/" + TASK_PATH);
    public static final Uri TASK_MODULE_URI = Uri.parse("content://" + AUTHORITY + "/" + TASK_MODULE_PATH);
    public static final Uri NOTE_URI = Uri.parse("content://" + AUTHORITY + "/" + NOTE_PATH);
    public static final Uri SETTINGS_URI = Uri.parse("content://" + AUTHORITY + "/" + SETTINGS_PATH);

    public static final String FIELDS_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/fields";
    public static final String QUESTIONS_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/questions";
    public static final String SECTIONS_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/sections";
    public static final String FORMS_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/forms";
    public static final String CHOICES_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/choices";
    public static final String FORM_INSTANCES_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/form_instances";
    public static final String SECTION_INSTANCES_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/section_instances";
    public static final String QUESTION_INSTANCES_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/question_instances";
    public static final String ANSWERS_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/answers";
    public static final String RESOURCE_INSTANCE_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/resource_instances";
    public static final String RESOURCE_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/resources";
    public static final String TASK_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/tasks";
    public static final String TASK_MODULE_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/task_modules";
    public static final String NOTE_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/notes";
    public static final String SETTINGS_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/settings";

    public static final String FIELD_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/field";
    public static final String QUESTION_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/question";
    public static final String SECTION_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/section";
    public static final String FORM_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/form";
    public static final String CHOICE_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/choice";
    public static final String FORM_INSTANCE_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/form_instance";
    public static final String SECTION_INSTANCE_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/section_instance";
    public static final String QUESTION_INSTANCE_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/question_instance";
    public static final String ANSWER_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/answer";
    public static final String RESOURCE_INSTANCE_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/resource_instance";
    public static final String RESOURCE_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/resource";
    public static final String TASK_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/task";
    public static final String TASK_MODULE_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/task_module";
    public static final String NOTE_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/note";


    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, FIELD_PATH, FIELDS);
        sURIMatcher.addURI(AUTHORITY, FIELD_PATH + "/#", FIELD_ID);

        sURIMatcher.addURI(AUTHORITY, QUESTION_PATH, QUESTIONS);
        sURIMatcher.addURI(AUTHORITY, QUESTION_PATH + "/#", QUESTION_ID);

        sURIMatcher.addURI(AUTHORITY, SECTION_PATH, SECTIONS);
        sURIMatcher.addURI(AUTHORITY, SECTION_PATH + "/#", SECTION_ID);

        sURIMatcher.addURI(AUTHORITY, FORM_PATH, FORMS);
        sURIMatcher.addURI(AUTHORITY, FORM_PATH + "/#", FORM_ID);

        sURIMatcher.addURI(AUTHORITY, CHOICE_PATH, CHOICES);
        sURIMatcher.addURI(AUTHORITY, CHOICE_PATH + "/#", CHOICE_ID);

        sURIMatcher.addURI(AUTHORITY, FORM_INSTANCE_PATH, FORM_INSTANCES);
        sURIMatcher.addURI(AUTHORITY, FORM_INSTANCE_PATH + "/#", FORM_INSTANCE_ID);

        sURIMatcher.addURI(AUTHORITY, SECTION_INSTANCE_PATH, SECTION_INSTANCES);
        sURIMatcher.addURI(AUTHORITY, SECTION_INSTANCE_PATH + "/#", SECTION_INSTANCE_ID);

        sURIMatcher.addURI(AUTHORITY, QUESTION_INSTANCE_PATH, QUESTION_INSTANCES);
        sURIMatcher.addURI(AUTHORITY, QUESTION_INSTANCE_PATH + "/#", QUESTION_INSTANCE_ID);

        sURIMatcher.addURI(AUTHORITY, ANSWER_PATH, ANSWERS);
        sURIMatcher.addURI(AUTHORITY, ANSWER_PATH + "/#", ANSWER_ID);

        sURIMatcher.addURI(AUTHORITY, RESOURCE_INSTANCE_PATH, RESOURCE_INSTANCES);
        sURIMatcher.addURI(AUTHORITY, RESOURCE_INSTANCE_PATH + "/#", RESOURCE_INSTANCE_ID);
        //sURIMatcher.addURI(AUTHORITY, RESOURCE_INSTANCE_PATH + "/", RESOURCE_INSTANCE_URI_FIELD);

        sURIMatcher.addURI(AUTHORITY, RESOURCE_PATH, RESOURCES);
        sURIMatcher.addURI(AUTHORITY, RESOURCE_PATH + "/#", RESOURCE_ID);

        sURIMatcher.addURI(AUTHORITY, TASK_PATH, TASKS);
        sURIMatcher.addURI(AUTHORITY, TASK_PATH + "/#", TASK_ID);

        sURIMatcher.addURI(AUTHORITY, TASK_MODULE_PATH, TASK_MODULES);
        sURIMatcher.addURI(AUTHORITY, TASK_MODULE_PATH + "/#", TASK_MODULE_ID);

        sURIMatcher.addURI(AUTHORITY, NOTE_PATH, NOTES);
        sURIMatcher.addURI(AUTHORITY, NOTE_PATH + "/#", NOTE_ID);

        sURIMatcher.addURI(AUTHORITY, SETTINGS_PATH, SETTINGS);


    }

    private DatabaseOpenHelper db;

    @Override
    public boolean onCreate() {
        this.db = new DatabaseOpenHelper(this.getContext());
        return false;
    }

    //TODO: implement search by tag!
    //TODO: refactor code to allow use of joins
    //at the moment we must make 2 query -> search for field and then search for related question for instance
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        //checkColumns(projection);

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case FIELDS:
                queryBuilder.setTables(FieldColumns.TABLE_NAME);
                break;
            case FIELD_ID:
                queryBuilder.setTables(FieldColumns.TABLE_NAME);
                queryBuilder.appendWhere(FieldColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case QUESTIONS:
                queryBuilder.setTables(QuestionColumns.TABLE_NAME);
                break;
            case QUESTION_ID:
                queryBuilder.setTables(QuestionColumns.TABLE_NAME);
                queryBuilder.appendWhere(QuestionColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case SECTIONS:
                queryBuilder.setTables(SectionColumns.TABLE_NAME);
                break;
            case SECTION_ID:
                queryBuilder.setTables(SectionColumns.TABLE_NAME);
                queryBuilder.appendWhere(SectionColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case FORMS:
                queryBuilder.setTables(FormColumns.TABLE_NAME);
                break;
            case FORM_ID:
                queryBuilder.setTables(FormColumns.TABLE_NAME);
                queryBuilder.appendWhere(FormColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case CHOICES:
                queryBuilder.setTables(ChoiceColumns.TABLE_NAME);
                break;
            case CHOICE_ID:
                queryBuilder.setTables(ChoiceColumns.TABLE_NAME);
                queryBuilder.appendWhere(ChoiceColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case FORM_INSTANCES:
                queryBuilder.setTables(FormInstanceColumns.TABLE_NAME);
                break;
            case FORM_INSTANCE_ID:
                queryBuilder.setTables(FormInstanceColumns.TABLE_NAME);
                queryBuilder.appendWhere(FormInstanceColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case SECTION_INSTANCES:
                queryBuilder.setTables(SectionInstanceColumns.TABLE_NAME);
                break;
            case SECTION_INSTANCE_ID:
                queryBuilder.setTables(SectionInstanceColumns.TABLE_NAME);
                queryBuilder.appendWhere(SectionInstanceColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case QUESTION_INSTANCES:
                queryBuilder.setTables(QuestionInstanceColumns.TABLE_NAME);
                break;
            case QUESTION_INSTANCE_ID:
                queryBuilder.setTables(QuestionInstanceColumns.TABLE_NAME);
                queryBuilder.appendWhere(QuestionInstanceColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case ANSWERS:
                queryBuilder.setTables(AnswerColumns.TABLE_NAME);
                break;
            case ANSWER_ID:
                queryBuilder.setTables(AnswerColumns.TABLE_NAME);
                queryBuilder.appendWhere(AnswerColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case RESOURCE_INSTANCES:
                queryBuilder.setTables(ResourceInstanceColumns.TABLE_NAME);
                break;
            case RESOURCE_INSTANCE_ID:
                queryBuilder.setTables(ResourceInstanceColumns.TABLE_NAME);
                queryBuilder.appendWhere(ResourceInstanceColumns._ID + "=" + uri.getLastPathSegment());
                break;
            /*case RESOURCE_INSTANCE_URI_FIELD:

                break;*/
            case RESOURCES:
                queryBuilder.setTables(ResourceColumns.TABLE_NAME);
                break;
            case RESOURCE_ID:
                queryBuilder.setTables(ResourceColumns.TABLE_NAME);
                queryBuilder.appendWhere(ResourceColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case TASKS:
                queryBuilder.setTables(TaskColumns.TABLE_NAME);
                break;
            case TASK_ID:
                queryBuilder.setTables(TaskColumns.TABLE_NAME);
                queryBuilder.appendWhere(TaskColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case TASK_MODULES:
                queryBuilder.setTables(TaskModuleColumns.TABLE_NAME);
                break;
            case TASK_MODULE_ID:
                queryBuilder.setTables(TaskModuleColumns.TABLE_NAME);
                queryBuilder.appendWhere(TaskModuleColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case NOTES:
                queryBuilder.setTables(NoteColumns.TABLE_NAME);
                break;
            case NOTE_ID:
                queryBuilder.setTables(NoteColumns.TABLE_NAME);
                queryBuilder.appendWhere(NoteColumns._ID + "=" + uri.getLastPathSegment());
                break;
            case SETTINGS:
                queryBuilder.setTables(SettingsColumns.TABLE_NAME);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = this.db.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case FIELDS:
                return FIELDS_TYPE;
            case FIELD_ID:
                return FIELD_ITEM_TYPE;
            case QUESTIONS:
                return QUESTIONS_TYPE;
            case QUESTION_ID:
                return QUESTION_ITEM_TYPE;
            case SECTIONS:
                return SECTIONS_TYPE;
            case SECTION_ID:
                return SECTION_ITEM_TYPE;
            case FORMS:
                return FORMS_TYPE;
            case FORM_ID:
                return FORM_ITEM_TYPE;
            case CHOICES:
                return CHOICES_TYPE;
            case CHOICE_ID:
                return CHOICE_ITEM_TYPE;
            case FORM_INSTANCES:
                return FORM_INSTANCES_TYPE;
            case FORM_INSTANCE_ID:
                return FORM_INSTANCE_ITEM_TYPE;
            case SECTION_INSTANCES:
                return SECTION_INSTANCES_TYPE;
            case SECTION_INSTANCE_ID:
                return SECTION_INSTANCE_ITEM_TYPE;
            case QUESTION_INSTANCES:
                return QUESTION_INSTANCES_TYPE;
            case QUESTION_INSTANCE_ID:
                return QUESTION_INSTANCE_ITEM_TYPE;
            case ANSWERS:
                return ANSWERS_TYPE;
            case ANSWER_ID:
                return ANSWER_ITEM_TYPE;
            case RESOURCE_INSTANCES:
                return RESOURCE_INSTANCE_TYPE;
            case RESOURCE_INSTANCE_ID:
                return RESOURCE_INSTANCE_ITEM_TYPE;
            case RESOURCES:
                return RESOURCE_TYPE;
            case RESOURCE_ID:
                return RESOURCE_ITEM_TYPE;
            case TASKS:
                return TASK_TYPE;
            case TASK_ID:
                return TASK_ITEM_TYPE;
            case TASK_MODULES:
                return TASK_MODULE_TYPE;
            case TASK_MODULE_ID:
                return TASK_MODULE_ITEM_TYPE;
            case NOTES:
                return NOTE_TYPE;
            case NOTE_ID:
                return NOTE_ITEM_TYPE;
            case SETTINGS:
                return SETTINGS_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = this.db.getWritableDatabase();
        long id = 0;
        //TODO: add check of correct values/columns!
        switch (uriType) {
            case FIELDS:
                id = sqlDB.insert(FieldColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(FIELD_PATH + "/" + id);
            case QUESTIONS:
                id = sqlDB.insert(QuestionColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(QUESTION_PATH + "/" + id);
            case SECTIONS:
                id = sqlDB.insert(SectionColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(SECTION_PATH + "/" + id);
            case FORMS:
                id = sqlDB.insert(FormColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(FORM_PATH + "/" + id);
            case CHOICES:
                id = sqlDB.insert(ChoiceColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(CHOICE_PATH + "/" + id);
            case FORM_INSTANCES:
                id = sqlDB.insert(FormInstanceColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(FORM_INSTANCE_PATH + "/" + id);
            case SECTION_INSTANCES:
                id = sqlDB.insert(SectionInstanceColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(SECTION_INSTANCE_PATH + "/" + id);
            case QUESTION_INSTANCES:
                id = sqlDB.insert(QuestionInstanceColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(QUESTION_INSTANCE_PATH + "/" + id);
            case ANSWERS:
                id = sqlDB.insert(AnswerColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(ANSWER_PATH + "/" + id);
            case RESOURCE_INSTANCES:
                id = sqlDB.insert(ResourceInstanceColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(RESOURCE_INSTANCE_PATH + "/" + id);
            case RESOURCES:
                id = sqlDB.insert(ResourceColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(RESOURCE_PATH + "/" + id);
            case TASKS:
                id = sqlDB.insert(TaskColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(TASK_PATH + "/" + id);
            case TASK_MODULES:
                id = sqlDB.insert(TaskModuleColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(TASK_MODULE_PATH + "/" + id);
            case NOTES:
                id = sqlDB.insert(NoteColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(TASK_PATH + "/" + id);
            case SETTINGS:
                id = sqlDB.insert(SettingsColumns.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(TASK_PATH + "/" + id);
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    //TODO: Delete must be done checking the references (delete form -> delete sections ->...)! And in a safe way!
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = this.db.getWritableDatabase();
        int rowsDeleted = 0;
        String id = new String();
        //TODO: add check of correct values/columns!
        switch (uriType) {
            case FIELDS:
                rowsDeleted = sqlDB.delete(FieldColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case FIELD_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(FieldColumns.TABLE_NAME, FieldColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(FieldColumns.TABLE_NAME, FieldColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case QUESTIONS:
                rowsDeleted = sqlDB.delete(QuestionColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case QUESTION_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(QuestionColumns.TABLE_NAME, QuestionColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(QuestionColumns.TABLE_NAME, QuestionColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case SECTIONS:
                rowsDeleted = sqlDB.delete(SectionColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case SECTION_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(SectionColumns.TABLE_NAME, SectionColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(SectionColumns.TABLE_NAME, SectionColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case FORMS:
                rowsDeleted = sqlDB.delete(FormColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case FORM_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(FormColumns.TABLE_NAME, FormColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(FormColumns.TABLE_NAME, FormColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case CHOICES:
                rowsDeleted = sqlDB.delete(ChoiceColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case CHOICE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(ChoiceColumns.TABLE_NAME, ChoiceColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(ChoiceColumns.TABLE_NAME, ChoiceColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case FORM_INSTANCES:
                rowsDeleted = sqlDB.delete(FormInstanceColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case FORM_INSTANCE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(FormInstanceColumns.TABLE_NAME, FormInstanceColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(FormInstanceColumns.TABLE_NAME, FormInstanceColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case SECTION_INSTANCES:
                rowsDeleted = sqlDB.delete(SectionInstanceColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case SECTION_INSTANCE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(SectionInstanceColumns.TABLE_NAME, SectionInstanceColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(SectionInstanceColumns.TABLE_NAME, SectionInstanceColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case QUESTION_INSTANCES:
                rowsDeleted = sqlDB.delete(QuestionInstanceColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case QUESTION_INSTANCE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(QuestionInstanceColumns.TABLE_NAME, QuestionInstanceColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(QuestionInstanceColumns.TABLE_NAME, QuestionInstanceColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;

            case ANSWERS:
                rowsDeleted = sqlDB.delete(AnswerColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case ANSWER_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(AnswerColumns.TABLE_NAME, AnswerColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(AnswerColumns.TABLE_NAME, AnswerColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case RESOURCE_INSTANCES:
                rowsDeleted = sqlDB.delete(ResourceInstanceColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case RESOURCE_INSTANCE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(ResourceInstanceColumns.TABLE_NAME, AnswerColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(ResourceInstanceColumns.TABLE_NAME, AnswerColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case RESOURCES:
                rowsDeleted = sqlDB.delete(ResourceColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case RESOURCE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(ResourceColumns.TABLE_NAME, AnswerColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(ResourceColumns.TABLE_NAME, AnswerColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case TASKS:
                rowsDeleted = sqlDB.delete(TaskColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case TASK_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(TaskColumns.TABLE_NAME, TaskColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(TaskColumns.TABLE_NAME, TaskColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case NOTES:
                rowsDeleted = sqlDB.delete(NoteColumns.TABLE_NAME, selection, selectionArgs);
                break;
            case NOTE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(NoteColumns.TABLE_NAME, NoteColumns._ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(NoteColumns.TABLE_NAME, NoteColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case SETTINGS:
                rowsDeleted = sqlDB.delete(SettingsColumns.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return rowsDeleted;
    }

    //TODO: check the invariance of the foreign keys
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        String id;
        SQLiteDatabase sqlDB = this.db.getWritableDatabase();
        int rowsUpdated = 0;
        switch (uriType) {
            case ANSWERS:
                rowsUpdated = sqlDB.update(AnswerColumns.TABLE_NAME, values, selection, selectionArgs);
                break;
            case ANSWER_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(AnswerColumns.TABLE_NAME, values, AnswerColumns._ID + "=" + id, null);
                }
                else {
                    rowsUpdated = sqlDB.update(AnswerColumns.TABLE_NAME, values, AnswerColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case QUESTION_INSTANCES:
                rowsUpdated = sqlDB.update(QuestionInstanceColumns.TABLE_NAME, values, selection, selectionArgs);
                break;
            case QUESTION_INSTANCE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(QuestionInstanceColumns.TABLE_NAME, values, QuestionInstanceColumns._ID + "=" + id, null);
                }
                else {
                    rowsUpdated = sqlDB.update(QuestionInstanceColumns.TABLE_NAME, values, QuestionInstanceColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case SECTION_INSTANCES:
                rowsUpdated = sqlDB.update(SectionInstanceColumns.TABLE_NAME, values, selection, selectionArgs);
                break;
            case SECTION_INSTANCE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(SectionInstanceColumns.TABLE_NAME, values, SectionInstanceColumns._ID + "=" + id, null);
                }
                else {
                    rowsUpdated = sqlDB.update(SectionInstanceColumns.TABLE_NAME, values, SectionInstanceColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case FORM_INSTANCES:
                rowsUpdated = sqlDB.update(FormInstanceColumns.TABLE_NAME, values, selection, selectionArgs);
                break;
            case FORM_INSTANCE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(FormInstanceColumns.TABLE_NAME, values, FormInstanceColumns._ID + "=" + id, null);
                }
                else {
                    rowsUpdated = sqlDB.update(FormInstanceColumns.TABLE_NAME, values, FormInstanceColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case RESOURCE_INSTANCES:
                rowsUpdated = sqlDB.update(ResourceInstanceColumns.TABLE_NAME, values, selection, selectionArgs);
                break;
            case RESOURCE_INSTANCE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(ResourceInstanceColumns.TABLE_NAME, values, ResourceInstanceColumns._ID + "=" + id, null);
                }
                else {
                    rowsUpdated = sqlDB.update(ResourceInstanceColumns.TABLE_NAME, values, ResourceInstanceColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case TASKS:
                rowsUpdated = sqlDB.update(TaskColumns.TABLE_NAME, values, selection, selectionArgs);
                break;
            case TASK_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(TaskColumns.TABLE_NAME, values, TaskColumns._ID + "=" + id, null);
                }
                else {
                    rowsUpdated = sqlDB.update(TaskColumns.TABLE_NAME, values, TaskColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case TASK_MODULES:
                rowsUpdated = sqlDB.update(TaskModuleColumns.TABLE_NAME, values, selection, selectionArgs);
                break;
            case TASK_MODULE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(TaskModuleColumns.TABLE_NAME, values, TaskModuleColumns._ID + "=" + id, null);
                }
                else {
                    rowsUpdated = sqlDB.update(TaskModuleColumns.TABLE_NAME, values, TaskModuleColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case NOTES:
                rowsUpdated = sqlDB.update(NoteColumns.TABLE_NAME, values, selection, selectionArgs);
                break;
            case NOTE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(NoteColumns.TABLE_NAME, values, NoteColumns._ID + "=" + id, null);
                }
                else {
                    rowsUpdated = sqlDB.update(NoteColumns.TABLE_NAME, values, NoteColumns._ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case SETTINGS:
                rowsUpdated = sqlDB.update(SettingsColumns.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return rowsUpdated;
    }
}
