package it.polirisposta.mobile.poliRISPOSTA.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.DataType;

import static it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract.*;

/**
 * Created by Rolando on 23/12/14.
 */
//TODO add answer tables and form instances for saving purposes (plus notes and resources acquired by the user)
//TODO add choices and tags and resources table
//TODO add status info on form section and question
public class DatabaseOpenHelper extends SQLiteOpenHelper {

    public static final String dbName = "polirispostadb";
    public static final int dbVersion = 1;


    private static final String dbCreat2 = "CREATE TABLE `section_instance` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`s_index`	INTEGER NOT NULL DEFAULT 0, " +
            "`form_instance_id`	INTEGER NOT NULL REFERENCES form_instance(ID) ON DELETE CASCADE, " +
            "`section_id`	INTEGER NOT NULL REFERENCES section(ID) ON DELETE CASCADE" +
    "); ";

    private static final String dbCreat3 = "CREATE TABLE `section` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`name`	TEXT NOT NULL UNIQUE, " +
            "`text_name`	TEXT NOT NULL, " +
            "`description`	TEXT NOT NULL, " +
            "`required`	INTEGER NOT NULL DEFAULT 0 CHECK(required < 2 and required >=0), " +
            "`repeat`	INTEGER NOT NULL DEFAULT 0 CHECK(repeat < 2 and repeat >=0), " +
            "`form_id`	INTEGER NOT NULL DEFAULT 0 REFERENCES form(ID) ON DELETE CASCADE" +
    "); " ;

    private static final String dbCreat4 = "CREATE TABLE `resource_instance` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`type`	TEXT NOT NULL, " +
            "`path`	TEXT NOT NULL, " +
            "`description`	TEXT, " +
            "`question_instance_id`	INTEGER NOT NULL REFERENCES question_instance(ID) ON DELETE CASCADE" +
    "); ";

    private static final String dbCreat5 = "CREATE TABLE `resource` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`description`	TEXT, " +
            "`type`	TEXT NOT NULL, " +
            "`path`	TEXT NOT NULL, " +
            "`question_id`	INTEGER NOT NULL REFERENCES question(ID) ON DELETE CASCADE" +
    "); ";
    private static final String dbCreat6 = "CREATE TABLE `question_instance` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`q_index`	INTEGER NOT NULL DEFAULT 0, " +
            "`section_instance_id`	INTEGER NOT NULL REFERENCES section_instance(ID) ON DELETE CASCADE, " +
            "`question_id`	INTEGER NOT NULL REFERENCES question(ID) ON DELETE CASCADE" +
    "); ";
    private static final String dbCreat7 = "CREATE TABLE 'question' ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`name`	TEXT NOT NULL, " +
            "`text_name`	TEXT NOT NULL, " +
            "`description`	TEXT NOT NULL, " +
            "`required`	INTEGER NOT NULL DEFAULT 0 CHECK(required < 2 and required >= 0), " +
            "`repeat`	INTEGER NOT NULL DEFAULT 0 CHECK(repeat < 2 and repeat >= 0), " +
            "`section_id`	INTEGER NOT NULL REFERENCES section(ID) ON DELETE CASCADE" +
    "); " ;

    private static final String dbCreat8 = "CREATE TABLE `note` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`text`	TEXT NOT NULL, " +
            "`question_instance_id`	INTEGER NOT NULL REFERENCES question_instance(ID) ON DELETE CASCADE" +
    "); ";
    private static final String dbCreat9 = "CREATE TABLE `form_instance` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`task_id`	INTEGER NOT NULL REFERENCES task(ID) ON DELETE CASCADE, " +
            "`form_id`	INTEGER NOT NULL REFERENCES form(ID) ON DELETE CASCADE, " +
            "`timestamp`	NUMERIC NOT NULL UNIQUE, " +
            "`device_id`	TEXT NOT NULL, " +
            "`total_fields`	INTEGER NOT NULL, " +
            "`total_mandatory_fields`	INTEGER NOT NULL, " +
            "`filled_fields`	INTEGER NOT NULL, " +
            "`not_filled_fields`	INTEGER NOT NULL, " +
            "`not_applicable_fields`	INTEGER NOT NULL, " +
            "`mandatory_filled_fields`	INTEGER NOT NULL, " +
            "`mandatory_not_filled_fields`	INTEGER NOT NULL, " +
            "`mandatory_not_applicable_fields`	INTEGER NOT NULL, " +
            "`finalized`	INTEGER NOT NULL DEFAULT 0 CHECK(finalized < 2 and finalized >= 0) " +
    "); ";
    private static final String dbCreat10 ="CREATE TABLE 'form_hierarchy' ( " +
            "`parent_form`	INTEGER NOT NULL REFERENCES form(ID) ON DELETE CASCADE, " +
            "`child_form`	INTEGER NOT NULL REFERENCES form(ID) ON DELETE CASCADE" +
    "); ";
    private static final String dbCreat11 ="CREATE TABLE `form` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`name`	TEXT NOT NULL UNIQUE, " +
            "`text_name`	TEXT NOT NULL, " +
            "`description`	TEXT NOT NULL " +
    "); ";

    private static final String dbCreat12 ="CREATE TABLE 'field' ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`name`	TEXT NOT NULL UNIQUE, " +
            "`text_name`	TEXT NOT NULL, " +
            "`type`	TEXT NOT NULL, " +
            "`required`	INTEGER NOT NULL DEFAULT 0 CHECK(required < 2 and required >= 0), " +
            "`repeat`	INTEGER NOT NULL DEFAULT 0 CHECK(repeat < 2 and repeat >= 0), " +
            "`cond`	INTEGER NOT NULL DEFAULT 0 CHECK(cond < 2 and cond >= 0), " +
            "`cond_statement`	TEXT, " +
            "`op1`	INTEGER, " +
            "`op2`	INTEGER, " +
            "`question_id`	INTEGER NOT NULL DEFAULT 0 REFERENCES question(ID) ON DELETE CASCADE" +
    "); " ;

    private static final String dbCreat13 = "CREATE TABLE `choice` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`text`	TEXT NOT NULL, " +
            "`other`	INTEGER NOT NULL DEFAULT 0 CHECK(other < 2 and other >= 0), " +
            "`field_id`	INTEGER NOT NULL REFERENCES field(ID) ON DELETE CASCADE" +
    "); ";
    private static final String dbCreat14 = "CREATE TABLE `answer` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`prefilled`	INTEGER NOT NULL DEFAULT 0 CHECK(prefilled < 2 and prefilled >=0), " +
            "`applicable`	INTEGER NOT NULL DEFAULT 1 CHECK(applicable < 2 and applicable >=0), " +
            "`type`	TEXT NOT NULL, " +
            "`filled` INTEGER NOT NULL DEFAULT 0 CHECK(filled < 2 and filled >=0), " +
            "`answer_string`	TEXT, " +
            "`answer_integer`	INTEGER, " +
            "`answer_float`	REAL, " +
            "`field_id`	INTEGER NOT NULL REFERENCES field(ID) ON DELETE CASCADE, " +
            "`question_instance_id`	INTEGER NOT NULL REFERENCES question_instance(ID) ON DELETE CASCADE" +
            "); " ;

    private static final String dbCreat15 = "CREATE TABLE `task` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`name`	TEXT, " +
            "`description`	TEXT, " +
            "`group_id`	INTEGER NOT NULL, " +
            "`generated` INTEGER NOT NULL DEFAULT 0 CHECK(generated < 2 and generated >=0), " +
            "`user_defined` INTEGER NOT NULL DEFAULT 0 CHECK(user_defined < 2 and user_defined >=0), " +
            "`sent` INTEGER NOT NULL DEFAULT 0 CHECK(sent < 2 and sent >=0)" +
            //TODO: other things?
            "); " ;

    private static final String dbCreat16 = "CREATE TABLE `task_module` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`task_id`	INTEGER NOT NULL REFERENCES task(ID) ON DELETE CASCADE, " +
            "`form_id`	INTEGER NOT NULL, " + //TODO:delete manually
            "`n_instances`	INTEGER NOT NULL" +
            "); " ;

    private static final String dbCreat17 = "CREATE TABLE `settings` ( " +
            "`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
            "`group_id`	INTEGER NOT NULL, " +
            "`username`	TEXT NOT NULL, " +
            "`password`	TEXT NOT NULL, " +
            "`address`	TEXT NOT NULL, " +
            "`session_name`	TEXT NOT NULL, " +
            "`session_id`	TEXT NOT NULL, " +
            "`csrftoken_name`	TEXT NOT NULL, " +
            "`csrftoken_id`	TEXT NOT NULL" +
            "); " ;
/*
    public static final String ins = "INSERT INTO `task` VALUES ('1','task di prova','descrizione del task di prova','0','1','0','0');";
    public static final String insModule = "INSERT INTO `task_module` VALUES ('1','1','1','1');";
    public static final String insModule2 = "INSERT INTO `task_module` VALUES ('2','1','2','0');";

    public static final String insT1 = "INSERT INTO `task` VALUES ('2','secondo task di prova','descrizione del primo task di prova','0','1','0','0');";
    public static final String insModuleT11 = "INSERT INTO `task_module` VALUES ('3','2','1','1');";
    public static final String insModuleT12 = "INSERT INTO `task_module` VALUES ('4','2','2','0');";

    public static final String insT2 = "INSERT INTO `task` VALUES ('3','terzo task di prova','descrizione del secondo task di prova','0','1','0','0');";
    public static final String insModuleT21 = "INSERT INTO `task_module` VALUES ('5','3','1','1');";
    public static final String insModuleT22 = "INSERT INTO `task_module` VALUES ('6','3','2','0');";


    public static final String ins0 = "INSERT INTO `form` VALUES ('1','scheda_A','Scheda A: informazioni generali','Scheda di identificazione edificio nelle sue informazioni generali');";

    public static final String ins1 = "INSERT INTO `section` VALUES ('1','section_0_scheda_A','Sezione 0: Dati questionario','','1','0','1');";
    public static final String ins2 = "INSERT INTO `section` VALUES ('2','section_1_scheda_A','Sezione 1: Informazioni generali','Informazioni di base','1','0','1');";
    public static final String ins3 = "INSERT INTO `section` VALUES ('3','section_2_scheda_A','Sezione 2: Caratteristiche edificio','Caratteristiche dell''edificio','1','0','1');";
    public static final String ins4 = "INSERT INTO `section` VALUES ('4','section_3_scheda_A','Sezione 3: Descrizione evento','Descrizione dell''evento','1','1','1');";

    public static final String ins5 = "INSERT INTO `question` VALUES ('1','q0_s0_scheda_A','Informazioni generali','Informazioni generali','1','0','1');";
    public static final String ins6 = "INSERT INTO `question` VALUES ('2','q1_s1_scheda_A','Indirizzo','Fornire indirizzo dell''edificio','1','0','2');";
    public static final String ins7 = "INSERT INTO `question` VALUES ('3','q2_s1_scheda_A','Coordinate geografiche','Inserire coordinate geografiche','1','0','2');";
    public static final String ins8 = "INSERT INTO `question` VALUES ('4','q3_s1_scheda_A','Riferimenti catastali','Inserire i riferimenti catastali (Foglio e Mappale)','1','0','2');";
    public static final String ins9 = "INSERT INTO `question` VALUES ('5','q4_s1_scheda_A','Modalità di rilievo','Inserire i dati sulla modalità di rilievo','1','0','2');";
    public static final String ins10 = "INSERT INTO `question` VALUES ('6','q0_s2_scheda_A','Tipologia Edificio','Inserire i dati sulla tipologia di edificio considerato','1','0','3');";
    public static final String ins11 = "INSERT INTO `question` VALUES ('7','q1_s2_scheda_A','Età di costruzione','Inserire un range indicativo dell''età di costruzione','1','0','3');";
    public static final String ins12 = "INSERT INTO `question` VALUES ('8','q2_s2_scheda_A','Tipologia Strutturale','Inserire la tipologia strutturale dell''edificio','1','0','3');";
    public static final String ins13 = "INSERT INTO `question` VALUES ('9','q3_s2_scheda_A','Superficie','Indicare una misura precisa della superficie dell''edificio','1','0','3');";
    public static final String ins14 = "INSERT INTO `question` VALUES ('10','q4_s2_scheda_A','Numero piani','Indicare il numero di piani dell''edificio','1','0','3');";
    public static final String ins15 = "INSERT INTO `question` VALUES ('11','q5_s2_scheda_A','Quote edificio','Indicare come da figura le quote dell''edificio considerato','1','0','3');";
    public static final String ins16 = "INSERT INTO `question` VALUES ('12','q0_s3_scheda_A','Durata evento','Inserire data e ora stimate dell''evento, ripetere se più di uno','1','1','4');";
    public static final String ins17 = "INSERT INTO `question` VALUES ('13','q1_s3_scheda_A','Altezza acqua esterno edificio','Inserire l''altezza misurata dell''acqua all''esterno dell''edificio','1','0','4');";
    public static final String ins18 = "INSERT INTO `question` VALUES ('14','q2_s3_scheda_A','Presenza di sedimenti','Indicare se presenti sedimenti','1','0','4');";
    public static final String ins19 = "INSERT INTO `question` VALUES ('15','q3_s3_scheda_A','Presenza di contaminanti','Indicare se presenti contaminanti','1','0','4');";


    public static final String ins20 = "INSERT INTO `field` VALUES ('1','f0_q0_s0_scheda_A','Codice ISTAT Provincia','istat_code','1','0','0',NULL,NULL,NULL,'1');";
    public static final String ins21 = "INSERT INTO `field` VALUES ('2','f1_q0_s0_scheda_A','Codice ISTAT Comune','istat_code','1','0','0',NULL,NULL,NULL,'1');";
    public static final String ins22 = "INSERT INTO `field` VALUES ('3','f2_q0_s0_scheda_A','Comune','string','1','0','0',NULL,NULL,NULL,'1');";
    public static final String ins23 = "INSERT INTO `field` VALUES ('4','f3_q0_s0_scheda_A','Data','date','1','0','0',NULL,NULL,NULL,'1');";
    public static final String ins24 = "INSERT INTO `field` VALUES ('5','f0_q0_s1_scheda_A','Tipo indirizzo','select1','1','0','0',NULL,NULL,NULL,'2');";
    public static final String ins25 = "INSERT INTO `field` VALUES ('6','f1_q0_s1_scheda_A','Indirizzo','string','1','0','0',NULL,NULL,NULL,'2');";
    public static final String ins26 = "INSERT INTO `field` VALUES ('7','f2_q0_s1_scheda_A','Numero civico','positive_integer','1','0','0',NULL,NULL,NULL,'2');";
    public static final String ins27 = "INSERT INTO `field` VALUES ('8','f0_q1_s1_scheda_A','Coordinate geografiche','gps','1','0','0',NULL,NULL,NULL,'3');";
    public static final String ins28 = "INSERT INTO `field` VALUES ('9','f0_q2_s1_scheda_A','Foglio','positive_integer','1','0','0',NULL,NULL,NULL,'4');";
    public static final String ins29 = "INSERT INTO `field` VALUES ('10','f1_q2_s1_scheda_A','Mappale','positive_integer','1','0','0',NULL,NULL,NULL,'4');";
    public static final String ins30 = "INSERT INTO `field` VALUES ('11','f0_q3_s1_scheda_A','Squadra','positive_integer','1','0','0',NULL,NULL,NULL,'5');";
    public static final String ins31 = "INSERT INTO `field` VALUES ('12','f1_q3_s1_scheda_A','Rilevatore','positive_integer','1','0','0',NULL,NULL,NULL,'5');";
    public static final String ins32 = "INSERT INTO `field` VALUES ('13','f2_q3_s1_scheda_A','Nome privato cittadino','string','1','0','0',NULL,NULL,NULL,'5');";
    public static final String ins33 = "INSERT INTO `field` VALUES ('14','f3_q3_s1_scheda_A','Cognome privato cittadino','string','1','0','0',NULL,NULL,NULL,'5');";
    public static final String ins34 = "INSERT INTO `field` VALUES ('15','f4_q3_s1_scheda_A','Recapito privato cittadino','string','1','0','0',NULL,NULL,NULL,'5');";
    public static final String ins35 = "INSERT INTO `field` VALUES ('16','f5_q3_s1_scheda_A','Ruolo','select1','1','0','0',NULL,NULL,NULL,'5');";
    public static final String ins36 = "INSERT INTO `field` VALUES ('17','f0_q0_s2_scheda_A','Tipologia Edificio','select1','1','0','0',NULL,NULL,NULL,'6');";
    public static final String ins37 = "INSERT INTO `field` VALUES ('18','f1_q0_s2_scheda_A','Unità immobiliari','positive_integer','0','0','0',NULL,NULL,NULL,'6');";
    public static final String ins38 = "INSERT INTO `field` VALUES ('19','f2_q0_s2_scheda_A','Annessi','positive_integer','0','0','0',NULL,NULL,NULL,'6');";
    public static final String ins39 = "INSERT INTO `field` VALUES ('20','f3_q0_s2_scheda_A','Specificare','string','0','0','0',NULL,NULL,NULL,'6');";
    public static final String ins40 = "INSERT INTO `field` VALUES ('21','f0_q1_s2_scheda_A','Età di costruzione','select1','1','0','0',NULL,NULL,NULL,'7');";
    public static final String ins41 = "INSERT INTO `field` VALUES ('22','f0_q2_s2_scheda_A','Tipologia Strutturale','select1','1','0','0',NULL,NULL,NULL,'8');";
    public static final String ins42 = "INSERT INTO `field` VALUES ('23','f0_q3_s2_scheda_A','Larghezza','positive_integer','1','0','0',NULL,NULL,NULL,'9');";
    public static final String ins43 = "INSERT INTO `field` VALUES ('24','f1_q3_s2_scheda_A','Lunghezza','positive_integer','1','0','0',NULL,NULL,NULL,'9');";
    public static final String ins44 = "INSERT INTO `field` VALUES ('25','f0_q4_s2_scheda_A','Numero piani','positive_integer','1','0','0',NULL,NULL,NULL,'10');";
    public static final String ins45 = "INSERT INTO `field` VALUES ('26','f0_q5_s2_scheda_A','Delta Q','measure','1','0','0',NULL,NULL,NULL,'11');";
    public static final String ins46 = "INSERT INTO `field` VALUES ('27','f1_q5_s2_scheda_A','Hg','measure','1','0','0',NULL,NULL,NULL,'11');";
    public static final String ins47 = "INSERT INTO `field` VALUES ('28','f2_q5_s2_scheda_A','H1','measure','1','0','0',NULL,NULL,NULL,'11');";
    public static final String ins48 = "INSERT INTO `field` VALUES ('29','f3_q5_s2_scheda_A','H2','measure','1','0','0',NULL,NULL,NULL,'11');";
    public static final String ins49 = "INSERT INTO `field` VALUES ('30','f0_q0_s3_scheda_A','Inizio','date_time','1','0','0',NULL,NULL,NULL,'12');";
    public static final String ins50 = "INSERT INTO `field` VALUES ('31','f1_q0_s3_scheda_A','Fine','date_time','1','0','0',NULL,NULL,NULL,'12');";
    public static final String ins51 = "INSERT INTO `field` VALUES ('32','f2_q0_s3_scheda_A','Picco altezza acqua','date_time','1','0','0',NULL,NULL,NULL,'12');";
    public static final String ins52 = "INSERT INTO `field` VALUES ('33','f0_q1_s3_scheda_A','Hw','measure','1','0','0',NULL,NULL,NULL,'13');";
    public static final String ins53 = "INSERT INTO `field` VALUES ('34','f0_q2_s3_scheda_A','Tipo sedimenti','select1','1','0','0',NULL,NULL,NULL,'14');";
    public static final String ins54 = "INSERT INTO `field` VALUES ('35','f0_q3_s3_scheda_A','Tipo contaminanti','string','1','0','0',NULL,NULL,NULL,'15');";


    public static final String ins55 = "INSERT INTO `choice` VALUES ('1','via, viale','0','5');";
    public static final String ins56 = "INSERT INTO `choice` VALUES ('2','vicolo','0','5');";
    public static final String ins57 = "INSERT INTO `choice` VALUES ('3','località','0','5');";
    public static final String ins58 = "INSERT INTO `choice` VALUES ('4','corso','0','5');";
    public static final String ins59 = "INSERT INTO `choice` VALUES ('5','piazza, largo','0','5');";
    public static final String ins60 = "INSERT INTO `choice` VALUES ('6','strada','0','5');";
    public static final String ins61 = "INSERT INTO `choice` VALUES ('7','proprietario','0','16');";
    public static final String ins62 = "INSERT INTO `choice` VALUES ('8','affittuario','0','16');";
    public static final String ins63 = "INSERT INTO `choice` VALUES ('9','altro','1','16');";
    public static final String ins64 = "INSERT INTO `choice` VALUES ('10','casa singola','0','17');";
    public static final String ins65 = "INSERT INTO `choice` VALUES ('11','condominio/casa plurifamiliare','0','17');";
    public static final String ins66 = "INSERT INTO `choice` VALUES ('12','edificio pubblico','0','17');";
    public static final String ins67 = "INSERT INTO `choice` VALUES ('13','prima del 1945','0','21');";
    public static final String ins68 = "INSERT INTO `choice` VALUES ('14','dal 1945 al 1991','0','21');";
    public static final String ins69 = "INSERT INTO `choice` VALUES ('15','dal 1991 al 2007','0','21');";
    public static final String ins70 = "INSERT INTO `choice` VALUES ('16','dopo il 2007','0','21');";
    public static final String ins71 = "INSERT INTO `choice` VALUES ('17','interventi negli ultimi 20 anni','0','21');";
    public static final String ins72 = "INSERT INTO `choice` VALUES ('18','muratura','0','22');";
    public static final String ins73 = "INSERT INTO `choice` VALUES ('19','mista (muratura + c.a.)','0','22');";
    public static final String ins74 = "INSERT INTO `choice` VALUES ('20','calcestruzzo armato','0','22');";
    public static final String ins75 = "INSERT INTO `choice` VALUES ('21','acciaio','0','22');";
    public static final String ins76 = "INSERT INTO `choice` VALUES ('22','legno','0','22');";
    public static final String ins77 = "INSERT INTO `choice` VALUES ('23','altro','1','22');";
    public static final String ins78 = "INSERT INTO `choice` VALUES ('24','materiale fine','0','34');";
    public static final String ins79 = "INSERT INTO `choice` VALUES ('25','rifiuti','0','34');";
    public static final String ins80 = "INSERT INTO `choice` VALUES ('26','materiale grossolano','0','34');";
    public static final String ins81 = "INSERT INTO `choice` VALUES ('27','materiale vegetale/ligneo','0','34');";
    public static final String ins82 = "INSERT INTO `choice` VALUES ('28','altro','1','34');";

    //public static final String ins83 = "INSERT INTO `resource` VALUES ('1','descrizione immagine 1','image', 'content://media/external/images/media/19','3');";
    //public static final String ins84 = "INSERT INTO `resource` VALUES ('2','descrizione immagine 2','image', 'content://media/external/images/media/18','3');";


    public static final String insB = "INSERT INTO `form` VALUES ('2','scheda_B','Scheda B: Danni alle unità immobiliari','Scheda che raccoglie i dati dei danni alle singole unità immobiliari, inserire una scheda per unità e una sezione per livello');";

    public static final String insB00 = "INSERT INTO `section` VALUES ('5','s1_fB','Sezione 1: informazioni generali','Informazioni generali sull''unità abitativa','1','0','2');";
    public static final String insB01 = "INSERT INTO `section` VALUES ('6','s2_fB','Sezione 2: Danni livello','Informazioni su uno specifico livello dell''edificio','1','1','2');";

    public static final String insB02 = "INSERT INTO `question` VALUES ('16','q0_s1_scheda_B','Subalterno','Indicare il numero di subalterno. Una foto potrebbe essere utile per la corretta identificazione','1','0','5');";
    public static final String insB03 = "INSERT INTO `question` VALUES ('17','q1_s1_scheda_B','Proprietà','Informazioni sul proprietario dell''unità abitativa','1','1','5');";
    public static final String insB04 = "INSERT INTO `question` VALUES ('18','q2_s1_scheda_B','Numero residenti','Indicare il numero di residenti secondo la classificazione sotto riportata','1','0','5');";
    public static final String insB05 = "INSERT INTO `question` VALUES ('19','q3_s2_scheda_B','Livello e superficie','Indicare il livello considerato e la superficie coperta','1','0','6');";
    public static final String insB06 = "INSERT INTO `question` VALUES ('20','q4_s2_scheda_B','Bocche di lupo','Indicare se sono presenti bocche di lupo','1','0','6');";
    public static final String insB07 = "INSERT INTO `question` VALUES ('21','q5_s2_scheda_B','Stato di conservazione','Indicare lo stato di conservazione del livello','1','0','6');";
    public static final String insB08 = "INSERT INTO `question` VALUES ('22','q6_s2_scheda_B','Destinazione d''uso','Indicare la destinazione d''uso del livello','1','0','6');";
    public static final String insB09 = "INSERT INTO `question` VALUES ('23','q7_s2_scheda_B','Impianti','Indicare quali impianti sono presenti e se sono danneggiati','1','0','6');";
    public static final String insB10 = "INSERT INTO `question` VALUES ('24','q8_s2_scheda_B','Danni a intonaci e rivestimenti interni','Indicare se sono presenti danni a intonaci e rivestimenti interni','1','0','6');";
    public static final String insB11 = "INSERT INTO `question` VALUES ('25','q9_s2_scheda_B','Danni a serramenti','Indicare i danni ai serramenti','1','0','6');";
    public static final String insB12 = "INSERT INTO `question` VALUES ('26','q10_s2_scheda_B','Danni a pavimenti','Indicare i danni ai pavimenti','1','0','6');";
    public static final String insB13 = "INSERT INTO `question` VALUES ('27','q11_s2_scheda_B','Danni dovuti a elevata velocità','Indicare se ci sono danni dovuti ad alta velocità dell''acqua','1','0','6');";
    public static final String insB14 = "INSERT INTO `question` VALUES ('28','q12_s2_scheda_A','Danni ad arredamenti e ad elettrodomestici','Indicare se sono presenti danni agli arredamenti e agli elettrodomestici','1','0','6');";
    public static final String insB15 = "INSERT INTO `question` VALUES ('29','q13_s2_scheda_B','Danni ad automobili e motoveicoli','Indicare se ci sono stati danni a veicoli e motoveicoli','1','0','6');";
    public static final String insB16 = "INSERT INTO `question` VALUES ('30','q14_s2_scheda_B','Altro','Indicare se sono presenti altri danni','1','0','6');";
    public static final String insB17 = "INSERT INTO `question` VALUES ('31','q15_s2_scheda_B','Inagibilità','Indicare se il livello è rimasto inagibile','1','0','6');";
    public static final String insB18 = "INSERT INTO `question` VALUES ('32','q16_s2_scheda_B','Costi di clean-up','Indicare se sono stati sostenuti dei costi di clean-up','1','0','6');";
    public static final String insB19 = "INSERT INTO `question` VALUES ('33','q17_s2_scheda_B','Azioni intraprese','Indicare le azioni intraprese e la modalità','1','1','6');";

    public static final String insB20 = "INSERT INTO `field` VALUES ('36','f0_q0_s1_scheda_B','Numero subalterno','positive_integer','1','0','0',NULL,NULL,NULL,'16');";
    public static final String insB21 = "INSERT INTO `field` VALUES ('37','f0_q1_s1_scheda_B','Nome','string','1','0','0',NULL,NULL,NULL,'17');";
    public static final String insB22 = "INSERT INTO `field` VALUES ('38','f1_q1_s1_scheda_B','Cognome','string','1','0','0',NULL,NULL,NULL,'17');";
    public static final String insB23 = "INSERT INTO `field` VALUES ('39','f2_q1_s1_scheda_B','Recapito','string','1','0','0',NULL,NULL,NULL,'17');";
    public static final String insB24 = "INSERT INTO `field` VALUES ('40','f0_q2_s1_scheda_B','Totali','positive_integer','1','0','0',NULL,NULL,NULL,'18');";
    public static final String insB25 = "INSERT INTO `field` VALUES ('41','f1_q2_s1_scheda_B','Bambini (età sotto i 14 anni)','positive_integer','1','0','0',NULL,NULL,NULL,'18');";
    public static final String insB26 = "INSERT INTO `field` VALUES ('42','f2_q2_s1_scheda_B','Anziani (età superiore a 70 anni','positive_integer','1','0','0',NULL,NULL,NULL,'18');";
    public static final String insB27 = "INSERT INTO `field` VALUES ('43','f3_q2_s1_scheda_B','Portatori di handicap/invalidi','positive_integer','1','0','0',NULL,NULL,NULL,'18');";
    public static final String insB28 = "INSERT INTO `field` VALUES ('44','f0_q3_s2_scheda_B','Livello considerato','integer','1','0','0',NULL,NULL,NULL,'19');";
    public static final String insB29 = "INSERT INTO `field` VALUES ('45','f1_q3_s2_scheda_B','Superficie coperta','measure','1','0','0',NULL,NULL,NULL,'19');";
    public static final String insB30 = "INSERT INTO `field` VALUES ('46','f0_q4_s2_scheda_B','Bocche di lupo','select1','1','0','0',NULL,NULL,NULL,'20');";
    public static final String insB31 = "INSERT INTO `field` VALUES ('47','f0_q5_s2_scheda_B','Stato di conservazione','select1','1','0','0',NULL,NULL,NULL,'21');";
    public static final String insB32 = "INSERT INTO `field` VALUES ('48','f0_q6_s2_scheda_B','Destinazione d''uso','select','1','0','0',NULL,NULL,NULL,'22');";
    public static final String insB33 = "INSERT INTO `field` VALUES ('49','f0_q7_s2_scheda_B','Presenza impianti','select','1','0','0',NULL,NULL,NULL,'23');";
    public static final String insB34 = "INSERT INTO `field` VALUES ('50','f1_q7_s2_scheda_B','Danni impianti','select','1','0','0',NULL,NULL,NULL,'23');";
    public static final String insB35 = "INSERT INTO `field` VALUES ('51','f0_q8_s2_schedaB','Altezza massima allagamento','measure','1','0','0',NULL,NULL,NULL,'24');";
    public static final String insB36 = "INSERT INTO `field` VALUES ('52','f1_q8_s2_scheda_B','Danni intonaci/rivestimenti','select1','1','0','0',NULL,NULL,NULL,'24');";
    public static final String insB37 = "INSERT INTO `field` VALUES ('53','f2_q8_s2_scheda_B','perimetro danneggiato','measure','0','0','0',NULL,NULL,NULL,'24');";
    public static final String insB38 = "INSERT INTO `field` VALUES ('54','f0_q9_s2_scheda_B','danni a serramenti','select1','1','0','0',NULL,NULL,NULL,'25');";
    public static final String insB39 = "INSERT INTO `field` VALUES ('55','f1_q9_s2_scheda_B','numero porte danneggiate','positive_integer','0','0','0',NULL,NULL,NULL,'25');";
    public static final String insB40 = "INSERT INTO `field` VALUES ('56','f2_q9_s2_scheda_B','area porte danneggiate','measure','0','0','0',NULL,NULL,NULL,'25');";
    public static final String insB41 = "INSERT INTO `field` VALUES ('57','f3_q9_s2_scheda_B','numero finestre danneggiate','positive_integer','0','0','0',NULL,NULL,NULL,'25');";
    public static final String insB42 = "INSERT INTO `field` VALUES ('58','f4_q9_s2_scheda_B','area finestre danneggiate','measure','0','0','0',NULL,NULL,NULL,'25');";
    public static final String insB43 = "INSERT INTO `field` VALUES ('59','f0_q10_s2_scheda_B','danni a pavimenti','select1','1','0','0',NULL,NULL,NULL,'26');";
    public static final String insB44 = "INSERT INTO `field` VALUES ('60','f1_q10_s2_scheda_B','superficie danneggiata','measure','0','0','0',NULL,NULL,NULL,'26');";
    public static final String insB45 = "INSERT INTO `field` VALUES ('61','f0_q11_s2_scheda_B','danni da elevata velocità','select1','1','0','0',NULL,NULL,NULL,'27');";
    public static final String insB46 = "INSERT INTO `field` VALUES ('62','f1_q11_s2_scheda_B','specificare','string','0','0','0',NULL,NULL,NULL,'27');";
    public static final String insB47 = "INSERT INTO `field` VALUES ('63','f0_q12_s2_scheda_B','Danni ad arredamenti','select1','1','0','0',NULL,NULL,NULL,'28');";
    public static final String insB48 = "INSERT INTO `field` VALUES ('64','f1_q12_s2_scheda_B','Danni ad elettrodomestici','select1','1','0','0',NULL,NULL,NULL,'28');";
    public static final String insB49 = "INSERT INTO `field` VALUES ('65','f2_q12_s2_scheda_B','specificare','string','0','0','0',NULL,NULL,NULL,'28');";
    public static final String insB50 = "INSERT INTO `field` VALUES ('66','f0_q13_s2_scheda_B','numero auto danneggiate','positive_integer','1','0','0',NULL,NULL,NULL,'29');";
    public static final String insB51 = "INSERT INTO `field` VALUES ('67','f1_q13_s2_scheda_B','numero motoveicoli danneggiati','positive_integer','1','0','0',NULL,NULL,NULL,'29');";
    public static final String insB52 = "INSERT INTO `field` VALUES ('68','f0_q14_s2_Scheda_B','specificare','string','0','0','0',NULL,NULL,NULL,'30');";
    public static final String insB53 = "INSERT INTO `field` VALUES ('69','f0_q15_s2_scheda_B','Inagibilità','select1','1','0','0',NULL,NULL,NULL,'31');";
    public static final String insB54 = "INSERT INTO `field` VALUES ('70','f1_q15_s2_scheda_B','Durata','positive_integer','1','0','0',NULL,NULL,NULL,'31');";
    public static final String insB55 = "INSERT INTO `field` VALUES ('71','f2_q15_s2_scheda_B','Causa','string','1','0','0',NULL,NULL,NULL,'31');";
    public static final String insB56 = "INSERT INTO `field` VALUES ('72','f0_q16_s2_scheda_B','costi clean-up','select1','1','0','0',NULL,NULL,NULL,'32');";
    public static final String insB57 = "INSERT INTO `field` VALUES ('73','f1_q16_s2_scheda_B','tipo intervento','select','1','0','0',NULL,NULL,NULL,'32');";
    public static final String insB58 = "INSERT INTO `field` VALUES ('74','f0_q17_s2_scheda_B','azioni intraprese','select','1','0','0',NULL,NULL,NULL,'33');";
    public static final String insB59 = "INSERT INTO `field` VALUES ('75','f1_q17_s2_scheda_B','tempo dell''azione','date_time','1','0','0',NULL,NULL,NULL,'33');";
    public static final String insB60 = "INSERT INTO `field` VALUES ('76','f2_q17_s2_scheda_B','Motivazione','string','1','0','0',NULL,NULL,NULL,'33');";

    public static final String insB61 = "INSERT INTO `choice` VALUES ('29','No','0','46');";
    public static final String insB62 = "INSERT INTO `choice` VALUES ('30','Si','0','46');";
    public static final String insB63 = "INSERT INTO `choice` VALUES ('31','Ottimo','0','47');";
    public static final String insB64 = "INSERT INTO `choice` VALUES ('32','Normale','0','47');";
    public static final String insB65 = "INSERT INTO `choice` VALUES ('33','Scadente','0','47');";
    public static final String insB66 = "INSERT INTO `choice` VALUES ('34','garage','0','48');";
    public static final String insB67 = "INSERT INTO `choice` VALUES ('35','taverna/cantina','0','48');";
    public static final String insB68 = "INSERT INTO `choice` VALUES ('36','abitazione','0','48');";
    public static final String insB69 = "INSERT INTO `choice` VALUES ('37','magazzino','0','48');";
    public static final String insB70 = "INSERT INTO `choice` VALUES ('38','commerciale','0','48');";
    public static final String insB71 = "INSERT INTO `choice` VALUES ('39','mansarda/soffitta','0','48');";
    public static final String insB72 = "INSERT INTO `choice` VALUES ('40','disuso','0','48');";
    public static final String insB73 = "INSERT INTO `choice` VALUES ('41','in costruzione/ristrutturazione','0','48');";
    public static final String insB74 = "INSERT INTO `choice` VALUES ('42','altro','1','48');";

    public static final String insB75 = "INSERT INTO `choice` VALUES ('43','impianto idraulico e sanitario','0','49');";
    public static final String insB76 = "INSERT INTO `choice` VALUES ('44','impianto elettrico','0','49');";
    public static final String insB77 = "INSERT INTO `choice` VALUES ('45','impianto termico','0','49');";
    public static final String insB78 = "INSERT INTO `choice` VALUES ('46','ascensore','0','49');";
    public static final String insB79 = "INSERT INTO `choice` VALUES ('47','altro','1','49');";

    public static final String insB80 = "INSERT INTO `choice` VALUES ('48','impianto idraulico e sanitario','0','50');";
    public static final String insB81 = "INSERT INTO `choice` VALUES ('49','impianto elettrico','0','50');";
    public static final String insB82 = "INSERT INTO `choice` VALUES ('50','impianto termico','0','50');";
    public static final String insB83 = "INSERT INTO `choice` VALUES ('51','ascensore','0','50');";
    public static final String insB84 = "INSERT INTO `choice` VALUES ('52','altro','1','50');";

    public static final String insB85 = "INSERT INTO `choice` VALUES ('53','Si','0','52');";
    public static final String insB86 = "INSERT INTO `choice` VALUES ('54','No','0','52');";
    public static final String insB87 = "INSERT INTO `choice` VALUES ('55','Si','0','54');";
    public static final String insB88 = "INSERT INTO `choice` VALUES ('56','No','0','54');";
    public static final String insB89 = "INSERT INTO `choice` VALUES ('57','Si','0','59');";
    public static final String insB90 = "INSERT INTO `choice` VALUES ('58','No','0','59');";
    public static final String insB91 = "INSERT INTO `choice` VALUES ('59','Si','0','61');";
    public static final String insB92 = "INSERT INTO `choice` VALUES ('60','No','0','61');";
    public static final String insB93 = "INSERT INTO `choice` VALUES ('61','Si','0','63');";
    public static final String insB94 = "INSERT INTO `choice` VALUES ('62','No','0','63');";
    public static final String insB95 = "INSERT INTO `choice` VALUES ('63','Si','0','64');";
    public static final String insB96 = "INSERT INTO `choice` VALUES ('64','No','0','64');";
    public static final String insB97 = "INSERT INTO `choice` VALUES ('65','Si','0','69');";
    public static final String insB98 = "INSERT INTO `choice` VALUES ('66','No','0','69');";
    public static final String insB99 = "INSERT INTO `choice` VALUES ('67','Si','0','72');";
    public static final String insB100 = "INSERT INTO `choice` VALUES ('68','No','0','72');";

    public static final String insB101 = "INSERT INTO `choice` VALUES ('69','intervento privato','0','73');";
    public static final String insB102 = "INSERT INTO `choice` VALUES ('70','intervento pubblico','0','73');";
    public static final String insB103 = "INSERT INTO `choice` VALUES ('71','Nessuna','0','74');";
    public static final String insB104 = "INSERT INTO `choice` VALUES ('72','Uso di pompe','0','74');";
    public static final String insB105 = "INSERT INTO `choice` VALUES ('73','Uso di paratoie','0','74');";
    public static final String insB106 = "INSERT INTO `choice` VALUES ('74','Spostamento oggetti ai piani alti','0','74');";
    public static final String insB107 = "INSERT INTO `choice` VALUES ('75','Interruzione corrente elettrica','0','74');";
    public static final String insB108 = "INSERT INTO `choice` VALUES ('76','Evacuazione','0','74');";
    public static final String insB109 = "INSERT INTO `choice` VALUES ('77','Altro','1','74');";
*/

    public DatabaseOpenHelper(Context context) {
        super(context, dbName, null, dbVersion);

    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(dbCreat2);
        db.execSQL(dbCreat3);
        db.execSQL(dbCreat4);
        db.execSQL(dbCreat5);
        db.execSQL(dbCreat6);
        db.execSQL(dbCreat7);
        db.execSQL(dbCreat8);
        db.execSQL(dbCreat9);
        db.execSQL(dbCreat10);
        db.execSQL(dbCreat11);
        db.execSQL(dbCreat12);
        db.execSQL(dbCreat13);
        db.execSQL(dbCreat14);
        db.execSQL(dbCreat15);
        db.execSQL(dbCreat16);
        db.execSQL(dbCreat17);

/*
        db.execSQL(ins0);
        db.execSQL(ins1);
        db.execSQL(ins2);
        db.execSQL(ins3);
        db.execSQL(ins4);
        db.execSQL(ins5);
        db.execSQL(ins6);
        db.execSQL(ins7);
        db.execSQL(ins8);
        db.execSQL(ins9);

        db.execSQL(ins10);
        db.execSQL(ins11);
        db.execSQL(ins12);
        db.execSQL(ins13);
        db.execSQL(ins14);
        db.execSQL(ins15);
        db.execSQL(ins16);
        db.execSQL(ins17);
        db.execSQL(ins18);
        db.execSQL(ins19);

        db.execSQL(ins20);
        db.execSQL(ins21);
        db.execSQL(ins22);
        db.execSQL(ins23);
        db.execSQL(ins24);
        db.execSQL(ins25);
        db.execSQL(ins26);
        db.execSQL(ins27);
        db.execSQL(ins28);
        db.execSQL(ins29);

        db.execSQL(ins30);
        db.execSQL(ins31);
        db.execSQL(ins32);
        db.execSQL(ins33);
        db.execSQL(ins34);
        db.execSQL(ins35);
        db.execSQL(ins36);
        db.execSQL(ins37);
        db.execSQL(ins38);
        db.execSQL(ins39);

        db.execSQL(ins40);
        db.execSQL(ins41);
        db.execSQL(ins42);
        db.execSQL(ins43);
        db.execSQL(ins44);
        db.execSQL(ins45);
        db.execSQL(ins46);
        db.execSQL(ins47);
        db.execSQL(ins48);
        db.execSQL(ins49);

        db.execSQL(ins50);
        db.execSQL(ins51);
        db.execSQL(ins52);
        db.execSQL(ins53);
        db.execSQL(ins54);
        db.execSQL(ins55);
        db.execSQL(ins56);
        db.execSQL(ins57);
        db.execSQL(ins58);
        db.execSQL(ins59);

        db.execSQL(ins60);
        db.execSQL(ins61);
        db.execSQL(ins62);
        db.execSQL(ins63);
        db.execSQL(ins64);
        db.execSQL(ins65);
        db.execSQL(ins66);
        db.execSQL(ins67);
        db.execSQL(ins68);
        db.execSQL(ins69);

        db.execSQL(ins70);
        db.execSQL(ins71);
        db.execSQL(ins72);
        db.execSQL(ins73);
        db.execSQL(ins74);
        db.execSQL(ins75);
        db.execSQL(ins76);
        db.execSQL(ins77);
        db.execSQL(ins78);
        db.execSQL(ins79);

        db.execSQL(ins80);
        db.execSQL(ins81);
        db.execSQL(ins82);

        //form B sql
        db.execSQL(insB);

        db.execSQL(insB00);
        db.execSQL(insB01);
        db.execSQL(insB02);
        db.execSQL(insB03);
        db.execSQL(insB04);
        db.execSQL(insB05);
        db.execSQL(insB06);
        db.execSQL(insB07);
        db.execSQL(insB08);
        db.execSQL(insB09);

        db.execSQL(insB10);
        db.execSQL(insB11);
        db.execSQL(insB12);
        db.execSQL(insB13);
        db.execSQL(insB14);
        db.execSQL(insB15);
        db.execSQL(insB16);
        db.execSQL(insB17);
        db.execSQL(insB18);
        db.execSQL(insB19);

        db.execSQL(insB20);
        db.execSQL(insB21);
        db.execSQL(insB22);
        db.execSQL(insB23);
        db.execSQL(insB24);
        db.execSQL(insB25);
        db.execSQL(insB26);
        db.execSQL(insB27);
        db.execSQL(insB28);
        db.execSQL(insB29);

        db.execSQL(insB30);
        db.execSQL(insB31);
        db.execSQL(insB32);
        db.execSQL(insB33);
        db.execSQL(insB34);
        db.execSQL(insB35);
        db.execSQL(insB36);
        db.execSQL(insB37);
        db.execSQL(insB38);
        db.execSQL(insB39);

        db.execSQL(insB40);
        db.execSQL(insB41);
        db.execSQL(insB42);
        db.execSQL(insB43);
        db.execSQL(insB44);
        db.execSQL(insB45);
        db.execSQL(insB46);
        db.execSQL(insB47);
        db.execSQL(insB48);
        db.execSQL(insB49);

        db.execSQL(insB50);
        db.execSQL(insB51);
        db.execSQL(insB52);
        db.execSQL(insB53);
        db.execSQL(insB54);
        db.execSQL(insB55);
        db.execSQL(insB56);
        db.execSQL(insB57);
        db.execSQL(insB58);
        db.execSQL(insB59);

        db.execSQL(insB60);
        db.execSQL(insB61);
        db.execSQL(insB62);
        db.execSQL(insB63);
        db.execSQL(insB64);
        db.execSQL(insB65);
        db.execSQL(insB66);
        db.execSQL(insB67);
        db.execSQL(insB68);
        db.execSQL(insB69);

        db.execSQL(insB70);
        db.execSQL(insB71);
        db.execSQL(insB72);
        db.execSQL(insB73);
        db.execSQL(insB74);
        db.execSQL(insB75);
        db.execSQL(insB76);
        db.execSQL(insB77);
        db.execSQL(insB78);
        db.execSQL(insB79);

        db.execSQL(insB80);
        db.execSQL(insB81);
        db.execSQL(insB82);
        db.execSQL(insB83);
        db.execSQL(insB84);
        db.execSQL(insB85);
        db.execSQL(insB86);
        db.execSQL(insB87);
        db.execSQL(insB88);
        db.execSQL(insB89);

        db.execSQL(insB90);
        db.execSQL(insB91);
        db.execSQL(insB92);
        db.execSQL(insB93);
        db.execSQL(insB94);
        db.execSQL(insB95);
        db.execSQL(insB96);
        db.execSQL(insB97);
        db.execSQL(insB98);
        db.execSQL(insB99);

        db.execSQL(insB100);
        db.execSQL(insB101);
        db.execSQL(insB102);
        db.execSQL(insB103);
        db.execSQL(insB104);
        db.execSQL(insB105);
        db.execSQL(insB106);
        db.execSQL(insB107);
        db.execSQL(insB108);
        db.execSQL(insB109);

        db.execSQL(ins);
        db.execSQL(insModule);
        db.execSQL(insModule2);

        db.execSQL(insT1);
        db.execSQL(insModuleT11);
        db.execSQL(insModuleT12);

        db.execSQL(insT2);
        db.execSQL(insModuleT21);
        db.execSQL(insModuleT22);
        //db.execSQL(ins83);
        //db.execSQL(ins84);
*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + AnswerColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ChoiceColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + FieldColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + FormColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + FormHierarchyColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + FormInstanceColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + NoteColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + QuestionColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + QuestionInstanceColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ResourceColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ResourceInstanceColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SectionColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SectionInstanceColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TaskColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TaskModuleColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SettingsColumns.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int newVersion, int oldVersion){
        onUpgrade(db, oldVersion, newVersion);

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys = ON;");
        super.onOpen(db);
    }
}
