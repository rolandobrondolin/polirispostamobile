package it.polirisposta.mobile.poliRISPOSTA.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Form implements Serializable {

    private int id;
    private String name;
    private String textName;
    private String description;
    private List<Section> sections;

    public Form(int id, String name, String textName, String description) {
        this.id = id;
        this.name = name;
        this.textName = textName;
        this.description = description;
        this.sections = new ArrayList<Section>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void addSection(Section section){
        this.sections.add(section);
    }

    public List<List<String>> getQuestionTextNames(){
        List<List<String>> qNames = new ArrayList<List<String>>();
        for (Section s : sections){
            qNames.add(s.getQuestionTextNames());
        }
        return qNames;
    }
    public List<String> getSectionTextNames(){
        ArrayList<String> names = new ArrayList<String>();
        for(Section s: sections){
            names.add(s.getTextName());
        }
        return names;
    }

    public Section getSectionById(int id){
        return sections.get(id);
    }

    public Question getQuestionById(int sectionId, int questionId){
        return sections.get(sectionId).getQuestionById(questionId);
    }

    public int sectionsSize(){
        return sections.size();
    }

    public int questionsSize(int sectionId){
        return sections.get(sectionId).size();
    }

    public String getTextName() {
        return textName;
    }

}
