package it.polirisposta.mobile.poliRISPOSTA.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Rolando on 15/03/15.
 */
public class FormInstance implements Serializable{
    private int id;
    //TODO: Task?
    private Form form;
    private String timestamp;
    private String deviceId;
    private int taskId;
    private List<SectionInstance> sectionInstances;
    private boolean finalized;
    private int totalFields;
    private int mTotalFields;
    private int filled;
    private int notFilled;
    private int notApplicable;
    private int mFilled;
    private int mNotFilled;
    private int mNotApplicable;

    public FormInstance(int id, Form form, String timestamp, String deviceId, int taskId, boolean finalized) {
        this.id = id;
        this.form = form;
        this.timestamp = timestamp;
        this.deviceId = deviceId;
        sectionInstances = new ArrayList<SectionInstance>();
        this.finalized = finalized;
        this.taskId = taskId;
        totalFields=0;
        mTotalFields=0;
        filled=0;
        notFilled=0;
        notApplicable=0;
        mFilled=0;
        mNotFilled=0;
        mNotApplicable=0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<SectionInstance> getSections() {
        return sectionInstances;
    }

    public void addSectionInstance(SectionInstance sect){
        this.sectionInstances.add(sect);
    }

    public List<String> getSectionNames(){
        List<String> names = new ArrayList<String>();
        for(SectionInstance s: sectionInstances){
            if(s.getIndex()>1){
                names.add(s.getSection().getTextName() + " (" + s.getIndex() + ")");
            }
            else{
                names.add(s.getSection().getTextName());
            }
        }
        return names;
    }

    public List<List<String>> getQuestionNames(){
        List<List<String>> qNames = new ArrayList<List<String>>();
        for(SectionInstance s: sectionInstances){
            qNames.add(s.getTextNames());
        }
        return qNames;
    }

    public QuestionInstance getQuestionInstance(int section, int question){
        return sectionInstances.get(section).getQuestionInstance(question);
    }

    public boolean isFinalized() {
        return finalized;
    }

    public void setFinalized(boolean finalized) {
        this.finalized = finalized;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getTotalFields() {
        return totalFields;
    }

    public void setTotalFields(int totalFields) {
        this.totalFields = totalFields;
    }

    public int getFilled() {
        return filled;
    }

    public void setFilled(int filled) {
        this.filled = filled;
    }

    public int getNotFilled() {
        return notFilled;
    }

    public void setNotFilled(int notFilled) {
        this.notFilled = notFilled;
    }

    public int getNotApplicable() {
        return notApplicable;
    }

    public void setNotApplicable(int notApplicable) {
        this.notApplicable = notApplicable;
    }

    public int getmFilled() {
        return mFilled;
    }

    public void setmFilled(int mFilled) {
        this.mFilled = mFilled;
    }

    public int getmNotFilled() {
        return mNotFilled;
    }

    public void setmNotFilled(int mNotFilled) {
        this.mNotFilled = mNotFilled;
    }

    public int getmNotApplicable() {
        return mNotApplicable;
    }

    public void setmNotApplicable(int mNotApplicable) {
        this.mNotApplicable = mNotApplicable;
    }

    public int getmTotalFields() {
        return mTotalFields;
    }

    public void setmTotalFields(int mTotalFields) {
        this.mTotalFields = mTotalFields;
    }

    public int findSectionFromPK(int pk){
        int i=0;
        for (SectionInstance s: sectionInstances){
            if(s.getId()==pk){
                return i;
            }
            i++;
        }
        return 0;
    }

    public int findQuestionFromPK(int pk){
        for (SectionInstance s: sectionInstances){
            int i = 0;
            for(QuestionInstance q: s.getQuestionInstances()){
                if(q.getId()==pk){
                    return i;
                }
                i++;
            }
        }
        return 0;
    }

    public int findSectionFromQuestionPK(int pk){
        int i = 0;
        for (SectionInstance s: sectionInstances){
            for(QuestionInstance q: s.getQuestionInstances()){
                if(q.getId()==pk){
                    return i;
                }
            }
            i++;
        }
        return 0;
    }

}
