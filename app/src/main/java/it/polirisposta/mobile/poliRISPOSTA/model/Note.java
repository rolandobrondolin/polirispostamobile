package it.polirisposta.mobile.poliRISPOSTA.model;

import java.io.Serializable;

/**
 * Created by rolando on 19/05/15.
 */
public class Note implements Serializable{
    String note;
    int id;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Note(int id, String note) {
        this.note = note;
        this.id = id;
    }
}
