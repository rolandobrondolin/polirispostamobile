package it.polirisposta.mobile.poliRISPOSTA.model;

import android.provider.BaseColumns;

/**
 * Created by Rolando on 26/12/14.
 */
public final class PoliRispostaDbContract {

    //missing details on tasks and on form instance missing information on group and device
    //missing also bridge table


    public PoliRispostaDbContract() {
    }


    public static abstract class SectionInstanceColumns implements BaseColumns{
        public static final String TABLE_NAME = "section_instance";
        public static final String _ID = "ID";
        public static final String INDEX = "s_index";
        public static final String FORM_INSTANCE_ID = "form_instance_id";
        public static final String SECTION_ID = "section_id";
    }

    public static abstract class SectionColumns implements BaseColumns{
        public static final String TABLE_NAME = "section";
        public static final String _ID = "ID";
        public static final String NAME = "name";
        public static final String TEXT_NAME = "text_name";
        public static final String DESCRIPTION = "description";
        public static final String REQUIRED = "required";
        public static final String REPEAT = "repeat";
        public static final String FORM_ID = "form_id";
    }

    public static abstract class ResourceInstanceColumns implements BaseColumns{
        public static final String TABLE_NAME = "resource_instance";
        public static final String _ID = "ID";
        public static final String TYPE = "type";
        public static final String PATH = "path";
        public static final String DESCRIPTION = "description";
        public static final String QUESTION_INSTANCE_ID = "question_instance_id";
    }

    public static abstract class ResourceColumns implements BaseColumns{
        public static final String TABLE_NAME = "resource";
        public static final String _ID = "ID";
        public static final String DESCRIPTION = "description";
        public static final String TYPE = "type";
        public static final String PATH = "path";
        public static final String QUESTION_ID = "question_id";
    }

    public static abstract class QuestionInstanceColumns implements BaseColumns{
        public static final String TABLE_NAME = "question_instance";
        public static final String _ID = "ID";
        public static final String INDEX = "q_index";
        public static final String SECTION_INSTANCE_ID = "section_instance_id";
        public static final String QUESTION_ID = "question_id";
    }

    public static abstract class QuestionColumns implements BaseColumns{
        public static final String TABLE_NAME = "question";
        public static final String _ID = "ID";
        public static final String NAME = "name";
        public static final String TEXT_NAME = "text_name";
        public static final String DESCRIPTION = "description";
        public static final String REQUIRED = "required";
        public static final String REPEAT = "repeat";
        public static final String SECTION_ID = "section_id";
    }

    public static abstract class NoteColumns implements BaseColumns{
        public static final String TABLE_NAME = "note";
        public static final String _ID = "ID";
        public static final String TEXT = "text";
        public static final String QUESTION_INSTANCE_ID = "question_instance_id";
    }

    public static abstract class FormInstanceColumns implements BaseColumns{
        public static final String TABLE_NAME = "form_instance";
        public static final String _ID = "ID";
        public static final String TASK_ID = "task_id";
        public static final String FORM_ID = "form_id";
        public static final String TIMESTAMP = "timestamp";
        public static final String DEVICE_ID = "device_id";
        public static final String FINALIZED = "finalized";
        public static final String TOTAL = "total_fields";
        public static final String M_TOTAL = "total_mandatory_fields";
        public static final String FILLED = "filled_fields";
        public static final String NOT_FILLED = "not_filled_fields";
        public static final String NOT_APPLICABLE = "not_applicable_fields";
        public static final String M_FILLED = "mandatory_filled_fields";
        public static final String M_NOT_FILLED = "mandatory_not_filled_fields";
        public static final String M_NOT_APPLICABLE = "mandatory_not_applicable_fields";
    }

    public static abstract class FormHierarchyColumns implements BaseColumns{
        public static final String TABLE_NAME = "form_hierarchy";
        //public static final String _ID = "ID";
        public static final String PARENT_FORM = "parent_form";
        public static final String CHILD_FORM = "child_form";
    }

    public static abstract class FormColumns implements BaseColumns{
        public static final String TABLE_NAME = "form";
        public static final String _ID = "ID";
        public static final String NAME = "name";
        public static final String TEXT_NAME = "text_name";
        public static final String DESCRIPTION = "description";
    }

    public static abstract class FieldColumns implements BaseColumns{
        public static final String TABLE_NAME = "field";
        public static final String _ID = "ID";
        public static final String NAME = "name";
        public static final String TEXT_NAME = "text_name";
        public static final String TYPE = "type";
        public static final String REQUIRED = "required";
        public static final String REPEAT = "repeat";
        public static final String COND = "cond";
        public static final String COND_STATEMENT = "cond_statement";
        public static final String OP1 = "op1";
        public static final String OP2 = "op2";
        public static final String QUESTION_ID = "question_id";
    }

    public static abstract class ChoiceColumns implements BaseColumns{
        public static final String TABLE_NAME = "choice";
        public static final String _ID = "ID";
        public static final String TEXT = "text";
        public static final String OTHER = "other";
        public static final String FIELD_ID = "field_id";
    }

    public static abstract class AnswerColumns implements BaseColumns{
        public static final String TABLE_NAME = "answer";
        public static final String _ID = "ID";
        public static final String PREFILLED = "prefilled";
        public static final String APPLICABLE = "applicable";
        public static final String TYPE = "type";
        public static final String FILLED = "filled";
        public static final String ANSWER_STRING = "answer_string";
        public static final String ANSWER_INTEGER = "answer_integer";
        public static final String ANSWER_FLOAT = "answer_float";
        public static final String FIELD_ID = "field_id";
        public static final String QUESTION_INSTANCE_ID = "question_instance_id";

    }

    public static abstract class TaskColumns implements BaseColumns{
        public static final String TABLE_NAME = "task";
        public static final String _ID = "ID";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String GROUP_ID = "group_id";
        public static final String GENERATED = "generated";
        public static final String USER_DEFINED = "user_defined";
        public static final String SENT = "sent";

    }

    public static abstract class TaskModuleColumns implements BaseColumns{
        public static final String TABLE_NAME = "task_module";
        public static final String _ID = "ID";
        public static final String TASK_ID = "task_id";
        public static final String FORM_ID = "form_id";
        public static final String N_INSTANCES = "n_instances";

    }

    public static abstract class SettingsColumns implements BaseColumns{
        public static final String TABLE_NAME = "settings";
        public static final String _ID = "ID";
        public static final String GROUP_ID = "group_id";
        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
        public static final String ADDRESS = "address";
        public static final String SESSION_NAME = "session_name";
        public static final String SESSION_ID = "session_id";
        public static final String CSRFTOKEN_NAME = "csrftoken_name";
        public static final String CSRFTOKEN = "csrftoken_id";


    }

    //we start setting conditions on constant values!! (e.g. x>5)

}
