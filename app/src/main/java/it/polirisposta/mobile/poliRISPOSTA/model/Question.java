package it.polirisposta.mobile.poliRISPOSTA.model;

import android.net.Uri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Question implements Serializable {

    private int id;
    private String name;
    private String textName;
    private String description;
    private boolean required;
    private boolean repeat;
    private List<Field> fields;
    private List<Resource> pictures;

    public Question(int id, String name, String textName, String description, boolean required, boolean repeat) {
        this.id = id;
        this.name = name;
        this.textName = textName;
        this.description = description;
        this.required = required;
        this.repeat = repeat;
        this.fields= new ArrayList<Field>();
        this.pictures = new ArrayList<Resource>();
    }

    public int getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isRequired() {
        return required;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void addField(Field field){
        fields.add(field);
    }

    public Field getField(int id){
        Field res = this.fields.get(Integer.valueOf(id));
        return res;
    }
/*
    public List<Integer> getPointers(){
        ArrayList<Integer> res = new ArrayList<Integer>();
        for (Integer i : pointers){
            res.add(i);
        }
        return res;
    }
*/
    public String getTextName() {
        return textName;
    }

    public int fieldSize(){
        return fields.size();
    }

    public void addPicture(Resource res){
        this.pictures.add(res);
    }
    public List<Resource> getPictures(){
        return pictures;
    }
}
