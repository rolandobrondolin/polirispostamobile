package it.polirisposta.mobile.poliRISPOSTA.model;

import android.net.Uri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rolando on 15/03/15.
 */
public class QuestionInstance implements Serializable {
    private int id;
    private int index;
    private Question question;
    private List<Answer> answers;
    private List<Resource> pictures;
    private List<Note> notes;

    public QuestionInstance(int id, int index, Question question) {
        this.id = id;
        this.index = index;
        this.question = question;
        answers = new ArrayList<Answer>();
        pictures = new ArrayList<Resource>();
        notes = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public void addAnswer(Answer ans){
        this.answers.add(ans);
    }

    public Answer getAnswer(int i){
        return answers.get(i);
    }

    public List<Resource> getPictures() {
        return pictures;
    }

    public void setPictures(List<Resource> pictures) {
        this.pictures = pictures;
    }

    public void addPicture (Resource res){
        this.pictures.add(res);
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public void addNote(Note note){
        this.notes.add(note);
    }

}
