package it.polirisposta.mobile.poliRISPOSTA.model;

import android.net.Uri;

import java.io.Serializable;

import it.polirisposta.mobile.poliRISPOSTA.model.datatypes.ResourceType;

/**
 * Created by Rolando on 18/04/15.
 */
public class Resource implements Serializable {
    private String uri;
    private String description;
    private ResourceType type;
    private int id;

    public Resource(Uri uri, String description, ResourceType type, int id) {
        this.uri = uri.toString();
        this.description = description;
        this.type = type;
        this.id = id;
    }

    public Uri getUri() {
        return Uri.parse(uri);
    }

    public void setUri(Uri uri) {
        this.uri = uri.toString();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ResourceType getType() {
        return type;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
