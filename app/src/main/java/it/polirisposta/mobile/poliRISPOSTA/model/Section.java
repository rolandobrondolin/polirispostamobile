package it.polirisposta.mobile.poliRISPOSTA.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Section implements Serializable {

    private List<Question> questions;
    private int id;
    private String name;
    private String description;
    private String textName;

    private boolean required;
    private boolean repeat;



    public Section(int id, String name, String textName, String description, boolean required, boolean repeat){
        questions = new ArrayList<Question>();
        this.id=id;
        this.name=name;
        this.textName = textName;
        this.description=description;
        this.required=required;
        this.repeat=repeat;

    }

    public void addQuestion(Question q){
        questions.add(q);
    }

    public void setTextName(String textName) {
        this.textName = textName;
    }

    public String getTextName() {
        return textName;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isRequired() {
        return required;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public int getId() {
        return id;
    }

    public List<String> getQuestionTextNames(){
        ArrayList<String> names = new ArrayList<String>();
        for(Question q: questions){
            names.add(q.getTextName());
        }
        return names;
    }

    public List<Question> getQuestions(){
        return questions;
    }

    public Question getQuestionById(int id){
        return questions.get(Integer.valueOf(id));
    }

    public int size(){
        return questions.size();
    }


}
