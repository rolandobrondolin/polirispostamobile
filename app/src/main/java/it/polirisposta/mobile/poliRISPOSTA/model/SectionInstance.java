package it.polirisposta.mobile.poliRISPOSTA.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rolando on 15/03/15.
 */
public class SectionInstance implements Serializable {
    private int id;
    private int index;
    private Section section;
    private List<QuestionInstance> questionInstances;

    public SectionInstance(int id, int index, Section section) {
        this.id = id;
        this.index = index;
        this.section = section;
        this.questionInstances = new ArrayList<QuestionInstance>();
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<QuestionInstance> getQuestionInstances() {
        return questionInstances;
    }

    public void setQuestionInstances(List<QuestionInstance> questionInstances) {
        this.questionInstances = questionInstances;
    }

    public void addQuestionInstance(QuestionInstance quest){
        this.questionInstances.add(quest);
    }

    public List<String> getTextNames(){
        List<String> names = new ArrayList<String>();
        for(QuestionInstance q: questionInstances){
            if(q.getIndex()>1){
                names.add(q.getQuestion().getTextName() + " (" + q.getIndex() + ")");
            }
            else{
                names.add(q.getQuestion().getTextName());
            }
        }
        return names;
    }

    public QuestionInstance getQuestionInstance(int i){
        return questionInstances.get(i);
    }

}
