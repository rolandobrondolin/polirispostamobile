package it.polirisposta.mobile.poliRISPOSTA.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rolando on 28/04/15.
 */
public class Task implements Serializable {
    private ArrayList<FormInstance> instances;
    private ArrayList<Form> origins;
    private HashMap<Integer, Integer> cardinalities;
    private HashMap<Integer, Integer> partials;
    private int id;
    private String name;
    private String description;
    //TODO: group info?
    boolean generated;
    boolean userDefined;
    boolean sent;

    public Task(int id, String name, String description, boolean generated, boolean userDefined, boolean sent) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.generated = generated;
        this.instances = new ArrayList<>();
        this.cardinalities = new HashMap<>();
        this.origins = new ArrayList<>();
        this.partials = new HashMap<>();
        this.userDefined = userDefined;
        this.sent = sent;
    }

    public ArrayList<FormInstance> getInstances() {
        return instances;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isGenerated() {
        return generated;
    }

    public void setGenerated(boolean generated) {
        this.generated = generated;
    }

    public void addInstance(FormInstance instance){
        try{

            int formID = instance.getForm().getId();
            int maxCardinality = cardinalities.get(formID);
            if(getFormFromDBPK(formID)==null){
                return;
            }
            if(partials.get(formID)==null){
                partials.put(formID, 0);
            }
            if(partials.get(formID) + 1 <= maxCardinality || maxCardinality == 0){
                partials.put(formID, partials.get(formID) + 1);
                this.instances.add(instance);
            }
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }

    }

    public ArrayList<Form> getOrigins() {
        return origins;
    }

    public void addOrigin(Form form, int cardinality){
        this.origins.add(form);
        this.cardinalities.put(form.getId(), cardinality);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HashMap<Integer, Integer> getCardinalities() {
        return cardinalities;
    }

    public Form getFormFromDBPK(int pk){
        for (Form f: origins){
            if(f.getId()==pk){
                return f;
            }
        }
        return null;
    }

    public void invalidateInstances(){
        instances = new ArrayList<>();
        partials = new HashMap<>();
    }

    public boolean checkPartials(int formPKID){
        if(getFormFromDBPK(formPKID)==null){
            return false;
        }
        if(partials.get(formPKID)==null){
            partials.put(formPKID, 0);
        }
        if(partials.get(formPKID) < cardinalities.get(formPKID) || cardinalities.get(formPKID) == 0){
            return true;
        }
        return false;
    }

    public boolean isUserDefined() {
        return userDefined;
    }

    public void setUserDefined(boolean userDefined) {
        this.userDefined = userDefined;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public FormInstance removeInstance(int instanceID){
        if(instances.get(instanceID)==null){
            return null;
        }
        FormInstance instance = instances.remove(instanceID);
        int formFKID = instance.getForm().getId();
        partials.put(formFKID, partials.get(formFKID)-1);
        return instance;
    }
}
