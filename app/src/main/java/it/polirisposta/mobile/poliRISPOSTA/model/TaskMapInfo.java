package it.polirisposta.mobile.poliRISPOSTA.model;

/**
 * Created by Rolando on 02/05/15.
 */
public class TaskMapInfo {
    int taskDBID;
    String taskName;
    String formName;
    String lat;
    String lon;

    public TaskMapInfo(int taskDBID, String taskName, String formName, String lat, String lon) {
        this.taskDBID = taskDBID;
        this.taskName = taskName;
        this.formName = formName;
        this.lat = lat;
        this.lon = lon;
    }

    public int getTaskDBID() {
        return taskDBID;
    }

    public void setTaskDBID(int taskDBID) {
        this.taskDBID = taskDBID;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }
}
