package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;


public enum DataType {
    STRING ("string"),
    INTEGER ("integer"),
    POSITIVE_INTEGER ("positive_integer"),
    NEGATIVE_INTEGER ("negative_integer"),
    FLOAT ("float"),
    POSITIVE_FLOAT ("positive_float"),
    NEGATIVE_FLOAT ("negative_float"),
    MEASURE ("measure"),
    ISTAT_CODE ("istat_code"),
    GPS ("gps"),
    DATE ("date"),
    DATE_TIME ("date_time"),
    SELECT ("select"),
    SELECT1 ("select1"),
    SKETCH ("sketch");

    private final String type;

     DataType(String value) {
         type = value;
    }

    public boolean equalsType(String type){
        return (type==null)? false: this.type.equals(type);
    }

    public DataType getDataTypeFromString (String type){
        switch (type){
            case "string":
                return STRING;
            case "integer":
                return INTEGER;
            case "positive_integer":
                return POSITIVE_INTEGER;
            case "negative_integer":
                return NEGATIVE_INTEGER;
            case "float":
                return FLOAT;
            case "positive_float":
                return POSITIVE_FLOAT;
            case "negative_float":
                return NEGATIVE_FLOAT;
            case "measure":
                return MEASURE;
            case "istat_code":
                return ISTAT_CODE;
            case "gps":
                return GPS;
            case "date":
                return DATE;
            case "date_time":
                return DATE_TIME;
            case "select":
                return SELECT;
            case "select1":
                return SELECT1;
            case "sketch":
                return  SKETCH;
            default:
                return STRING;
        }
    }

    public String toString(){
        return type;
    }
}
