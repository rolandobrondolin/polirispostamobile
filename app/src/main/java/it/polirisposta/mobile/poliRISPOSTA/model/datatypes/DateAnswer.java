package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 07/04/15.
 */
public class DateAnswer extends Answer implements Serializable {
    private Date answer;

    public DateAnswer(){
        super();
        this.answer = new Date();
        this.setType(DataType.DATE);

    }

    public DateAnswer(Date ans){
        super();
        this.answer = new Date(ans.getTime());
        this.setType(DataType.DATE);
    }

    public DateAnswer(int id, boolean prefilled, boolean applicable, boolean filled, boolean time, Date ans, Field f){
        super(id, prefilled, applicable, DataType.DATE, filled, f);
        if(time){
            setType(DataType.DATE_TIME);
        }
        this.answer = new Date(ans.getTime());
    }

    public void setAnswer(Date ans){
        this.answer = new Date(ans.getTime());
    }

    public Date getAnswer(){
        return new Date(answer.getTime());
    }

    @Override
    public void resetAnswer() {
        this.setFilled(false);
        this.setAnswer(new Date());
        this.setApplicable(true);
    }

}
