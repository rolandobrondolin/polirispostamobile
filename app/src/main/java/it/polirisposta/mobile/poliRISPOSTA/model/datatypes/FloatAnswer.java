package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import java.io.Serializable;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 20/03/15.
 */
public class FloatAnswer extends Answer implements Serializable {
    private float answer;

    public FloatAnswer(){
        super();
        this.answer = 0;
        this.setType(DataType.FLOAT);
    }

    public FloatAnswer(float ans){
        super();
        this.answer = ans;
        this.setType(DataType.FLOAT);
    }

    public FloatAnswer(int id, boolean prefilled, boolean applicable, boolean filled, float ans, Field f){
        super(id, prefilled, applicable, DataType.FLOAT, filled, f);
        this.answer = ans;
    }

    public float getAnswer(){
        return answer;
    }

    public void setAnswer(float ans){
        this.answer = ans;
    }

    @Override
    public void resetAnswer() {
        this.setFilled(false);
        this.setAnswer(0);
        this.setApplicable(true);
    }
    

}
