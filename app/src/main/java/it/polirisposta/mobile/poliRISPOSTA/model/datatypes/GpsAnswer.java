package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import android.util.Log;

import java.io.Serializable;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 29/12/14.
 */
public class GpsAnswer extends Answer implements Serializable {
    private String lon;
    private String lat;
    private String prec;


    public GpsAnswer(){
        super();
        this.lat = new String();
        this.lon = new String();
        this.prec = new String();
        this.setType(DataType.GPS);
    }

    public GpsAnswer(String ans){
        super();
        gpsParser(ans);
        this.setType(DataType.GPS);
    }

    public GpsAnswer(int id, boolean prefilled, boolean applicable, boolean filled, String ans, Field f){
        super(id, prefilled, applicable, DataType.GPS,filled, f);
        gpsParser(ans);
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getPrec() {
        return prec;
    }

    public void setPrec(String prec) {
        this.prec = prec;
    }

    @Override
    public void resetAnswer() {
        this.setFilled(false);
        this.lat="0";
        this.lon="0";
        this.prec="0";
        this.setApplicable(true);
    }

    private void gpsParser(String text){
        String vect[] = text.split(";");
        if(vect.length==3){
            lat = vect[0];
            lon = vect[1];
            prec = vect[2];
        }
        else{
            lat="0";
            lon="0";
            prec="0";
        }

    }

    public String gpsConcat(){
        return lat + ";" + lon +";" + prec;
    }

    public void setGpsFromConcatSpace(String str){
        String vect[] = str.split(" ");
        if(vect.length==3){
            lat = vect[0];
            lon = vect[1];
            prec = vect[2];
        }
        else{
            lat="0";
            lon="0";
            prec="0";
        }
    }


}
