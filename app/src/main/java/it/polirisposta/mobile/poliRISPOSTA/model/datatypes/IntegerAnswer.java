package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import java.io.Serializable;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 29/12/14.
 */
public class IntegerAnswer extends Answer implements Serializable {
    private int answer;

    public IntegerAnswer(){
        super();
        this.answer = 0;
        this.setType(DataType.INTEGER);
    }

    public IntegerAnswer(int ans){
        super();
        this.answer = ans;
        this.setType(DataType.INTEGER);
    }

    public IntegerAnswer(int id, boolean prefilled, boolean applicable, boolean filled, int ans, Field f){
        super(id, prefilled, applicable, DataType.INTEGER,filled, f);
        this.answer = ans;
    }

    public int getAnswer(){
        return answer;
    }

    public void setAnswer(int ans){
        this.answer = ans;
    }

    @Override
    public void resetAnswer() {
        this.setFilled(false);
        this.setAnswer(0);
        this.setApplicable(true);
    }

}
