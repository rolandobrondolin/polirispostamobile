package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import java.io.Serializable;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 02/04/15.
 */
public class IstatAnswer extends Answer implements Serializable {
    private String answer;


    public IstatAnswer(){
        super();
        this.answer = new String();
        this.setType(DataType.ISTAT_CODE);
    }

    public IstatAnswer(String ans){
        super();
        this.answer = new String(ans);
        this.setType(DataType.ISTAT_CODE);
    }

    public IstatAnswer(int id, boolean prefilled, boolean applicable, boolean filled, String ans, Field f){
        super(id, prefilled, applicable, DataType.ISTAT_CODE,filled, f);
        this.answer = new String(ans);
    }

    public void setAnswer(String ans){
        this.answer = new String(ans);
    }

    public String getAnswer(){
        return new String(answer);
    }

    @Override
    public void resetAnswer() {
        this.setFilled(false);
        this.setAnswer("");
        this.setApplicable(true);
    }

}
