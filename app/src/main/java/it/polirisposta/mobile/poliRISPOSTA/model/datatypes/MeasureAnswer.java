package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import java.io.Serializable;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 04/04/15.
 */
public class MeasureAnswer extends Answer implements Serializable {

    private float answer;
    private String um;

    public MeasureAnswer(){
        super();
        this.answer = 0;
        this.um="mm";
        this.setType(DataType.MEASURE);
    }

    public MeasureAnswer(float ans){
        super();
        this.answer = ans;
        this.um="mm";
        this.setType(DataType.MEASURE);
    }

    public MeasureAnswer(int id, boolean prefilled, boolean applicable, boolean filled, float ans, String um, Field f){
        super(id, prefilled, applicable, DataType.MEASURE, filled, f);
        this.answer = ans;
        this.um=um;
    }

    public float getAnswer(){
        return answer;
    }

    public void setAnswer(float ans){
        this.answer = ans;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    @Override
    public void resetAnswer() {
        this.setFilled(false);
        this.setAnswer(0);
        this.setApplicable(true);
    }
}
