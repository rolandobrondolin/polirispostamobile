package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import java.io.Serializable;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 20/03/15.
 */
public class NegativeIntegerAnswer extends Answer implements Serializable {

    private int answer;

    public NegativeIntegerAnswer(){
        super();
        this.answer = 0;
        this.setType(DataType.NEGATIVE_INTEGER);
    }

    public NegativeIntegerAnswer(int ans){
        super();
        this.answer = ans;
        this.setType(DataType.NEGATIVE_INTEGER);
    }

    public NegativeIntegerAnswer(int id, boolean prefilled, boolean applicable, boolean filled, int ans, Field f){
        super(id, prefilled, applicable, DataType.NEGATIVE_INTEGER, filled, f);
        this.answer = ans;
    }

    public int getAnswer(){
        return answer;
    }

    public void setAnswer(int ans){
        this.answer = ans;
    }

    @Override
    public void resetAnswer() {
        this.setFilled(false);
        this.setAnswer(0);
        this.setApplicable(true);
    }

}
