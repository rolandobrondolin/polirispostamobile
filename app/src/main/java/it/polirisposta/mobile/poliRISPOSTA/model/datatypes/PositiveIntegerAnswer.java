package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import java.io.Serializable;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 19/03/15.
 */

public class PositiveIntegerAnswer extends Answer implements Serializable {
    private int answer;

    public PositiveIntegerAnswer(){
        super();
        this.answer = 0;
        this.setType(DataType.POSITIVE_INTEGER);
    }

    public PositiveIntegerAnswer(int ans){
        super();
        this.answer = ans;
        this.setType(DataType.POSITIVE_INTEGER);
    }

    public PositiveIntegerAnswer(int id, boolean prefilled, boolean applicable, boolean filled, int ans, Field f){
        super(id, prefilled, applicable, DataType.POSITIVE_INTEGER, filled, f);
        this.answer = ans;
    }

    public int getAnswer(){
            return answer;
        }

    public void setAnswer(int ans){
            this.answer = ans;
        }

    @Override
    public void resetAnswer() {
        this.setFilled(false);
        this.setAnswer(0);
        this.setApplicable(true);
    }

}
