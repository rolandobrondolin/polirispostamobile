package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

/**
 * Created by Rolando on 18/04/15.
 */
public enum ResourceType {
    IMAGE ("image"),
    AUDIO ("audio");

    private final String type;

    ResourceType(String value) {
        type = value;
    }

    public boolean equalsType(String type){
        return (type==null)? false: this.type.equals(type);
    }

    public static ResourceType getResourceTypeFromString (String type){
        switch (type){
            case "image":
                return IMAGE;
            case "audio":
                return AUDIO;
            default:
                return IMAGE;
        }
    }

    public String toString(){
        return type;
    }

}
