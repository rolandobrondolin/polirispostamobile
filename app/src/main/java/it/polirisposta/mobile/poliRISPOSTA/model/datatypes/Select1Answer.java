package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import java.io.Serializable;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 29/12/14.
 */
public class Select1Answer extends Answer implements Serializable {
    private String answer;


    public Select1Answer(){
        super();
        this.answer = new String();
        this.setType(DataType.SELECT1);
    }

    public Select1Answer(String ans){
        super();
        this.answer = new String(ans);
        this.setType(DataType.SELECT1);
    }

    public Select1Answer(int id, boolean prefilled, boolean applicable, boolean filled, String ans, Field f){
        super(id, prefilled, applicable, DataType.SELECT1, filled, f);
        this.answer = new String(ans);
    }

    public void setAnswer(String ans){
        this.answer = new String(ans);
    }

    public String getAnswer(){
        return new String(answer);
    }
/*
    public boolean isOther(){
        return this.getField().isOther();
    }

    public boolean isAnswerOther(){
        if(!isOther())
            return false;

        for(int i=0; i<getField().getChoices().size(); i++){
            if(getField().getChoices().get(i).equals(answer)){
                return false;
            }
        }
        return true;
    }
*/

    @Override
    public void resetAnswer() {
        this.setFilled(false);
        this.setAnswer("");
        this.setApplicable(true);
    }

}
