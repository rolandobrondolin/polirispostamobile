package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 04/05/15.
 */
public class SelectAnswer extends Answer {
    HashMap<Integer, String> answers;
    ArrayList<Integer> viewIds;

    public SelectAnswer(){
        super();
        this.answers = new HashMap<Integer, String>();
        this.setType(DataType.SELECT);
        viewIds = new ArrayList<>();
    }

    public SelectAnswer(String ans){
        super();
        this.answers = new HashMap<Integer, String>();
        this.setType(DataType.SELECT);
        viewIds = new ArrayList<>();
    }

    public SelectAnswer(int id, boolean prefilled, boolean applicable, boolean filled, String completeAnswer, Field f){
        super(id, prefilled, applicable, DataType.SELECT, filled, f);
        this.answers = new HashMap<Integer, String>();
        stringParser(completeAnswer);
        viewIds = new ArrayList<>();
    }

    public void putAnswer(int index, String str){
        answers.put(index, str);

    }

    public HashMap<Integer, String> getAnswers(){
        return answers;
    }

    public String getAnswer(int key){
        return answers.get(key);
    }

    public String getAnswerDB(){
        String answer = new String();

        for(Integer i: answers.keySet()){
            String s = answers.get(i);
            answer = answer.concat(i + ">>" + s + ":::");
        }
        return answer;
    }

    private void stringParser(String text){
        String vect[] = text.split(":::");
        for(int i=0; i<vect.length; i++){
            String vect2[] = vect[i].split(">>");
            try{
                if(vect2.length==1){
                    answers.put(Integer.parseInt(vect2[0]), "");
                }
                else{
                    answers.put(Integer.parseInt(vect2[0]), vect2[1]);
                }

            }catch (NumberFormatException e){
                //e.printStackTrace();
                //do nothing, the answer is not filled
            }
        }

    }



    @Override
    public void resetAnswer() {
        this.setFilled(false);
        answers = new HashMap<Integer, String>();
        this.setApplicable(true);
    }

    public ArrayList<Integer> getViewIds() {
        return viewIds;
    }

    public void setViewIds(ArrayList<Integer> viewIds) {
        this.viewIds = viewIds;
    }
}
