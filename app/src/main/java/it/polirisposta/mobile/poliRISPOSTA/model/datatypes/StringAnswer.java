package it.polirisposta.mobile.poliRISPOSTA.model.datatypes;

import android.util.Log;

import java.io.Serializable;
import java.nio.charset.Charset;

import it.polirisposta.mobile.poliRISPOSTA.model.Answer;
import it.polirisposta.mobile.poliRISPOSTA.model.Field;

/**
 * Created by Rolando on 29/12/14.
 */

public class StringAnswer extends Answer implements Serializable {
    private String answer;


    public StringAnswer(){
        super();
        this.answer = new String();
        this.setType(DataType.STRING);
    }

    public StringAnswer(String ans){
        super();
        this.answer = new String(ans);
        this.setType(DataType.STRING);
    }

    public StringAnswer(int id, boolean prefilled, boolean applicable, boolean filled, String ans, Field f){
        super(id, prefilled, applicable, DataType.STRING,filled, f);
        this.answer = new String(ans);
    }

    public void setAnswer(String ans){
        this.answer = new String(ans);
    }

    public String getAnswer(){
        return new String(answer);
    }

    @Override
    public void resetAnswer() {
        this.setFilled(false);
        this.setAnswer("");
        this.setApplicable(true);
    }

}
