package it.polirisposta.mobile.poliRISPOSTA.search.activity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by rolando on 16/05/15.
 */
public class ResultContainer {
    private int taskID;
    private ArrayList<Integer> formInstanceIDs;
    private ArrayList<String> formNames;
    private HashMap<Integer, List<SearchSection>> sections;
    private HashMap<Integer, List<SearchQuestion>> questions;
    private HashMap<Integer, List<SearchField>> fields;

    public int getTaskID() {
        return taskID;
    }

    public void setTaskID(int taskID) {
        this.taskID = taskID;
    }

    public ArrayList<Integer> getFormInstanceIDs() {
        return formInstanceIDs;
    }

    public void setFormInstanceIDs(ArrayList<Integer> formInstanceIDs) {
        this.formInstanceIDs = formInstanceIDs;
    }

    public HashMap<Integer, List<SearchSection>> getSections() {
        return sections;
    }

    public void setSections(HashMap<Integer, List<SearchSection>> sections) {
        this.sections = sections;
    }

    public HashMap<Integer, List<SearchQuestion>> getQuestions() {
        return questions;
    }

    public void setQuestions(HashMap<Integer, List<SearchQuestion>> questions) {
        this.questions = questions;
    }

    public HashMap<Integer, List<SearchField>> getFields() {
        return fields;
    }

    public void setFields(HashMap<Integer, List<SearchField>> fields) {
        this.fields = fields;
    }

    public ResultContainer(int taskID) {
        this.taskID = taskID;
        this.formInstanceIDs = new ArrayList<>();
        this.fields = new HashMap<>();
        this.questions = new HashMap<>();
        this.sections = new HashMap<>();
    }

    public void addField(SearchField f, int formInstanceID){
        if(fields.get(formInstanceID)==null){
            fields.put(formInstanceID, new ArrayList<SearchField>());
        }
        this.fields.get(formInstanceID).add(f);
    }

    public void addQuestion(SearchQuestion q, int formInstanceID){
        if(questions.get(formInstanceID)==null){
            questions.put(formInstanceID,new ArrayList<SearchQuestion>());
        }
        this.questions.get(formInstanceID).add(q);
    }

    public void addSection(SearchSection s, int formInstanceID){
        if(sections.get(formInstanceID)==null){
            sections.put(formInstanceID, new ArrayList<SearchSection>());
        }
        this.sections.get(formInstanceID).add(s);
    }

    public ArrayList<String> getFormNames() {
        return formNames;
    }

    public void setFormNames(ArrayList<String> formNames) {
        this.formNames = formNames;
    }


}
