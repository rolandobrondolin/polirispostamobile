package it.polirisposta.mobile.poliRISPOSTA.search.activity;



import android.app.SearchManager;
import android.content.Intent;

import android.os.Bundle;

import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.concurrent.ExecutionException;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.form.activity.FormActivity;


public class SearchActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {
    private int instanceID;
    private int taskID;
    private ResultContainer container;
    private SearchFieldAdapter fieldAdapter;
    private SearchAsyncTask asTask;
    boolean taskSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parseIntent(getIntent());
        setContentView(R.layout.activity_search);
    }

    @Override
    protected void onNewIntent(Intent intent){
        super.onNewIntent(intent);
        parseIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    private void parseIntent(Intent intent){
        if(Intent.ACTION_SEARCH.equals(intent.getAction())){
            String searchQuery = intent.getStringExtra(SearchManager.QUERY);
            taskID = intent.getIntExtra("taskID", -1);
            instanceID = intent.getIntExtra("instanceID", -1);
            this.setTitle(this.getString(R.string.title_activity_search) +" "+ searchQuery);

            container = new ResultContainer(taskID);
            if(instanceID!=-1){
                //spin.setVisibility(View.INVISIBLE);
                taskSearch=false;
                asTask = new SearchAsyncTask(this, searchQuery, instanceID, false);
                asTask.execute();
            }
            else{
                taskSearch=true;
                //spin.setVisibility(View.VISIBLE);
                asTask = new SearchAsyncTask(this, searchQuery, taskID, true);
                asTask.execute();
            }
        }

    }

    @Override
    protected void onResume() {
        try {
            container = asTask.get();
            if(container.getFields().size()!=0){
                fieldAdapter = new SearchFieldAdapter(this, container.getFields().get(instanceID));
                ListView listView = (ListView) findViewById(R.id.resultListView);
                listView.invalidate();
                listView.invalidateViews();
                listView.setAdapter(fieldAdapter);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();

        }
        Spinner spinForm = (Spinner) findViewById(R.id.resultSpinner);

        if(taskSearch && container.getFormInstanceIDs().size()!=0){
            spinForm.setVisibility(View.VISIBLE);
            instanceID = container.getFormInstanceIDs().get(0);

            String[] formArray = new String[container.getFormNames().size()];
            int i=0;
            for (String s: container.getFormNames()){
                formArray[i] = s;
                i++;
            }
            ArrayAdapter<String> formAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, formArray);
            formAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinForm.setAdapter(formAdapter);
            spinForm.setSelection(0);
            spinForm.setOnItemSelectedListener(this);
        }
        else{
            spinForm.setVisibility(View.INVISIBLE);
        }

        Spinner spin = (Spinner) findViewById(R.id.granularitySpinner);
        String[] spinElements = {"Sezioni", "Domande", "Campi"};
        ArrayAdapter<String> spinAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinElements);
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(spinAdapter);
        spin.setSelection(2);
        spin.setOnItemSelectedListener(this);
        ListView list = (ListView) findViewById(R.id.resultListView);
        list.setOnItemClickListener(this);
        super.onResume();
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        ListView list = (ListView) findViewById(R.id.resultListView);
        if (adapterView.getId()==R.id.granularitySpinner){
            changeSearchPerspective(i, list);
        }
        else if(adapterView.getId()==R.id.resultSpinner){
            int newInstanceID = container.getFormInstanceIDs().get(i);
            instanceID = newInstanceID;
            Spinner spin = (Spinner) findViewById(R.id.granularitySpinner);
            int selected = spin.getSelectedItemPosition();
            changeSearchPerspective(selected, list);

        }

    }

    private void changeSearchPerspective(int perspective, ListView list){
        if(perspective==0){
            list.invalidate();
            list.invalidateViews();
            SearchSectionAdapter sAdapter = new SearchSectionAdapter(container.getSections().get(instanceID),this);
            list.setAdapter(sAdapter);
        }
        else if(perspective==1){
            list.invalidate();
            list.invalidateViews();
            SearchQuestionAdapter qAdapter = new SearchQuestionAdapter(container.getQuestions().get(instanceID),this);
            list.setAdapter(qAdapter);
        }
        else if(perspective==2){
            list.invalidate();
            list.invalidateViews();
            SearchFieldAdapter fAdapter = new SearchFieldAdapter(this, container.getFields().get(instanceID));
            list.setAdapter(fAdapter);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        //do nothing!
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(adapterView.getId()==R.id.resultListView) {
            Intent intent = new Intent(this, FormActivity.class);
            boolean edit = true;
            intent.putExtra("id", instanceID);
            intent.putExtra("edit", edit);
            intent.putExtra("taskID", taskID);
            intent.putExtra("fromSearch", true);
            Spinner spin = (Spinner) findViewById(R.id.granularitySpinner);
            int selected = spin.getSelectedItemPosition();
            if (selected == 0) {
                intent.putExtra("sectionInstanceID", container.getSections().get(instanceID).get(i).getSectionInstanceID());
            }
            else if(selected == 1){
                intent.putExtra("questionInstanceID", container.getQuestions().get(instanceID).get(i).getQuestionInstanceID());
            }
            else{
                intent.putExtra("questionInstanceID", container.getFields().get(instanceID).get(i).getQuestionInstanceID());
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("taskID", taskID);
        outState.putInt("instanceID", instanceID);
        outState.putBoolean("taskSearch", taskSearch);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        taskID = savedInstanceState.getInt("taskID",-1);
        instanceID = savedInstanceState.getInt("instanceID", -1);
        taskSearch = savedInstanceState.getBoolean("taskSearch");
        super.onRestoreInstanceState(savedInstanceState);
    }
}
