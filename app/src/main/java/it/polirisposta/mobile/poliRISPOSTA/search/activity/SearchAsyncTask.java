package it.polirisposta.mobile.poliRISPOSTA.search.activity;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;

/**
 * Created by rolando on 17/05/15.
 */
public class SearchAsyncTask extends AsyncTask<Void, Void, ResultContainer> {

    String searchQuery;
    int id;
    boolean taskSearch;
    SearchActivity context;
    ArrayList<String> forms;
    ArrayList<Integer> formInstanceIDs;
    private ResultContainer container;


    public SearchAsyncTask(SearchActivity context, String searchQuery, int id, boolean taskSearch){
        super();
        this.context = context;
        this.searchQuery = searchQuery;
        this.id=id;
        this.taskSearch=taskSearch;
        container= new ResultContainer(id);
        forms = new ArrayList<>();
        formInstanceIDs = new ArrayList<>();
    }

    @Override
    protected ResultContainer doInBackground(Void... voids) {
        if(taskSearch){
            doTaskSearch(searchQuery, id);
        }
        else{
            doFormSearch(searchQuery, id, false);
        }

        return container;

    }
/*
    @Override
    protected void onPostExecute(ResultContainer resultContainer) {
        SearchFieldAdapter fieldAdapter = new SearchFieldAdapter(context, container.getFields().get(id));
        ListView listView = (ListView) context.findViewById(R.id.resultListView);
        listView.invalidate();
        listView.invalidateViews();
        listView.setAdapter(fieldAdapter);
        super.onPostExecute(resultContainer);
    }
*/
    private void doFormSearch(String searchQuery, int instanceID, boolean taskSearch){
        int formID;
        //search for instances, then retrieve the model by id coming from the instances
        Uri formInstanceUri = DatabaseContentProvider.FORM_INSTANCE_URI;
        Uri sectionInstanceUri = DatabaseContentProvider.SECTION_INSTANCE_URI;
        Uri questionInstanceUri = DatabaseContentProvider.QUESTION_INSTANCE_URI;
        Uri formUri = DatabaseContentProvider.FORM_URI;
        Uri sectionUri = DatabaseContentProvider.SECTION_URI;
        Uri questionUri = DatabaseContentProvider.QUESTION_URI;
        String formTextName = new String();
        String sectionTextName = new String();
        String questionTextName = new String();
        int sectionIndex = 0;
        int questionIndex = 0;
        int questInstID = 0;

        ContentResolver resolver = context.getContentResolver();
        Cursor formInstCursor = resolver.query(formInstanceUri, null, PoliRispostaDbContract.FormInstanceColumns._ID + " = " + instanceID, null, null);
        if(formInstCursor!=null){
            formInstCursor.moveToFirst();
            int formFkId = formInstCursor.getInt(formInstCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormInstanceColumns.FORM_ID));
            formInstCursor.close();
            Cursor formCursor = resolver.query(formUri, null, PoliRispostaDbContract.FormColumns._ID + " = " + formFkId, null, null);
            if(formCursor!=null){
                formCursor.moveToFirst();
                formID = formCursor.getInt(formCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormColumns._ID));
                formTextName = formCursor.getString(formCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormColumns.TEXT_NAME));
                formCursor.close();
                //TODO: populate arrayAdapter in case of task search!
                //TODO: assign index with mapping to ids
                if(taskSearch){
                    forms.add(formTextName);
                    formInstanceIDs.add(instanceID);
                }
            }

            //retrieve section instances and sections
            Cursor sectInstCursor = resolver.query(sectionInstanceUri, null, PoliRispostaDbContract.SectionInstanceColumns.FORM_INSTANCE_ID + " = " + instanceID, null, PoliRispostaDbContract.SectionInstanceColumns.SECTION_ID + ", " + PoliRispostaDbContract.SectionInstanceColumns.INDEX);
            if(sectInstCursor!=null){
                while(sectInstCursor.moveToNext()){

                    //data for instance
                    int sectInstID = sectInstCursor.getInt(sectInstCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SectionInstanceColumns._ID));
                    int sectIndex = sectInstCursor.getInt(sectInstCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SectionInstanceColumns.INDEX));
                    int sectionId = sectInstCursor.getInt(sectInstCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SectionInstanceColumns.SECTION_ID));
                    sectionIndex = sectIndex;
                    //find the unique section related
                    Cursor sectionCursor = resolver.query(sectionUri, null, PoliRispostaDbContract.SectionColumns._ID + " = " + sectionId, null, null);
                    if(sectionCursor!= null){
                        sectionCursor.moveToFirst();
                        int sectionID = sectionCursor.getInt(sectionCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SectionColumns._ID));
                        sectionTextName = sectionCursor.getString(sectionCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SectionColumns.TEXT_NAME));
                        if(sectionTextName.toLowerCase().contains(searchQuery.toLowerCase())){
                            String text = sectionTextName;
                            if(sectIndex>1){
                                text = sectionTextName + " (" + sectIndex + ")";
                            }
                            String path = formTextName;
                            container.addSection(new SearchSection(sectionID, sectInstID, text, path), instanceID);
                        }
                        sectionCursor.close();
                    }
                    //now find question instances for each section instance!
                    Cursor questInstanceCursor = resolver.query(questionInstanceUri, null, PoliRispostaDbContract.QuestionInstanceColumns.SECTION_INSTANCE_ID + "=" + sectInstID, null, PoliRispostaDbContract.QuestionInstanceColumns.QUESTION_ID + " , " + PoliRispostaDbContract.QuestionInstanceColumns.INDEX);
                    if(questInstanceCursor!=null){
                        while(questInstanceCursor.moveToNext()){
                            questInstID = questInstanceCursor.getInt(questInstanceCursor.getColumnIndexOrThrow(PoliRispostaDbContract.QuestionInstanceColumns._ID));
                            int questIndex = questInstanceCursor.getInt(questInstanceCursor.getColumnIndexOrThrow(PoliRispostaDbContract.QuestionInstanceColumns.INDEX));
                            //int questSectionInstanceId = formInstCursor.getInt(questInstanceCursor.getColumnIndexOrThrow(QuestionInstanceColumns.SECTION_INSTANCE_ID));
                            int questionId = questInstanceCursor.getInt(questInstanceCursor.getColumnIndexOrThrow(PoliRispostaDbContract.QuestionInstanceColumns.QUESTION_ID));
                            questionIndex = questIndex;
                            Cursor questionCursor = resolver.query(questionUri, null, PoliRispostaDbContract.QuestionColumns._ID + " = " + questionId, null, null);
                            if(questionCursor!=null){
                                questionCursor.moveToFirst();
                                int questionID = questionCursor.getInt(questionCursor.getColumnIndexOrThrow(PoliRispostaDbContract.QuestionColumns._ID));
                                questionTextName = questionCursor.getString(questionCursor.getColumnIndexOrThrow(PoliRispostaDbContract.QuestionColumns.TEXT_NAME));

                                if(questionTextName.toLowerCase().contains(searchQuery.toLowerCase())){
                                    String text = questionTextName;
                                    if(questIndex>1){
                                        text = questionTextName + " (" + questIndex + ")";
                                    }
                                    String path = formTextName + " -> " + sectionTextName;
                                    if(sectionIndex>1){
                                        path = path.concat(" (" + sectionIndex + ")");
                                    }
                                    container.addQuestion(new SearchQuestion(questionID, questInstID, text, path), instanceID);
                                }
                                questionCursor.close();
                            }
                            Cursor answerCursor = resolver.query(DatabaseContentProvider.ANSWER_URI, null, PoliRispostaDbContract.AnswerColumns.QUESTION_INSTANCE_ID + " = " + questInstID, null, null);
                            if(answerCursor!=null){
                                while(answerCursor.moveToNext()){
                                    String ansString = answerCursor.getString(answerCursor.getColumnIndexOrThrow(PoliRispostaDbContract.AnswerColumns.ANSWER_STRING));
                                    Float ansFloat = answerCursor.getFloat(answerCursor.getColumnIndexOrThrow(PoliRispostaDbContract.AnswerColumns.ANSWER_FLOAT));
                                    Integer ansInt = answerCursor.getInt(answerCursor.getColumnIndexOrThrow(PoliRispostaDbContract.AnswerColumns.ANSWER_INTEGER));
                                    Integer ansID = answerCursor.getInt(answerCursor.getColumnIndexOrThrow(PoliRispostaDbContract.AnswerColumns._ID));
                                    Integer fieldID = answerCursor.getInt(answerCursor.getColumnIndexOrThrow(PoliRispostaDbContract.AnswerColumns.FIELD_ID));
                                    Cursor fieldCursor = resolver.query(DatabaseContentProvider.FIELD_URI, null, PoliRispostaDbContract.FieldColumns._ID + " = " + fieldID, null, null);
                                    if(fieldCursor!=null){
                                        fieldCursor.moveToNext();
                                        String textname = fieldCursor.getString(fieldCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FieldColumns.TEXT_NAME));
                                        if(textname.toLowerCase().contains(searchQuery.toLowerCase())){
                                            String path = formTextName + " -> " + sectionTextName;
                                            if(sectionIndex>0){
                                                path = path.concat(" (" + sectionIndex + ")");
                                            }
                                            path = path.concat(" -> " + questionTextName);
                                            if(questionIndex>0){
                                                path = path.concat(" (" + questionIndex + ")");
                                            }

                                            SearchField searchField = new SearchField(fieldID, ansID, questInstID, textname, ansString + " " + ansInt + " " + ansFloat, path);
                                            container.addField(searchField, instanceID);
                                        }

                                        fieldCursor.close();
                                    }

                                }
                                answerCursor.close();
                            }
                        }
                        questInstanceCursor.close();
                    }
                }
                sectInstCursor.close();
            }

        }
    }


    private void doTaskSearch(String searchQuery, int taskID){
        Cursor formInstanceCursor = context.getContentResolver().query(DatabaseContentProvider.FORM_INSTANCE_URI, null, PoliRispostaDbContract.FormInstanceColumns.TASK_ID + " = " + taskID, null, null);
        forms = new ArrayList<>();
        formInstanceIDs = new ArrayList<>();
        if(formInstanceCursor!=null){
            while(formInstanceCursor.moveToNext()){
                int instanceID = formInstanceCursor.getInt(formInstanceCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormInstanceColumns._ID));
                doFormSearch(searchQuery, instanceID, true);
            }
            formInstanceCursor.close();
        }
        container.setFormNames(forms);
        container.setFormInstanceIDs(formInstanceIDs);

    }


}
