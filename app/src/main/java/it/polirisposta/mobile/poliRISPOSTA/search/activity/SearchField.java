package it.polirisposta.mobile.poliRISPOSTA.search.activity;

/**
 * Created by rolando on 16/05/15.
 */
public class SearchField {
    private int fieldID;
    private int answerID;
    private String path;
    private String textname;
    private String answer;
    private int questionInstanceID;

    public int getFieldID() {
        return fieldID;
    }

    public void setFieldID(int fieldID) {
        this.fieldID = fieldID;
    }

    public int getAnswerID() {
        return answerID;
    }

    public void setAnswerID(int answerID) {
        this.answerID = answerID;
    }

    public String getTextname() {
        return textname;
    }

    public void setTextname(String textname) {
        this.textname = textname;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        answer = answer;
    }

    public SearchField(int fieldID, int answerID, int questionInstanceID, String textname, String answer, String path) {
        this.fieldID = fieldID;
        this.answerID = answerID;
        this.textname = textname;
        this.answer = answer;
        this.path = path;
        this.questionInstanceID = questionInstanceID;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getQuestionInstanceID() {
        return questionInstanceID;
    }

    public void setQuestionInstanceID(int questionInstanceID) {
        this.questionInstanceID = questionInstanceID;
    }
}
