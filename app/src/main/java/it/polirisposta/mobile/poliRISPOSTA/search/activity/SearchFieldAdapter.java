package it.polirisposta.mobile.poliRISPOSTA.search.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rolando on 17/05/15.
 */
public class SearchFieldAdapter extends BaseAdapter {

    Context context;
    List<SearchField> fields;

    public SearchFieldAdapter(Context context, List<SearchField> fields) {
        this.context = context;
        this.fields = fields;
    }

    @Override
    public int getCount() {
        if(fields==null){
            return 0;
        }
        return fields.size();
    }

    @Override
    public Object getItem(int i) {
        return fields.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_list_item_2, null);

        }
        TextView title = (TextView)convertView.findViewById(android.R.id.text1);
        title.setText(fields.get(position).getTextname());
        TextView title2 = (TextView)convertView.findViewById(android.R.id.text2);
        title2.setText(fields.get(position).getPath());

        return convertView;
    }

}
