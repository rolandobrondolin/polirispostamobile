package it.polirisposta.mobile.poliRISPOSTA.search.activity;

/**
 * Created by rolando on 16/05/15.
 */
public class SearchQuestion {
    private int questionID;
    private int questionInstanceID;
    private String path;
    private String textName;

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public int getQuestionInstanceID() {
        return questionInstanceID;
    }

    public void setQuestionInstanceID(int questionInstanceID) {
        this.questionInstanceID = questionInstanceID;
    }

    public String getTextName() {
        return textName;
    }

    public void setTextName(String textName) {
        this.textName = textName;
    }

    public SearchQuestion(int questionID, int questionInstanceID, String textName, String path) {
        this.questionID = questionID;
        this.questionInstanceID = questionInstanceID;
        this.textName = textName;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
