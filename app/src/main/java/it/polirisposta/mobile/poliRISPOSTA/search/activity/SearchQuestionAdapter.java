package it.polirisposta.mobile.poliRISPOSTA.search.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rolando on 17/05/15.
 */
public class SearchQuestionAdapter extends BaseAdapter {

    List<SearchQuestion> questions;
    Context context;

    public SearchQuestionAdapter(List<SearchQuestion> questions, Context context) {
        this.questions = questions;
        this.context = context;
    }

    @Override
    public int getCount() {
        if(questions==null){
            return 0;
        }
        return questions.size();
    }

    @Override
    public Object getItem(int i) {
        return questions.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_list_item_2, null);

        }
        TextView title = (TextView)convertView.findViewById(android.R.id.text1);
        title.setText(questions.get(position).getTextName());
        TextView title2 = (TextView)convertView.findViewById(android.R.id.text2);
        title2.setText(questions.get(position).getPath());

        return convertView;
    }
}