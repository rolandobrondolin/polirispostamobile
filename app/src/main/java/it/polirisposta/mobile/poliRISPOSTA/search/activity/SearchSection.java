package it.polirisposta.mobile.poliRISPOSTA.search.activity;

import java.util.ArrayList;

/**
 * Created by rolando on 16/05/15.
 */
public class SearchSection {
    private int sectionID;
    private int sectionInstanceID;
    private String textname;
    private String path;

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public int getSectionInstanceID() {
        return sectionInstanceID;
    }

    public void setSectionInstanceID(int sectionInstanceID) {
        this.sectionInstanceID = sectionInstanceID;
    }

    public String getTextname() {
        return textname;
    }

    public void setTextname(String textname) {
        this.textname = textname;
    }

    public SearchSection(int sectionID, int sectionInstanceID, String textname, String path) {
        this.sectionID = sectionID;
        this.sectionInstanceID = sectionInstanceID;
        this.textname = textname;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
