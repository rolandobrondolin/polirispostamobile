package it.polirisposta.mobile.poliRISPOSTA.search.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rolando on 17/05/15.
 */
public class SearchSectionAdapter extends BaseAdapter {
    List<SearchSection> sections;
    Context context;

    public SearchSectionAdapter(List<SearchSection> sections, Context context) {
        this.sections = sections;
        this.context = context;
    }

    @Override
    public int getCount() {
        if(sections==null){
            return 0;
        }
        return sections.size();
    }

    @Override
    public Object getItem(int i) {
        return sections.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_list_item_2, null);

        }
        TextView title = (TextView)convertView.findViewById(android.R.id.text1);
        title.setText(sections.get(position).getTextname());
        TextView title2 = (TextView)convertView.findViewById(android.R.id.text2);
        title2.setText(sections.get(position).getPath());

        return convertView;
    }
}
