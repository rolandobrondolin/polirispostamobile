package it.polirisposta.mobile.poliRISPOSTA.task.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.util.Util;

public class NewTaskActivity extends Activity {
    ArrayList <Integer> connectIds;
    ArrayList<Integer> boxIds;
    ArrayList<Integer> editIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectIds = new ArrayList<>();
        boxIds = new ArrayList<>();
        editIds = new ArrayList<>();
        setContentView(R.layout.activity_new_task);

        //search for form to be added to the task
        //add it in the view
        //set the btn listener to save the new task, remove activity from the backstack and show the new task

        TableLayout table = (TableLayout) findViewById(R.id.newTaskTable);


        Cursor formCursor = getContentResolver().query(DatabaseContentProvider.FORM_URI, null, null, null, null);
        if(formCursor!=null){
            while(formCursor.moveToNext()){
                String textName = formCursor.getString(formCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormColumns.TEXT_NAME));
                int id = formCursor.getInt(formCursor.getColumnIndexOrThrow(PoliRispostaDbContract.FormColumns._ID));
                connectIds.add(id);

                TableRow row = new TableRow(this);
                row.setId(Util.generateViewId());
                row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.MATCH_PARENT));


                CheckBox box = new CheckBox(this);
                box.setId(Util.generateViewId());
                box.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,TableRow.LayoutParams.WRAP_CONTENT, 1));
                boxIds.add(box.getId());

                TextView text = new TextView(this);
                text.setId(Util.generateViewId());
                text.setText(textName);
                text.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 15));

                EditText edit = new EditText(this);
                edit.setId(Util.generateViewId());
                edit.setInputType(InputType.TYPE_CLASS_NUMBER);
                edit.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,TableRow.LayoutParams.WRAP_CONTENT, 1));
                editIds.add(edit.getId());

                row.addView(box);
                row.addView(text);
                row.addView(edit);
                table.addView(row);

            }

            formCursor.close();
        }

        Button b = (Button) findViewById(R.id.generateTask);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int taskId = -1;
                String name = ((EditText)findViewById(R.id.nameEditText)).getText().toString();
                if(name.equals("")){
                    Toast.makeText(NewTaskActivity.this, "Inserisci un nome per il task!", Toast.LENGTH_LONG).show();
                }
                else{
                    String description = ((EditText)findViewById(R.id.descriptionEditText)).getText().toString();
                    if(description.equals("")){
                        Toast.makeText(NewTaskActivity.this, "Inserisci una descrizione per il task!", Toast.LENGTH_LONG).show();
                    }
                    else{
                        int groupID = 0;
                        Cursor settingsCursor = getContentResolver().query(DatabaseContentProvider.SETTINGS_URI, null, null, null, null);
                        if(settingsCursor!=null){
                            settingsCursor.moveToFirst();
                            groupID = settingsCursor.getInt(settingsCursor.getColumnIndexOrThrow(PoliRispostaDbContract.SettingsColumns.GROUP_ID));
                            settingsCursor.close();
                        }

                        ContentValues taskValues = new ContentValues();
                        taskValues.put(PoliRispostaDbContract.TaskColumns.NAME, name);
                        taskValues.put(PoliRispostaDbContract.TaskColumns.DESCRIPTION, description);
                        taskValues.put(PoliRispostaDbContract.TaskColumns.GENERATED, 1);
                        taskValues.put(PoliRispostaDbContract.TaskColumns.GROUP_ID, groupID);
                        taskValues.put(PoliRispostaDbContract.TaskColumns.SENT, 0);
                        taskValues.put(PoliRispostaDbContract.TaskColumns.USER_DEFINED, 1);

                        int selectedForms = 0;
                        for(int i=0; i<boxIds.size(); i++){
                            CheckBox box = (CheckBox) findViewById(boxIds.get(i));
                            if(box.isChecked()){
                                selectedForms++;
                                if(selectedForms==1){
                                    Uri newTask = getContentResolver().insert(DatabaseContentProvider.TASK_URI, taskValues);
                                    taskId = Integer.parseInt(newTask.getLastPathSegment());

                                    if(taskId<Util.BASE_ID){
                                        ContentValues val = new ContentValues();
                                        val.put(PoliRispostaDbContract.TaskColumns._ID, Util.BASE_ID);
                                        getContentResolver().update(DatabaseContentProvider.TASK_URI, val, PoliRispostaDbContract.TaskColumns._ID + " = " + taskId, null);
                                        taskId = Util.BASE_ID;
                                    }

                                }
                                ContentValues taskModuleValues = new ContentValues();
                                taskModuleValues.put(PoliRispostaDbContract.TaskModuleColumns.TASK_ID, taskId);
                                taskModuleValues.put(PoliRispostaDbContract.TaskModuleColumns.FORM_ID, connectIds.get(i));
                                String nInst = ((EditText) findViewById(editIds.get(i))).getText().toString();
                                int nInstances;
                                if(nInst.equals("")){
                                    nInstances=0;
                                }
                                else{
                                    try{
                                        nInstances = Integer.parseInt(nInst);
                                    }
                                    catch (NumberFormatException e){
                                        e.printStackTrace();
                                        nInstances=0;
                                    }
                                }
                                taskModuleValues.put(PoliRispostaDbContract.TaskModuleColumns.N_INSTANCES, nInstances);
                                Uri taskURI = getContentResolver().insert(DatabaseContentProvider.TASK_MODULE_URI, taskModuleValues);
                                int id = Integer.parseInt(taskURI.getLastPathSegment());
                                if(id<Util.BASE_ID){
                                    ContentValues val = new ContentValues();
                                    val.put(PoliRispostaDbContract.TaskModuleColumns._ID, Util.BASE_ID);
                                    getContentResolver().update(DatabaseContentProvider.TASK_MODULE_URI, val, PoliRispostaDbContract.TaskModuleColumns._ID + " = " + id, null);
                                }


                            }
                        }
                        if(selectedForms==0){
                            Toast.makeText(NewTaskActivity.this, "Seleziona almeno un form per il task che stai creando!", Toast.LENGTH_LONG).show();
                        }
                        else{
                            Intent intent = new Intent(NewTaskActivity.this, TaskActivity.class);
                            intent.putExtra("taskID", taskId);
                            startActivity(intent);
                        }
                    }
                }
            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_task, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
