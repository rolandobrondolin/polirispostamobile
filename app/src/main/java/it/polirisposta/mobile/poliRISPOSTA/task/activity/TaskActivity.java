package it.polirisposta.mobile.poliRISPOSTA.task.activity;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.support.v7.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import it.polirisposta.mobile.poliRISPOSTA.form.activity.FormActivity;
import it.polirisposta.mobile.poliRISPOSTA.launch.activity.asynctask.TaskDownloadAsyncTask;
import it.polirisposta.mobile.poliRISPOSTA.model.DatabaseContentProvider;
import it.polirisposta.mobile.poliRISPOSTA.model.Form;
import it.polirisposta.mobile.poliRISPOSTA.model.FormInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract;
import it.polirisposta.mobile.poliRISPOSTA.model.PoliRispostaDbContract.*;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.model.Task;
import it.polirisposta.mobile.poliRISPOSTA.search.activity.SearchActivity;


public class TaskActivity extends ActionBarActivity {
    private int taskID;
    private Task task;
    private TaskAdapter taskAdapter;
    private int selectedTask;

    //TODO: add group info and statistics
    //TODO: use async task
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        if(savedInstanceState!=null){
            taskID = savedInstanceState.getInt("taskID");
        }
        else{
            Bundle extras = getIntent().getExtras();
            taskID = extras.getInt("taskID");
        }


        Cursor taskCursor = getContentResolver().query(DatabaseContentProvider.TASK_URI, null, TaskColumns._ID + " = " + taskID, null, null);
        if(taskCursor!=null){
            taskCursor.moveToFirst();
            String name = taskCursor.getString(taskCursor.getColumnIndexOrThrow(TaskColumns.NAME));
            String description = taskCursor.getString(taskCursor.getColumnIndexOrThrow(TaskColumns.DESCRIPTION));
            boolean generated = taskCursor.getInt(taskCursor.getColumnIndexOrThrow(TaskColumns.GENERATED))==1;
            boolean userDefined = taskCursor.getInt(taskCursor.getColumnIndexOrThrow(TaskColumns.USER_DEFINED))==1;
            boolean sent = taskCursor.getInt(taskCursor.getColumnIndexOrThrow(TaskColumns.SENT))==1;
            task = new Task(taskID, name, description, generated, userDefined, sent);

            Cursor moduleCursor = getContentResolver().query(DatabaseContentProvider.TASK_MODULE_URI, null, TaskModuleColumns.TASK_ID + " = " + taskID, null, null);
            if(moduleCursor!=null){
                while(moduleCursor.moveToNext()){
                    int formID = moduleCursor.getInt(moduleCursor.getColumnIndexOrThrow(TaskModuleColumns.FORM_ID));
                    int cardinality = moduleCursor.getInt(moduleCursor.getColumnIndexOrThrow(TaskModuleColumns.N_INSTANCES));
                    Cursor formCursor = getContentResolver().query(DatabaseContentProvider.FORM_URI, null, FormColumns._ID + " = " + formID, null, null);
                    if(formCursor!=null){
                        formCursor.moveToFirst();
                        formID = formCursor.getInt(formCursor.getColumnIndexOrThrow(FormColumns._ID));
                        String formName = formCursor.getString(formCursor.getColumnIndexOrThrow(FormColumns.NAME));
                        String formTextName = formCursor.getString(formCursor.getColumnIndexOrThrow(FormColumns.TEXT_NAME));
                        String formDescription = formCursor.getString(formCursor.getColumnIndexOrThrow(FormColumns.DESCRIPTION));
                        formCursor.close();
                        Form form = new Form(formID, formName, formTextName, formDescription);
                        task.addOrigin(form, cardinality);
                    }
                }
                moduleCursor.close();
            }

            taskCursor.close();
        }

        TextView v = (TextView) findViewById(R.id.taskTitleTextView);
        v.setText(task.getName());
        v = (TextView) findViewById(R.id.taskDescriptionTextView);
        v.setText(task.getDescription());

        GridView gridview = (GridView) findViewById(R.id.formInstanceGridView);
        taskAdapter = new TaskAdapter(task, this);
        gridview.setAdapter(taskAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                //launch form activity
                if(!task.getInstances().get(position).isFinalized()){
                    boolean edit = true;
                    Intent intent = new Intent(TaskActivity.this, FormActivity.class);
                    intent.putExtra("id", task.getInstances().get(position).getId());
                    intent.putExtra("edit", edit);
                    intent.putExtra("taskID", taskID);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(TaskActivity.this, "Il form è finalizzato, se vuoi modificarlo definalizzalo", Toast.LENGTH_LONG).show();
                }

            }
        });

        Button b = (Button) findViewById(R.id.addNewForm);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(task.isSent()){
                    Toast.makeText(TaskActivity.this, "Form già inviato, impossibile aggiungere nuovi form!", Toast.LENGTH_LONG).show();
                    return;
                }

                final ArrayList<Form> forms = task.getOrigins();
                int i = 0;
                int size = task.getOrigins().size();
                CharSequence it[] = new CharSequence[size];
                for(Form f : task.getOrigins()){
                    it[i] = f.getTextName();
                    i++;
                }

                final CharSequence items[] = it;

                AlertDialog.Builder builder = new AlertDialog.Builder(TaskActivity.this);
                builder.setTitle("Scegli nuovo form");
                builder.setNeutralButton("Annulla", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        boolean edit = false;
                        Intent intent = new Intent(TaskActivity.this, FormActivity.class);
                        if(task.checkPartials(forms.get(item).getId())){
                            task.setGenerated(true);
                            ContentValues taskVal = new ContentValues();
                            taskVal.put(TaskColumns.GENERATED, task.isGenerated()?1:0);
                            TaskActivity.this.getContentResolver().update(DatabaseContentProvider.TASK_URI, taskVal, TaskColumns._ID + " = " + task.getId(), null);
                            intent.putExtra("id", forms.get(item).getId());
                            intent.putExtra("edit", edit);
                            intent.putExtra("taskID", taskID);
                            startActivity(intent);
                        }
                        else{
                            Toast.makeText(TaskActivity.this, "Puoi inserire al più " + task.getCardinalities().get(forms.get(item).getId()) + " form di questo tipo!", Toast.LENGTH_LONG).show();
                        }

                    }
                });
                builder.show();
            }
        });

        registerForContextMenu(gridview);
    }

    @Override
    protected void onResume() {
        super.onResume();
        View grid = findViewById(R.id.formInstanceGridView);
        populateForms();

        taskAdapter = new TaskAdapter(task, this);
        ((GridView)grid).setAdapter(taskAdapter);
        ((GridView)grid).invalidateViews();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //populateForms();
    }

    private void populateForms(){
        //if already generated take the information of the forms
        task.invalidateInstances();
        if(task.isGenerated()){
            Cursor instanceCursor = getContentResolver().query(DatabaseContentProvider.FORM_INSTANCE_URI, null, FormInstanceColumns.TASK_ID + " = " + taskID, null, null);
            if(instanceCursor!=null){
                while(instanceCursor.moveToNext()){
                    int formInstID = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns._ID));
                    //taskId missing actually!
                    int taskId = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.TASK_ID));
                    int formFkId = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.FORM_ID));
                    String timestamp = instanceCursor.getString(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.TIMESTAMP));
                    String deviceId = instanceCursor.getString(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.DEVICE_ID));
                    boolean finalized = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.FINALIZED))==1;

                    int totalFields = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.TOTAL));
                    int mtotalFields = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.M_TOTAL));
                    int filledFields = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.FILLED));
                    int notFilledFields = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.M_NOT_FILLED));
                    int notApplicableFields = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.M_NOT_APPLICABLE));
                    int mFilled = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.M_FILLED));
                    int mNotFilled = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.M_NOT_FILLED));
                    int mNotApplicable = instanceCursor.getInt(instanceCursor.getColumnIndexOrThrow(FormInstanceColumns.M_NOT_APPLICABLE));

                    Form form = task.getFormFromDBPK(formFkId);
                    FormInstance instance = new FormInstance(formInstID, form, timestamp, deviceId, taskId, finalized);

                    instance.setTotalFields(totalFields);
                    instance.setmTotalFields(mtotalFields);
                    instance.setFilled(filledFields);
                    instance.setNotFilled(notFilledFields);
                    instance.setNotApplicable(notApplicableFields);
                    instance.setmFilled(mFilled);
                    instance.setmNotFilled(mNotFilled);
                    instance.setmNotApplicable(mNotApplicable);

                    task.addInstance(instance);
                    Log.v("TOTAL", "total: " + totalFields);
                    Log.v("TOTAL", "filled: " + filledFields);
                    Log.v("TOTAL", "Mtotal: " + mtotalFields);
                    Log.v("TOTAL", "Mfilled: " + mFilled);
                }
                instanceCursor.close();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_task, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.formSearch).getActionView();

        ComponentName cn = new ComponentName(this, SearchActivity.class);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id== R.id.formSearch){
            onSearchRequested();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void startActivity(Intent intent) {
        // check if search intent
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            intent.putExtra("taskID", task.getId());
        }

        super.startActivity(intent);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("taskID", taskID);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getMenuInflater().inflate(R.menu.menu_context_task, menu);
        menu.setHeaderTitle("Opzioni task");
        GridView gv = (GridView) v;
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        selectedTask = info.position;
        FormInstance instance = (FormInstance) gv.getAdapter().getItem(info.position);
        if(instance.isFinalized()){
            menu.findItem(R.id.menuFinalize).setTitle("Definalizza form");
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.menuFinalize){
            task.getInstances().get(selectedTask).setFinalized(!task.getInstances().get(selectedTask).isFinalized());
            ((GridView)findViewById(R.id.formInstanceGridView)).invalidateViews();
            ContentValues val = new ContentValues();
            val.put(FormInstanceColumns.FINALIZED, task.getInstances().get(selectedTask).isFinalized()?1:0);
            getContentResolver().update(DatabaseContentProvider.FORM_INSTANCE_URI, val, FormInstanceColumns._ID + " = " + task.getInstances().get(selectedTask).getId(), null);
            //with
        }
        else if(item.getItemId()==R.id.menuRemove){

            FormInstance removed = task.removeInstance(selectedTask);
            ((GridView)findViewById(R.id.formInstanceGridView)).invalidateViews();

            getContentResolver().delete(DatabaseContentProvider.FORM_INSTANCE_URI, FormInstanceColumns._ID + " = " + removed.getId(), null);
            //TODO: bug inside the removing action, must update also the internal task state


        }

        return super.onContextItemSelected(item);
    }



}
