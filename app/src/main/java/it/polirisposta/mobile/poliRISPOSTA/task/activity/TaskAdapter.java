package it.polirisposta.mobile.poliRISPOSTA.task.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import it.polirisposta.mobile.poliRISPOSTA.R;
import it.polirisposta.mobile.poliRISPOSTA.model.FormInstance;
import it.polirisposta.mobile.poliRISPOSTA.model.Task;

/**
 * Created by Rolando on 29/04/15.
 */
public class TaskAdapter extends BaseAdapter {
    Task task;
    Context context;

    public TaskAdapter(Task task, Context context){
        this.task = task;
        this.context = context;
    }

    @Override
    public int getCount() {
        return task.getInstances().size();
    }

    @Override
    public Object getItem(int position) {
        return task.getInstances().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.task_grid_item, null);
        }
        FormInstance inst = task.getInstances().get(position);

        TextView v = (TextView) convertView.findViewById(R.id.taskInstanceName);
        v.setText(inst.getForm().getTextName());
        v = (TextView) convertView.findViewById(R.id.taskInstanceDescription);
        v.setText(inst.getForm().getDescription());
        v = (TextView) convertView.findViewById(R.id.taskInstanceCreationTimestamp);

        Long date = Long.parseLong(inst.getTimestamp());
        Date date1 = new Date(date);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ITALY);
        v.setText(sdf.format(date1));

        v = (TextView) convertView.findViewById(R.id.taskInstanceFinalized);
        if(inst.isFinalized()){
            v.setText("Form finalizzato e pronto per essere spedito");
        }
        else{
            v.setText("Form non finalizzato");
        }

        ProgressBar bar = (ProgressBar) convertView.findViewById(R.id.totalProgress);
        bar.setMax(task.getInstances().get(position).getTotalFields());
        bar.setProgress(task.getInstances().get(position).getFilled());

        bar = (ProgressBar) convertView.findViewById(R.id.mandatoryProgress);
        bar.setMax(task.getInstances().get(position).getmTotalFields());
        bar.setProgress(task.getInstances().get(position).getmFilled());

        return convertView;
    }


}
